package modelo;

public class Servicio {
    private String Serv_Codigo;
    private String Serv_Tipo;
    private double Serv_PrecioBase;

    public String getServ_Codigo() {
        return Serv_Codigo;
    }

    public void setServ_Codigo(String Serv_Codigo) {
        this.Serv_Codigo = Serv_Codigo;
    }

    public String getServ_Tipo() {
        return Serv_Tipo;
    }

    public void setServ_Tipo(String Serv_Tipo) {
        this.Serv_Tipo = Serv_Tipo;
    }

    public double getServ_PrecioBase() {
        return Serv_PrecioBase;
    }

    public void setServ_PrecioBase(double Serv_PrecioBase) {
        this.Serv_PrecioBase = Serv_PrecioBase;
    }
    
    public Servicio(){
        
    }
}
