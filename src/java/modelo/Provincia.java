
package modelo;


public class Provincia {
    
    private String prov_codigo;
    private String prov_nombre;
   
    public String getProv_codigo() {
        return prov_codigo;
    }

    public void setProv_codigo(String prov_codigo) {
        this.prov_codigo = prov_codigo;
    }

    public String getProv_nombre() {
        return prov_nombre;
    }

    public void setProv_nombre(String prov_nombre) {
        this.prov_nombre = prov_nombre;
    }
    
    
}