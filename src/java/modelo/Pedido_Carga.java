
package modelo;
public class Pedido_Carga {
    private String PC_codigo;
    private Ruta ruta;//ruta_codigo
    private Servicio servicio;// serv_codigo;
    private Encomienda encomienda;// enco_codigo;
    private Pedido pedido; //ped_codigo;
    private double PC_tarifa_final; 

    public String getPC_codigo() {
        return PC_codigo;
    }

    public void setPC_codigo(String PC_codigo) {
        this.PC_codigo = PC_codigo;
    }


    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Encomienda getEncomienda() {
        return encomienda;
    }

    public void setEncomienda(Encomienda encomienda) {
        this.encomienda = encomienda;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }


    public Pedido_Carga() {
    }

    public double getPC_tarifa_final() {
        return PC_tarifa_final;
    }

    public void setPC_tarifa_final(double PC_tarifa_final) {
        this.PC_tarifa_final = PC_tarifa_final;
    }

    public Pedido_Carga(Ruta ruta, Servicio servicio, Encomienda encomienda, Pedido pedido, double PC_tarifa_final) {
        this.ruta = ruta;
        this.servicio = servicio;
        this.encomienda = encomienda;
        this.pedido = pedido;
        this.PC_tarifa_final = PC_tarifa_final;
    }


    
    

}
