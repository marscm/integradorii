
package modelo;

public class Distrito extends Provincia {
    private int cod_dist;
    private String nombre_dist;

    public int getCod_dist() {
        return cod_dist;
    }

    public void setCod_dist(int cod_dist) {
        this.cod_dist = cod_dist;
    }

    public String getNombre_dist() {
        return nombre_dist;
    }

    public void setNombre_dist(String nombre_dist) {
        this.nombre_dist = nombre_dist;
    }
    
}
