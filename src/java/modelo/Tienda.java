package modelo;

public class Tienda {

    private String tiend_codigo;
    private String tiend_nombre;
    private String tiend_ubicacion;
    private Provincia provincia;

    public Tienda(String tiend_codigo, String tiend_nombre, String tiend_ubicacion, Provincia provincia) {
        this.tiend_codigo = tiend_codigo;
        this.tiend_nombre = tiend_nombre;
        this.tiend_ubicacion = tiend_ubicacion;
        this.provincia = provincia;
    }

    public Tienda() {
    }

    public String getTiend_codigo() {
        return tiend_codigo;
    }

    public void setTiend_codigo(String tiend_codigo) {
        this.tiend_codigo = tiend_codigo;
    }

    public String getTiend_nombre() {
        return tiend_nombre;
    }

    public void setTiend_nombre(String tiend_nombre) {
        this.tiend_nombre = tiend_nombre;
    }

    public String getTiend_ubicacion() {
        return tiend_ubicacion;
    }

    public void setTiend_ubicacion(String tiend_ubicacion) {
        this.tiend_ubicacion = tiend_ubicacion;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

}
