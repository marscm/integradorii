/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author mello
 */
public class DatosTablaResumenCarga {
    private Pedido_Carga pc;
    private Persona dest;

    public Pedido_Carga getPc() {
        return pc;
    }

    public void setPc(Pedido_Carga pc) {
        this.pc = pc;
    }

    public Persona getDest() {
        return dest;
    }

    public void setDest(Persona dest) {
        this.dest = dest;
    }

    

    public DatosTablaResumenCarga() {
    }

    public DatosTablaResumenCarga(Pedido_Carga pc, Persona dest) {
        this.pc = pc;
        this.dest = dest;
    }

    
}
