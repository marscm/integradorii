/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author mello
 */
public class DatosTablaResumenDomicilio {
    private Pedido_Domicilio pd;
    private Persona dest;

    public Pedido_Domicilio getPd() {
        return pd;
    }

    public void setPd(Pedido_Domicilio pd) {
        this.pd = pd;
    }

    public Persona getDest() {
        return dest;
    }

    public void setDest(Persona dest) {
        this.dest = dest;
    }

    public DatosTablaResumenDomicilio(Pedido_Domicilio pd, Persona dest) {
        this.pd = pd;
        this.dest = dest;
    }

    public DatosTablaResumenDomicilio() {
    }

    
}
