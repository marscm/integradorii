
package modelo;

public class PedidosCliente {
    private String cliente_codigo;
    private int ped_codigo;
    private int cliente_celular;
    private String cliente_correo;

    public String getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(String cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
    }

    public int getPed_codigo() {
        return ped_codigo;
    }

    public void setPed_codigo(int ped_codigo) {
        this.ped_codigo = ped_codigo;
    }

    public int getCliente_celular() {
        return cliente_celular;
    }

    public void setCliente_celular(int cliente_celular) {
        this.cliente_celular = cliente_celular;
    }

    public String getCliente_correo() {
        return cliente_correo;
    }

    public void setCliente_correo(String cliente_correo) {
        this.cliente_correo = cliente_correo;
    }



    public PedidosCliente(String cliente_codigo, int cliente_celular, String cliente_correo) {
        this.cliente_codigo = cliente_codigo;
        this.cliente_celular = cliente_celular;
        this.cliente_correo = cliente_correo;
    }

    public PedidosCliente() {
    }

    public PedidosCliente(String cliente_codigo, int ped_codigo, int cliente_celular, String cliente_correo) {
        this.cliente_codigo = cliente_codigo;
        this.ped_codigo = ped_codigo;
        this.cliente_celular = cliente_celular;
        this.cliente_correo = cliente_correo;
    }
    
    
}
