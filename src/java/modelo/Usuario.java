/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author CRISTIAN
 */
public class Usuario {

    private String usua_codigo;
    private int usua_celular;
    private String usua_correo;
    private String usua_contra;
    private String pers_dni;
    private String tu_codigo;
    public Usuario() {
    }

    public Usuario(String usua_codigo, int usua_celular, String usua_correo, String pers_dni, String tu_codigo, String usua_contra) {
        this.usua_codigo = usua_codigo;
        this.usua_celular = usua_celular;
        this.usua_correo = usua_correo;
        this.pers_dni = pers_dni;
        this.tu_codigo = tu_codigo;
        this.usua_contra = usua_contra;
    }

    public String getUsua_codigo() {
        return usua_codigo;
    }

    public String getUsua_contra() {
        return usua_contra;
    }

    public void setUsua_contra(String usua_contra) {
        this.usua_contra = usua_contra;
    }

    public void setUsua_codigo(String usua_codigo) {
        this.usua_codigo = usua_codigo;
    }

    public int getUsua_celular() {
        return usua_celular;
    }

    public void setUsua_celular(int usua_celular) {
        this.usua_celular = usua_celular;
    }

    public String getUsua_correo() {
        return usua_correo;
    }

    public void setUsua_correo(String usua_correo) {
        this.usua_correo = usua_correo;
    }

    public String getPers_dni() {
        return pers_dni;
    }

    public void setPers_dni(String pers_dni) {
        this.pers_dni = pers_dni;
    }

    public String getTu_codigo() {
        return tu_codigo;
    }

    public void setTu_codigo(String tu_codigo) {
        this.tu_codigo = tu_codigo;
    }

}
