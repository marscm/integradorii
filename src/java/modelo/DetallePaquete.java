
package modelo;

public class DetallePaquete {
    private String DP_codigo;
    private Encomienda encomienda;// enco_codigo
    private double DP_anchoMax;
    private double DP_largoMax;
    private double DP_altoMax;

    public String getDP_codigo() {
        return DP_codigo;
    }

    public void setDP_codigo(String DP_codigo) {
        this.DP_codigo = DP_codigo;
    }

    public Encomienda getEncomienda() {
        return encomienda;
    }

    public void setEncomienda(Encomienda encomienda) {
        this.encomienda = encomienda;
    }

    public double getDP_anchoMax() {
        return DP_anchoMax;
    }

    public void setDP_anchoMax(double DP_anchoMax) {
        this.DP_anchoMax = DP_anchoMax;
    }

    public double getDP_largoMax() {
        return DP_largoMax;
    }

    public void setDP_largoMax(double DP_largoMax) {
        this.DP_largoMax = DP_largoMax;
    }

    public double getDP_altoMax() {
        return DP_altoMax;
    }

    public void setDP_altoMax(double DP_altoMax) {
        this.DP_altoMax = DP_altoMax;
    }

    public DetallePaquete() {
    }



    public DetallePaquete(Encomienda encomienda, double DP_anchoMax, double DP_largoMax, double DP_altoMax) {
        this.encomienda = encomienda;
        this.DP_anchoMax = DP_anchoMax;
        this.DP_largoMax = DP_largoMax;
        this.DP_altoMax = DP_altoMax;
    }
 
}
