
package modelo;

public class Encomienda {
    private String enco_codigo;
    private String enco_tipo;
    private double enco_pesoMaxGramos;
    private double enco_precioBase;

    public String getEnco_codigo() {
        return enco_codigo;
    }

    public void setEnco_codigo(String enco_codigo) {
        this.enco_codigo = enco_codigo;
    }

    public String getEnco_tipo() {
        return enco_tipo;
    }

    public void setEnco_tipo(String enco_tipo) {
        this.enco_tipo = enco_tipo;
    }

    public double getEnco_pesoMaxGramos() {
        return enco_pesoMaxGramos;
    }

    public void setEnco_pesoMaxGramos(double enco_pesoMaxGramos) {
        this.enco_pesoMaxGramos = enco_pesoMaxGramos;
    }

    public double getEnco_precioBase() {
        return enco_precioBase;
    }

    public void setEnco_precioBase(double enco_precioBase) {
        this.enco_precioBase = enco_precioBase;
    }

    public Encomienda() {
    }

    public Encomienda(String enco_codigo, double enco_pesoMaxGramos, double enco_precioBase) {
        this.enco_codigo = enco_codigo;
        this.enco_pesoMaxGramos = enco_pesoMaxGramos;
        this.enco_precioBase = enco_precioBase;
    }
    
    
}
