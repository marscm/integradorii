/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
public class Pedido_Domicilio {
    private String PD_codigo;
    private Ruta ruta;//ruta_codigo
    private Servicio servicio;// serv_codigo;
    private Encomienda encomienda;// enco_codigo;
    private Pedido pedido; //ped_codigo;
    private double PD_tarifa_final; 

    public String getPD_codigo() {
        return PD_codigo;
    }

    public void setPD_codigo(String PD_codigo) {
        this.PD_codigo = PD_codigo;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Encomienda getEncomienda() {
        return encomienda;
    }

    public void setEncomienda(Encomienda encomienda) {
        this.encomienda = encomienda;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public double getPD_tarifa_final() {
        return PD_tarifa_final;
    }

    public void setPD_tarifa_final(double PD_tarifa_final) {
        this.PD_tarifa_final = PD_tarifa_final;
    }

    public Pedido_Domicilio() {
    }

    public Pedido_Domicilio(Ruta ruta, Servicio servicio, Encomienda encomienda, Pedido pedido, double PD_tarifa_final) {
        this.ruta = ruta;
        this.servicio = servicio;
        this.encomienda = encomienda;
        this.pedido = pedido;
        this.PD_tarifa_final = PD_tarifa_final;
    }
    
    

}
