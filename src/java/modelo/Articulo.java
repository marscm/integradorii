
package modelo;

public class Articulo {
    private String art_codigo;
    private String art_descripcion;

    public String getArt_codigo() {
        return art_codigo;
    }

    public void setArt_codigo(String art_codigo) {
        this.art_codigo = art_codigo;
    }

    public String getArt_descripcion() {
        return art_descripcion;
    }

    public void setArt_descripcion(String art_descripcion) {
        this.art_descripcion = art_descripcion;
    }

    public Articulo(String art_descripcion) {
        this.art_descripcion = art_descripcion;
    }

    public Articulo() {
    }
    
    
}
