package modelo;

public class Detalle_Pedido_Domicilio {
    private Pedido_Domicilio pd;//PD_codigo;
    private Articulo articulo;// art_codigo;
    private String DPD_art_esp;
    private String DPD_codigo;
    private String DPD_ubicacion;
    private String DPD_referencia;

    public String getDPD_codigo() {
        return DPD_codigo;
    }

    public void setDPD_codigo(String DPD_codigo) {
        this.DPD_codigo = DPD_codigo;
    }

    
    
    public Pedido_Domicilio getPd() {
        return pd;
    }

    public void setPd(Pedido_Domicilio pd) {
        this.pd = pd;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getDPD_art_esp() {
        return DPD_art_esp;
    }

    public void setDPD_art_esp(String DPD_art_esp) {
        this.DPD_art_esp = DPD_art_esp;
    }

    public String getDPD_ubicacion() {
        return DPD_ubicacion;
    }

    public void setDPD_ubicacion(String DPD_ubicacion) {
        this.DPD_ubicacion = DPD_ubicacion;
    }

    public String getDPD_referencia() {
        return DPD_referencia;
    }

    public void setDPD_referencia(String DPD_referencia) {
        this.DPD_referencia = DPD_referencia;
    }

    public Detalle_Pedido_Domicilio() {
    }

    public Detalle_Pedido_Domicilio(Pedido_Domicilio pd, Articulo articulo, String DPD_art_esp, String DPD_ubicacion, String DPD_referencia) {
        this.pd = pd;
        this.articulo = articulo;
        this.DPD_art_esp = DPD_art_esp;
        this.DPD_ubicacion = DPD_ubicacion;
        this.DPD_referencia = DPD_referencia;
    }
    
    
    
}
