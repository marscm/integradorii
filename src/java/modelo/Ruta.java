package modelo;

public class Ruta {
    private String ruta_codigo;
    private String ruta_origen;
    private String ruta_sede_origen;
    private String ruta_destino;
    private String ruta_sede_destino;
    private double ruta_precio_base;

    public Ruta(String ruta_origen, String ruta_sede_origen, String ruta_destino, String ruta_sede_destino) {
        this.ruta_origen = ruta_origen;
        this.ruta_sede_origen = ruta_sede_origen;
        this.ruta_destino = ruta_destino;
        this.ruta_sede_destino = ruta_sede_destino;
    }
    
    public Ruta(String ruta_codigo, double ruta_precio_base) {
        this.ruta_codigo = ruta_codigo;
        this.ruta_precio_base = ruta_precio_base;
    }
    
    
    public Ruta() {
    }

    
    
    public String getRuta_codigo() {
        return ruta_codigo;
    }

    public void setRuta_codigo(String ruta_codigo) {
        this.ruta_codigo = ruta_codigo;
    }

    public String getRuta_origen() {
        return ruta_origen;
    }

    public void setRuta_origen(String ruta_origen) {
        this.ruta_origen = ruta_origen;
    }

    public String getRuta_sede_origen() {
        return ruta_sede_origen;
    }

    public void setRuta_sede_origen(String ruta_sede_origen) {
        this.ruta_sede_origen = ruta_sede_origen;
    }

    public String getRuta_destino() {
        return ruta_destino;
    }

    public void setRuta_destino(String ruta_destino) {
        this.ruta_destino = ruta_destino;
    }

    public String getRuta_sede_destino() {
        return ruta_sede_destino;
    }

    public void setRuta_sede_destino(String ruta_sede_destino) {
        this.ruta_sede_destino = ruta_sede_destino;
    }

    public double getRuta_precio_base() {
        return ruta_precio_base;
    }

    public void setRuta_precio_base(double ruta_precio_base) {
        this.ruta_precio_base = ruta_precio_base;
    }
    
    
}
