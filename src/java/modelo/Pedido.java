
package modelo;
import java.util.Date;
public class Pedido {
    private int pedido_codigo;
    private Date ped_fecha;

    public int getPedido_codigo() {
        return pedido_codigo;
    }

    public void setPedido_codigo(int pedido_codigo) {
        this.pedido_codigo = pedido_codigo;
    }

    public Date getPed_fecha() {
        return ped_fecha;
    }

    public void setPed_fecha(Date ped_fecha) {
        this.ped_fecha = ped_fecha;
    }


    
}
