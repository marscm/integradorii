package modelo;

public class Detalle_Pedido_Carga {
    private Pedido_Carga pc;//PC_codigo;
    private Articulo articulo;// art_codigo;
    private String DPC_codigo;
    private String DPC_art_esp;

    public Pedido_Carga getPc() {
        return pc;
    }

    public void setPc(Pedido_Carga pc) {
        this.pc = pc;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Detalle_Pedido_Carga() {
    }

    public String getDPC_codigo() {
        return DPC_codigo;
    }

    public void setDPC_codigo(String DPC_codigo) {
        this.DPC_codigo = DPC_codigo;
    }

    public String getDPC_art_esp() {
        return DPC_art_esp;
    }

    public void setDPC_art_esp(String DPC_art_esp) {
        this.DPC_art_esp = DPC_art_esp;
    }

    public Detalle_Pedido_Carga(Pedido_Carga pc, Articulo articulo, String DPC_art_esp) {
        this.pc = pc;
        this.articulo = articulo;
        this.DPC_art_esp = DPC_art_esp;
    }
        
    
    
}
