/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mello
 */
public class Conexion {

    Connection con;

    public Connection getCon() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/fast_courier", "root", "");
            System.out.println("Conexión exitosa");
        } catch (ClassNotFoundException | SQLException exc) {
            System.out.println("Error de conexión: " + exc);
        }
        return con;
    }

    public static void main(String[] args) {
        Conexion con = new Conexion();
        con.getCon();
    }

}
