/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.util.List;
import modelo.DetallePaquete;


public interface Interface_Negocio_DetallePaquete {   
    public void agregar(DetallePaquete dp);
    public void eliminar(String codigo_dp);
    public void editar (DetallePaquete dp);
    public List<DetallePaquete> listar(String codigo_encomienda);
    public Object listarUno(String codigo_dp);
}
