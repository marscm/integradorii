/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Pedido_Domicilio;


public interface Interface_Negocio_PedidoDomicilio {   
    public Pedido_Domicilio agregar(Pedido_Domicilio pd);
    public void editar (Pedido_Domicilio pd);
    public Pedido_Domicilio listarUno(String codigo);
}
