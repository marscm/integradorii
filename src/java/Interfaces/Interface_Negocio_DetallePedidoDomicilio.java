/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Detalle_Pedido_Domicilio;

public interface Interface_Negocio_DetallePedidoDomicilio {   
    public Detalle_Pedido_Domicilio Generar(Detalle_Pedido_Domicilio dpd);
    public void eliminar(int codigo);
    public void editar (Detalle_Pedido_Domicilio dpd);
    public Object listarUno(int codigo);
}
