/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;
import modelo.Destinatario;
public interface InterfaceDestinatario {
    public interface InterfaceCliente {
        public boolean agregar(Destinatario d);
        public boolean eliminar(int dni);
        public boolean editar(Destinatario d);
}
}
