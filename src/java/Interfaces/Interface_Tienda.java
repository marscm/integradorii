
package Interfaces;
import modelo.Tienda;
import java.util.List;
public interface Interface_Tienda extends Interface_Generica<Tienda>{
public void agregarTienda(String tiend_nombre, String tiend_ubicacion, String prov_codigo);
    public List<Tienda> listar(String prov_cod);
    public List<Tienda> listar2(String parametro);
    public int listarCantidad(String prov_cod);
    public int listarCantidad2(String parametro);
}
