/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.util.List;
import modelo.Articulo;
public interface Interface_Negocio_Articulo{   
    public Articulo agregar(Articulo art);
    public void eliminar(String codigo);
    public List<Articulo> listar();
    public int  listarCantidad();
    public Articulo listarUno(String codigo);
}
