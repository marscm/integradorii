/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.util.List;
import modelo.Encomienda;
public interface Interface_Negocio_Encomienda{   
    public List<Encomienda> listar();
    public Encomienda listarUno(String codigo);
    public Encomienda getEncomiendaByPeso(Double Peso);
}
