package Interfaces;

import java.util.List;

public interface Interface_Generica<O>{
    
    public List<O> listar();
    public O obtenerUno(String Codigo);
    int listarCantidad();
    public void eliminaUno(String codigo);
    public void actualizaUno(O o);

}
