/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Pedido_Carga;


public interface Interface_Negocio_PedidoCarga {   
    public Pedido_Carga agregar(Pedido_Carga pc);
    public void editar (Pedido_Carga pc);
    public Pedido_Carga listarUno(String codigo);
}
