/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Persona;
public interface InterfazPersona {
        public boolean agregar(Persona p);
        public boolean eliminar(int dni);
        public boolean editar(Persona p);
}
