
package Interfaces;

import java.util.ArrayList;
import modelo.Provincia;

public interface InterfazProvincia {
    public ArrayList<Provincia> ListarProvincias();
}
