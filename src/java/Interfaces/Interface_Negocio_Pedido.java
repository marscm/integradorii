
package Interfaces;

import modelo.Pedido;
public interface Interface_Negocio_Pedido{   
    public Pedido generarPedido();
    public void eliminar(int codigo);
    public Pedido listarUno(int codigo);
}
