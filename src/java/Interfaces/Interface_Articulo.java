/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Articulo;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface Interface_Articulo extends Interface_Generica<Articulo>{
     public void agregarArticulo(String art_descripcion);
     public List<Articulo> listar(String parametro_busqueda);
     public int listarCantidad2(String parametro_busqueda);
}
