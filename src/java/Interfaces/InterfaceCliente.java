/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Cliente;

/**
 *
 * @author mello
 */
public interface InterfaceCliente {
        public boolean agregar(Cliente p);
        public boolean eliminar(String dni);
        public boolean editar(Cliente p);
}
