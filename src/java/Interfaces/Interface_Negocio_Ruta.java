/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.util.List;
import modelo.Ruta;
public interface Interface_Negocio_Ruta{   
    public void agregar(Ruta ruta);
    public void eliminar(String codigo);
    public void editar (Ruta ruta);
    public List<Ruta> listar();
    public int  listarCantidad();
    public Ruta listarUno(String codigo);
}
