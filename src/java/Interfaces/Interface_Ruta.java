package Interfaces;

import java.util.List;
import modelo.Ruta;

public interface Interface_Ruta extends Interface_Generica<Ruta> {
    public Ruta buscarRuta(String RUTA_ORIGEN,String RUTA_SEDE_ORIGEN,String RUTA_DESTINO,String RUTA_SEDE_DESTINO);
    public Ruta listarUno(String RUTA_COD);
    public void agregarRuta(String ruta_origen, String ruta_sede_origen, String ruta_destino, String ruta_sede_destino, double ruta_precio_base);
    public List<Ruta> listar(String ruta_codigo);
    public List<Ruta> listarSegunOrigen(String ruta_origen);
    public List<Ruta> listarSegunDestino(String ruta_destino);
    public int listarCantidad(String segun_origen_o_destino, String origen_o_destino);
}