/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.util.List;
import modelo.Servicio;

public interface Interface_Servicio extends Interface_Generica<Servicio> {   
    public void agregarServicio(String serv_tipo,Double serv_precio_base);
    public void eliminarServicio(String serv_cod);
    public Servicio buscarCodigo(String serv_tipo);
    public void editarServicio (Servicio s);
    @Override
    public List<Servicio> listar();
    @Override
    public int  listarCantidad();
    public Servicio listarUno(String serv_cod);
    public int listarCantidad2(String parametro_busqueda) ;
    public List<Servicio> listar(String parametro_busqueda);
    @Override
    public Servicio obtenerUno(String Codigo);
}
