package Interfaces;

import modelo.Encomienda;
import java.util.List;

public interface Interface_Encomienda extends Interface_Generica<Encomienda> {

    public void agregarEncomienda(String Categoría,
            String enco_tipo, double enco_PesoMaxGramos,
            double enco_precio_base, double dp_AnchoMax_cm,
            double dp_LargoMax_cm,
            double dp_AltoMax_cm);

    public List<Encomienda> listar(String parametro_busqueda);

    public int listarCantidad(String parametro_busqueda);
}
