/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import modelo.Detalle_Pedido_Carga;

public interface Interface_Negocio_DetallePedidoCarga {   
    public Detalle_Pedido_Carga Generar(Detalle_Pedido_Carga dpc);
    public void eliminar(int codigo);
    public void editar (Detalle_Pedido_Carga dpc);
    public Object listarUno(int codigo);
}
