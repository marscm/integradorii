package Interfaces;

import modelo.Provincia;

public interface Interface_Provincia extends Interface_Generica<Provincia>{
    
    public void agregarProvincia(String prov_nombre);
    public String buscarCodigo(String prov_nombre);
}