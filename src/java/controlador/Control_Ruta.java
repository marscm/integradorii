
package controlador;

import Interfaces.Interface_Ruta;

import modeloDAO.RutaDAO;

import modelo.Ruta;

import java.io.IOException;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Control_Ruta", urlPatterns = {"/Control_Ruta"})
public class Control_Ruta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Interface_Ruta interface_ruta = new RutaDAO();
        
        Ruta ruta;
        
        String accion = request.getParameter("accion");
        String pagina = "";
        
        if(accion.equalsIgnoreCase("agregarRuta")){
            
            String ruta_origen =  request.getParameter("cmbProvincia1");
            String ruta_sede_origen = request.getParameter("cmbTienda1");
            String ruta_destino =  request.getParameter("cmbProvincia2");
            String ruta_sede_destino = request.getParameter("cmbTienda2");
            double ruta_precio_base = Double.parseDouble(request.getParameter("txtPrecio"));
            
            interface_ruta.agregarRuta(ruta_origen, ruta_sede_origen, ruta_destino, ruta_sede_destino, ruta_precio_base);
            
            pagina = "admi_actuRuta_1";            
            
        }else if(accion.equalsIgnoreCase("eliminaRuta")){
            
            String ruta_codigo = request.getParameter("ruta_codigo");
            
            interface_ruta.eliminaUno(ruta_codigo);
            
            pagina = "admi_actuRuta_1"; 
            
        }else if(accion.equalsIgnoreCase("actualizaRuta")){
            

            String ruta_codigo = request.getParameter("txtCodigo");
            double ruta_precio = Double.parseDouble(request.getParameter("txtPrecio"));
            
            ruta = new Ruta(ruta_codigo,ruta_precio);

            interface_ruta.actualizaUno(ruta);
            
            pagina = "admi_actuRuta_1"; 
        }
        
        if(pagina.equals("admi_actuRuta_1")){
            
            RequestDispatcher rd = request.getRequestDispatcher("admi_actuRuta_1.jsp");
            rd.forward(request, response);  
            
        }        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
