/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Interfaces.Interface_Detalle_Paquete;
import modelo.DetallePaquete;
import modelo.Encomienda;
import modeloDAO.Detalle_PaqueteDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(name = "Control_DetallePaquete", urlPatterns = {"/Control_DetallePaquete"})
public class Control_DetallePaquete extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Interface_Detalle_Paquete interface_detalle_paquete = new Detalle_PaqueteDAO();
        
        Encomienda encomienda;
        DetallePaquete detalle_paquete;
        
        String accion = request.getParameter("accion");

        String pagina = "";
        
        if (accion.equalsIgnoreCase("actualizaDetallePaquete")) {

            String enco_codigo = request.getParameter("txtCodigo");
            double enco_pesomaxgramos = Double.parseDouble(request.getParameter("txtPeso"));
            double enco_precio_base = Double.parseDouble(request.getParameter("txtPrecio"));
            double dp_anchomax_cm = Double.parseDouble(request.getParameter("txtAncho"));
            double dp_largomax_cm = Double.parseDouble(request.getParameter("txtLargo"));
            double dp_altomax_cm = Double.parseDouble(request.getParameter("txtAlto"));
            
            encomienda = new Encomienda(enco_codigo,enco_pesomaxgramos,enco_precio_base);

            System.out.println(encomienda.getEnco_codigo() + "HOLA");
            
            detalle_paquete = new DetallePaquete(encomienda,dp_anchomax_cm,dp_largomax_cm,dp_altomax_cm);

            
            interface_detalle_paquete.actualizaUno(detalle_paquete);
            
            pagina = "admi_encomienda";
        }
        
        
        if(pagina.equals("admi_encomienda")){
            
            RequestDispatcher rd = request.getRequestDispatcher("admi_encomienda.jsp");
            rd.forward(request, response);  
            
        } 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
