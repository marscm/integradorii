package controlador;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.faces.event.ActionEvent;
import modelo.Articulo;
import modelo.Cliente;
import modelo.DatosTablaResumenCarga;
import modelo.Destinatario;
import modelo.Detalle_Pedido_Carga;
import modelo.Encomienda;
import modelo.Pedido;
import modelo.Pedido_Carga;
import modelo.PedidosCliente;
import modelo.Persona;
import modelo.Provincia;
import modelo.Ruta;
import modelo.Servicio;
import modelo.Tienda;
import modeloDAO.ArticuloDAO;
import modeloDAO.ClienteDAO;
import modeloDAO.DestinatarioDAO;
import modeloDAO.DetallePedidoCargaDAO;
import modeloDAO.EncomiendaDAO;
import modeloDAO.PedidoCargaDAO;
import modeloDAO.PedidoDAO;
import modeloDAO.Pedidos_ClienteDAO;
import modeloDAO.PersonaDAO;
import modeloDAO.ProvinciaDAO;
import modeloDAO.RutaDAO;
import modeloDAO.ServicioDAO;
import modeloDAO.TiendaDAO;
@ManagedBean
@SessionScoped
public class beanPedidoCarga {
private List<SelectItem> listaProvincias;
    private List<SelectItem> listaTiendas;
    private List<SelectItem> listaDep;
    private List<SelectItem> listaDist;
    private String tienda_codigo;
    private String tienda_nombre;
    private String prov_codigo;
    private String prov_nombre;
    private String dep_llegada_codigo;
    private String dep_llegada_nombre;
    private String prov_llegada_codigo;
    private String prov_llegada_nombre;
    private String cliente_nom;
    private String cliente_ap;
    private String cliente_dni;
    private int cliente_celular;
    private String cliente_correo;
    private double alto;
    private double ancho;
    private double fondo;
    private double peso;
    private double valorRef;
    private String paquete_desc;
    private String destinatario_nom;
    private String destinatario_ap;
    private String destinatario_dni;
    private int tlf_llegada;
    private Pedido ped=new Pedido();
    private Pedido_Carga pcarga=new Pedido_Carga();
    private final PersonaDAO pdao=new PersonaDAO();
    private final ProvinciaDAO pvdao=new ProvinciaDAO();
    private final TiendaDAO tddao=new TiendaDAO();
    private final ClienteDAO cd=new ClienteDAO();
    private final DestinatarioDAO dd=new DestinatarioDAO();
    private final ServicioDAO servDAO=new ServicioDAO();
    private final EncomiendaDAO encoDAO=new EncomiendaDAO();
    private final ArticuloDAO artDAO=new ArticuloDAO();
    private final RutaDAO ruDAO = new RutaDAO();
    private final PedidoDAO pedDAO=new PedidoDAO();
    private final PedidoCargaDAO pcarDAO=new PedidoCargaDAO();
    private final Pedidos_ClienteDAO PCD=new Pedidos_ClienteDAO();
    private final DetallePedidoCargaDAO dpcDAO= new DetallePedidoCargaDAO();
    public List<DatosTablaResumenCarga> PedidosGenerados = new ArrayList(); 
    @PostConstruct
    public void init(){
       
    }
    
    public String getTienda_codigo() {
        return tienda_codigo;
    }

    public void setTienda_codigo(String tienda_codigo) {
        this.tienda_codigo = tienda_codigo;
    }

    public String getTienda_nombre() {
        return tienda_nombre;
    }

    public void setTienda_nombre(String tienda_nombre) {
        this.tienda_nombre = tienda_nombre;
    }

    public String getProv_codigo() {
        return prov_codigo;
    }

    public void setProv_codigo(String prov_codigo) {
        this.prov_codigo = prov_codigo;
    }

    public String getProv_nombre() {
        return prov_nombre;
    }

    public void setProv_nombre(String prov_nombre) {
        this.prov_nombre = prov_nombre;
    }

    public List<SelectItem> getListaProvincias() {
        this.listaProvincias = new ArrayList<>();
        ProvinciaDAO pdao = new ProvinciaDAO();
        List<Provincia> provincias = pdao.listar();
        listaProvincias.clear();
        for (Provincia p : provincias) {
            SelectItem sItem = new SelectItem(p.getProv_codigo(), p.getProv_nombre());
            this.listaProvincias.add(sItem);
        }
        System.out.println("Se cargaron las provincias: " + listaProvincias.size());
        return listaProvincias;
    }

    public void setListaProvincias(List<SelectItem> listaProvincias) {
        this.listaProvincias = listaProvincias;
    }

    public List<SelectItem> getListaTiendas() {
        this.listaTiendas = new ArrayList<>();
        TiendaDAO tdao = new TiendaDAO();
        List<Tienda> tiendas = tdao.listar(prov_codigo);

        listaTiendas.clear();
        for (Tienda t : tiendas) {
            SelectItem sItem = new SelectItem(t.getTiend_codigo(), t.getTiend_nombre());
            this.listaTiendas.add(sItem);
        }
        System.out.println("Se agregaron las tiendas de la prov " + prov_codigo + ": " + listaTiendas.size());

        return listaTiendas;
    }

    public void setListaTiendas(List<SelectItem> listaTiendas) {
        this.listaTiendas = listaTiendas;
    }

    public String getDep_llegada_codigo() {
        return dep_llegada_codigo;
    }

    public void setDep_llegada_codigo(String dep_llegada_codigo) {
        this.dep_llegada_codigo = dep_llegada_codigo;
    }

    public String getDep_llegada_nombre() {
        return dep_llegada_nombre;
    }

    public void setDep_llegada_nombre(String dep_llegada_nombre) {
        this.dep_llegada_nombre = dep_llegada_nombre;
    }

    public String getProv_llegada_codigo() {
        return prov_llegada_codigo;
    }

    public void setProv_llegada_codigo(String prov_llegada_codigo) {
        this.prov_llegada_codigo = prov_llegada_codigo;
    }

    public String getProv_llegada_nombre() {
        return prov_llegada_nombre;
    }

    public void setProv_llegada_nombre(String prov_llegada_nombre) {
        this.prov_llegada_nombre = prov_llegada_nombre;
    }

    public List<SelectItem> getListaDep() {
        this.listaDep = new ArrayList<>();
        ProvinciaDAO pdao = new ProvinciaDAO();
        List<Provincia> provincias = pdao.listar();
        listaDep.clear();
        for (Provincia p : provincias) {
            SelectItem sItem = new SelectItem(p.getProv_codigo(), p.getProv_nombre());
            this.listaDep.add(sItem);
        }
        System.out.println("Se cargaron las provincias: " + listaDep.size());
        return listaDep;
    }

    public void setListaDep(List<SelectItem> listaDep) {
        this.listaDep = listaDep;
    }

    public List<SelectItem> getListaDist() {
        this.listaDist = new ArrayList<>();
        TiendaDAO tdao = new TiendaDAO();
        List<Tienda> tiendas = tdao.listar(dep_llegada_codigo);

        listaDist.clear();
        for (Tienda t : tiendas) {
            SelectItem sItem = new SelectItem(t.getTiend_codigo(), t.getTiend_nombre());
            this.listaDist.add(sItem);
        }
        System.out.println("Se agregaron las tiendas de la prov " + dep_llegada_codigo + ": " + listaDist.size());
        return listaDist;
    }

    public void setListaDist(List<SelectItem> listaDist) {
        this.listaDist = listaDist;
    }

    public String getCliente_nom() {
        return cliente_nom;
    }

    public void setCliente_nom(String cliente_nom) {
        this.cliente_nom = cliente_nom;
    }

    public String getCliente_ap() {
        return cliente_ap;
    }

    public void setCliente_ap(String cliente_ap) {
        this.cliente_ap = cliente_ap;
    }

    public String getCliente_dni() {
        return cliente_dni;
    }

    public void setCliente_dni(String cliente_dni) {
        this.cliente_dni = cliente_dni;
    }

    public int getCliente_celular() {
        return cliente_celular;
    }

    public void setCliente_celular(int cliente_celular) {
        this.cliente_celular = cliente_celular;
    }

    public String getCliente_correo() {
        return cliente_correo;
    }

    public void setCliente_correo(String cliente_correo) {
        this.cliente_correo = cliente_correo;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getFondo() {
        return fondo;
    }

    public void setFondo(double fondo) {
        this.fondo = fondo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getValorRef() {
        return valorRef;
    }

    public void setValorRef(double valorRef) {
        this.valorRef = valorRef;
    }

    public String getPaquete_desc() {
        return paquete_desc;
    }

    public void setPaquete_desc(String paquete_desc) {
        this.paquete_desc = paquete_desc;
    }

    public String getDestinatario_nom() {
        return destinatario_nom;
    }

    public void setDestinatario_nom(String destinatario_nom) {
        this.destinatario_nom = destinatario_nom;
    }

    public String getDestinatario_ap() {
        return destinatario_ap;
    }

    public void setDestinatario_ap(String destinatario_ap) {
        this.destinatario_ap = destinatario_ap;
    }

    public String getDestinatario_dni() {
        return destinatario_dni;
    }

    public void setDestinatario_dni(String destinatario_dni) {
        this.destinatario_dni = destinatario_dni;
    }

    public int getTlf_llegada() {
        return tlf_llegada;
    }

    public void setTlf_llegada(int tlf_llegada) {
        this.tlf_llegada = tlf_llegada;
    }

    public Pedido getPed() {
        return ped;
    }

    public void setPed(Pedido ped) {
        this.ped = ped;
    }

    public Pedido_Carga getPcarga() {
        return pcarga;
    }

    public void setPcarga(Pedido_Carga pcarga) {
        this.pcarga = pcarga;
    }

    public List<DatosTablaResumenCarga> getPedidosGenerados() {
        return PedidosGenerados;
    }

    public void setPedidosGenerados(List<DatosTablaResumenCarga> PedidosGenerados) {
        this.PedidosGenerados = PedidosGenerados;
    }
    
    public void cargarATablaResumen(ActionEvent event){
        Persona cliente=new Persona(cliente_ap,cliente_nom,cliente_dni);
        Persona destinatario=new Persona(destinatario_ap,destinatario_nom,destinatario_dni);
        Cliente c=new Cliente();
        c.setDni(cliente.getDni());
        Destinatario d=new Destinatario();   
        d.setDni(destinatario.getDni());
        pdao.agregar(cliente);
        pdao.agregar(destinatario);
        cd.agregar(c);
        dd.agregar(d);
        /* termina la carga de personas a la BD*/
        /*Obtencion de valores para llenar tabla */
            Servicio serv=servDAO.listarServicio("Carga");
            System.out.println("Seleccionó el servicio con precio: "+serv.getServ_PrecioBase());
            /* Obtengo el tipo de servicio*/
            if(peso==0.0) peso=0.09;
            Encomienda enco = encoDAO.getEncomiendaByPeso(peso);
            System.out.println("Encomienda seleccionada: "+enco.getEnco_tipo()+" con precio:"+enco.getEnco_precioBase());
            /* Obtengo el tipo de Encomienda */
            Articulo art=new Articulo(paquete_desc);
            art=artDAO.agregar(art);
            prov_nombre=pvdao.obtenerUno(prov_codigo).getProv_nombre();
            tienda_nombre=tddao.obtenerUno(tienda_codigo).getTiend_nombre();
            dep_llegada_nombre=pvdao.obtenerUno(dep_llegada_codigo).getProv_nombre();
            prov_llegada_nombre=tddao.obtenerUno(prov_llegada_codigo).getTiend_nombre();
            System.out.println("provincia origen: "+prov_nombre);
            System.out.println("tienda origen: "+tienda_nombre);    
            System.out.println("Departamente destino: "+dep_llegada_nombre);
            System.out.println("tienda destino: "+prov_llegada_nombre);
            Ruta ruta = ruDAO.buscarRuta(prov_nombre, tienda_nombre, dep_llegada_nombre, prov_llegada_nombre);
            System.out.println("Ruta seleccionada: "+ruta.getRuta_codigo()+" con precio: "+ruta.getRuta_precio_base());
            /* obtengo la ruta a partir de datos del formulario*/
        /*Obtencion de valores para llenar tabla */
        double tarifa_final=(ruta.getRuta_precio_base()+serv.getServ_PrecioBase())*enco.getEnco_precioBase();
        /* obtengo el valor de la tarifa final basada en tipo de encomienda el tipo de servicio y la ruta*/
        ped=pedDAO.generarPedido();
        System.out.println("Se genero el pedido: "+ped.getPedido_codigo());
        pcarga=new Pedido_Carga(ruta, serv, enco, ped, tarifa_final);
        pcarga=pcarDAO.agregar(pcarga);
        System.out.println("Se cargo el pedido carga: "+pcarga.getPC_codigo());
        System.out.println("paque desc:  "+paquete_desc);
        Detalle_Pedido_Carga dpc=new Detalle_Pedido_Carga(pcarga,art,paquete_desc);
        dpc=dpcDAO.Generar(dpc);
        System.out.println("Se cargo el detalle del pedido: "+dpc.getDPC_codigo());
        String cod_cliente=cd.getCodigo(cliente_dni);
        PedidosCliente PedidosXcliente= new PedidosCliente(cod_cliente,ped.getPedido_codigo(),cliente_celular,cliente_correo);
        PCD.agregar(PedidosXcliente);
        DatosTablaResumenCarga e=new DatosTablaResumenCarga(pcarga,destinatario);
        PedidosGenerados.add(e);
        //llenado del objeto DPD
        clear();
    }
    public void clear() {
        setCliente_ap(null);
        setCliente_nom(null);
        setCliente_dni(null);
        setDestinatario_ap(null);
        setDestinatario_nom(null);
        setDestinatario_dni(null);
        setTlf_llegada(0);
        setPaquete_desc(null);
        setProv_codigo(null);
        setTienda_codigo(null);
        setDep_llegada_codigo(null);
        setProv_llegada_codigo(null);
        setValorRef(0);
        setAlto(0);
        setAncho(0);
        setPeso(0);
        setFondo(0);
        setCliente_celular(0);
        setCliente_correo(null);
    }
}
