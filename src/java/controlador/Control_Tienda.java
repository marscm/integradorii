/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Interfaces.Interface_Provincia;
import Interfaces.Interface_Tienda;
import modelo.Provincia;
import modelo.Tienda;
import modeloDAO.ProvinciaDAO;
import modeloDAO.TiendaDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(name = "Control_Tienda", urlPatterns = {"/Control_Tienda"})
public class Control_Tienda extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ProvinciaDAO provinciaDao = new ProvinciaDAO();
        Interface_Provincia interface_provincia = provinciaDao;        
        
        TiendaDAO tiendaDao = new TiendaDAO();
        Interface_Tienda interface_tienda = tiendaDao;  
        
        Provincia provincia;
        Tienda tienda;
        
        String action = request.getParameter("accion");
        
        String pagina = "";
        
        if(action.equalsIgnoreCase("agregarTienda")){
            
            String prov_nombre =  request.getParameter("cmbprov_nombre");
            String tiend_nombre = request.getParameter("txttiend_nombre");
            String tiend_ubicacion = request.getParameter("txttiend_ubicacion");
            
            String prov_codigo = interface_provincia.buscarCodigo(prov_nombre);
            
            interface_tienda.agregarTienda(tiend_nombre, tiend_ubicacion, prov_codigo);
            
            pagina = "admi_tiendaRuta_1";
            
            
        }else if(action.equalsIgnoreCase("eliminar")){
            
            String tiend_codigo = request.getParameter("atiendcodigo");
            
            interface_tienda.eliminaUno(tiend_codigo);
            
            pagina = "admi_tiendaRuta_1";
            
  
        } else if(action.equalsIgnoreCase("actualizarTienda")){
            
            String tiend_codigo = request.getParameter("up_tienda_codigo");
            String tiend_nombre = request.getParameter("update_nombre");
            String tiend_ubicacion = request.getParameter("update_ubicacion"); 
            String prov_codigo = request.getParameter("update_provincia");
            
            provincia = interface_provincia.obtenerUno(prov_codigo);
            
            
            tienda = new Tienda(tiend_codigo,tiend_nombre,tiend_ubicacion,provincia);
          
          
            interface_tienda.actualizaUno(tienda);
            
            
            pagina = "admi_tiendaRuta_1";
            
        }
        
        
        if(pagina.equals("admi_tiendaRuta_1")){
            
            RequestDispatcher rd = request.getRequestDispatcher("admi_tiendaRuta_1.jsp");
            rd.forward(request, response);  
            
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
