package controlador;

import Interfaces.Interface_Detalle_Paquete;
import Interfaces.Interface_Encomienda;
import modelo.Encomienda;
import modeloDAO.Detalle_PaqueteDAO;

import modeloDAO.EncomiendaDAO;

import java.io.IOException;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Control_Encomienda", urlPatterns = {"/Control_Encomienda"})
public class Control_Encomienda extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Interface_Encomienda interface_encomienda = new EncomiendaDAO();
        Interface_Detalle_Paquete interface_detalle_paquete = new Detalle_PaqueteDAO();

        String accion = request.getParameter("accion");

        String pagina = "";

        if (accion.equalsIgnoreCase("agregarEncomienda")) {

            String categoria;

            String dp_AnchoMax_cm = request.getParameter("txtAncho");
            String dp_LargoMax_cm = request.getParameter("txtLargo");
            String dp_AltoMax_cm = request.getParameter("txtAlto");
            String enco_tipo = request.getParameter("txtTipo");
            double enco_PesoMaxGramos = Double.parseDouble(request.getParameter("txtPeso"));
            double enco_precio_base = Double.parseDouble(request.getParameter("txtPrecio"));

            if (dp_AnchoMax_cm.isEmpty() || dp_LargoMax_cm.isEmpty() || dp_AltoMax_cm.isEmpty()) {
                categoria = "OTRO";
                interface_encomienda.agregarEncomienda(categoria, enco_tipo, enco_PesoMaxGramos, enco_precio_base, 0, 0, 0);
            } else {
                categoria = "PAQUETE";
                interface_encomienda.agregarEncomienda(categoria, enco_tipo, enco_PesoMaxGramos, enco_precio_base,
                        Double.parseDouble(dp_AnchoMax_cm), Double.parseDouble(dp_LargoMax_cm), Double.parseDouble(dp_AltoMax_cm));
            }

            pagina = "admi_encomienda";

        } else if (accion.equalsIgnoreCase("eliminaEncomienda")) {

            String enco_codigo = request.getParameter("codigo");
            interface_encomienda.eliminaUno(enco_codigo);

            pagina = "admi_encomienda";

        } else if (accion.equalsIgnoreCase("actualizaEncomienda")) {

            String enco_codigo = request.getParameter("txtCodigo");
            double enco_pesomaxgramos = Double.parseDouble(request.getParameter("txtPeso"));
            double enco_precio_base = Double.parseDouble(request.getParameter("txtPrecio"));

            Encomienda encomienda = new Encomienda();
            encomienda.setEnco_codigo(enco_codigo);
            encomienda.setEnco_pesoMaxGramos(enco_pesomaxgramos);
            encomienda.setEnco_precioBase(enco_precio_base);

            interface_encomienda.actualizaUno(encomienda);

            pagina = "admi_encomienda";

        }

        if (pagina.equals("admi_encomienda")) {

            RequestDispatcher rd = request.getRequestDispatcher("admi_encomienda.jsp");
            rd.forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
