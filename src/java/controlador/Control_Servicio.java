
package controlador;

import Interfaces.Interface_Servicio;
import modelo.Servicio;
import modeloDAO.ServicioDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Control_Servicio", urlPatterns = {"/Control_Servicio"})
public class Control_Servicio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        Interface_Servicio interface_servicio = new ServicioDAO();
        
        String accion = request.getParameter("accion");
        
        String pagina = "";
        
        if(accion.equalsIgnoreCase("agregarServicio")){
            
            String serv_tipo =  request.getParameter("txtTipo");
            double serv_precio_base = Double.parseDouble(request.getParameter("txtPrecioBase"));
            interface_servicio.agregarServicio(serv_tipo, serv_precio_base);
            
            pagina = "admi_servicio";  
            
        }else if(accion.equalsIgnoreCase("actualizaServicio")){
            
            String serv_codigo=  request.getParameter("txtCodigo");
            double serv_precio_base = Double.parseDouble(request.getParameter("txtPrecio"));
            
            Servicio servicio = new Servicio();
            servicio.setServ_Codigo(serv_codigo);
            servicio.setServ_PrecioBase(serv_precio_base);
            
            interface_servicio.actualizaUno(servicio);
            pagina = "admi_servicio";
            
            
        }else if(accion.equalsIgnoreCase("eliminaServicio")){
            
            String serv_codigo = request.getParameter("serv_codigo");
            
            interface_servicio.eliminaUno(serv_codigo);
            pagina = "admi_servicio";
            
        }   
        
        
        if(pagina.equalsIgnoreCase("admi_servicio")){
            
            RequestDispatcher rd = request.getRequestDispatcher("admi_servicio.jsp");
            rd.forward(request, response);  
            
        }
                
        
        
        
        


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
