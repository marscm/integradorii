/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import Interfaces.InterfaceDestinatario;
import Util.Conexion;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Destinatario;

/**
 *
 * @author mello
 */
public class DestinatarioDAO implements InterfaceDestinatario{
    Destinatario d;
    Connection conn = null;
    PreparedStatement sentencia;
    ResultSet rs;
    public boolean agregar(Destinatario d){
        conn = MySQLConexion.getConexion();
        try {
        String sql = "CALL SP_INSERT_DESTINATARIO(?);";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1,d.getDni());
            st.executeUpdate();
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }

    public boolean eliminar(int dni) {
        try{
        conn = MySQLConexion.getConexion();
        String solicitud="DELETE FROM destinatario WHERE Pers_Dni="+dni;
        sentencia=conn.prepareStatement(solicitud);
        sentencia.executeUpdate();
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }


    public boolean editar(Destinatario d) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
