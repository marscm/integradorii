/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import Interfaces.Validar;
import modelo.Usuario;
import Util.MySQLConexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author CRISTIAN
 */
public class Logindao implements Validar {
    Connection conn = null;
    PreparedStatement ps;
    ResultSet rs;
    int r=0;
    
    @Override
    public int validar (Usuario u){
     
        String sql="select * from usuario where pers_dni=? and usua_contra=? and tu_codigo=?";
     
        try{
            conn = MySQLConexion.getConexion();
            ps=conn.prepareStatement(sql);
    
              ps.setString(1,u.getPers_dni());
              ps.setString(2,u.getUsua_contra());
              
              rs=ps.executeQuery();
              while(rs.next()){
                  r=r+1;
                
             u.setPers_dni(rs.getString("pers_dni"));
             u.setUsua_contra(rs.getString("usua_contra"));
              } 
              if(r==1){
                 return 1; 
              }else {
                  return 0;
              }
                           
        }catch (Exception e) {
            return 0;
        }
      
    }
}



  