
package modeloDAO;

import Interfaces.Interface_Articulo;
import Interfaces.Interface_Negocio_Articulo;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Articulo;

public class ArticuloDAO implements Interface_Negocio_Articulo,Interface_Articulo{

    @Override
    public Articulo agregar(Articulo art) {
            Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_ARTICULO(?);";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1, art.getArt_descripcion());
            st.executeQuery();
            PreparedStatement ps=conn.prepareStatement("SELECT * FROM ARTICULO WHERE ART_CODIGO =(SELECT MAX(ART_CODIGO) FROM ARTICULO);");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                
                art.setArt_codigo(rs.getString(1));
                art.setArt_descripcion(rs.getString(2));
            }
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }
        }
        return art;
    }

    @Override
    public void eliminar(String codigo) {
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "DELETE FROM ARTICULO WHERE ART_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            st.executeUpdate(); 
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }
    
    @Override
    public List<Articulo> listar() {
        List<Articulo> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ARTICULO";
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Articulo art = new Articulo();
                
                art.setArt_codigo(rs.getString(1));
                art.setArt_descripcion(rs.getString(2));
                lis.add(art);
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return lis;           
    }

    @Override
    public int listarCantidad() {
        return listar().size();
    }

    @Override
    public Articulo listarUno(String codigo) {
        Articulo articulo = null;
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ARTICULO where art_codigo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Articulo art = new Articulo();
                
                art.setArt_codigo(rs.getString(1));
                art.setArt_descripcion(rs.getString(2));
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return articulo;     
    }

    @Override
    public void agregarArticulo(String art_descripcion) {
       Connection conn = null;

        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_ARTICULO(?);";
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, art_descripcion);

            ResultSet rs = st.executeQuery();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }

    @Override
    public List<Articulo> listar(String parametro_busqueda) {
        List<Articulo> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ARTICULO WHERE ART_CODIGO LIKE ? OR ART_DESCRIPCION LIKE ?";
            PreparedStatement st = conn.prepareStatement(sql);

           
            st.setString(1, "%" + parametro_busqueda + "%");
            st.setString(2, "%" + parametro_busqueda + "%");
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                
                Articulo articulo = new Articulo();
                
                articulo.setArt_codigo(rs.getString("art_codigo"));
                articulo.setArt_descripcion(rs.getString("art_descripcion"));

                lis.add(articulo);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    }

    @Override
    public int listarCantidad2(String parametro_busqueda) {
        return listar(parametro_busqueda).size();
    }

    @Override
    public Articulo obtenerUno(String Codigo) {
        Articulo articulo = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ARTICULO WHERE ART_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, Codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                articulo = new Articulo();
                
                articulo.setArt_codigo(rs.getString("art_codigo"));
                articulo.setArt_descripcion(rs.getString("art_descripcion"));
               
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return articulo;
    }

    @Override
    public void eliminaUno(String codigo) {
        int resp = 0;
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM ARTICULO WHERE ART_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            
            resp = st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (Exception e2) {
                
            }
        }
    }
    

    @Override
    public void actualizaUno(Articulo articulo) {
        int resp = 0;
        Connection conn = null;
        
        try {

            String sql = "UPDATE ARTICULO SET ART_DESCRIPCION = ? WHERE ART_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);

            st.setString(1, articulo.getArt_descripcion());
            st.setString(2, articulo.getArt_codigo());

            resp = st.executeUpdate(); // ejecuta la accionde grabar.

        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }
    
}
