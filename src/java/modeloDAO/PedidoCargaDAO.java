
package modeloDAO;

import Interfaces.Interface_Negocio_PedidoCarga;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Pedido_Carga;

public class PedidoCargaDAO implements Interface_Negocio_PedidoCarga {

    @Override
    public void editar(Pedido_Carga pd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pedido_Carga listarUno(String codigo) {
        Pedido_Carga pedido = null;
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM Pedido_Carga where pc_codigo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                pedido = new Pedido_Carga();
                
                pedido.setPC_codigo(rs.getString(1));
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return pedido;     
    }

    @Override
    public Pedido_Carga agregar(Pedido_Carga pc) {
        String ruta=pc.getRuta().getRuta_codigo();//
        String servicio=pc.getServicio().getServ_Codigo();//
        String enco=pc.getEncomienda().getEnco_codigo();//
        int cod_ped=pc.getPedido().getPedido_codigo();//
        double tarifa_final= pc.getPC_tarifa_final();
        ResultSet rs;
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_PEDIDO_CARGA(?,?,?,?,?);";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1, ruta);
            st.setString(2, servicio);
            st.setString(3, enco);
            st.setInt(4, cod_ped);
            st.setDouble(5,tarifa_final);
            st.executeQuery();
            PreparedStatement ps = conn.prepareStatement("SELECT PC_CODIGO FROM "
                    + "PEDIDO_CARGA WHERE PC_CODIGO=(SELECT MAX(PC_CODIGO) "
                    + "FROM PEDIDO_CARGA);");
            rs = ps.executeQuery();
            while (rs.next()) {
                pc.setPC_codigo(rs.getString(1));
            }
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }
        }
        return pc;
    }

}
