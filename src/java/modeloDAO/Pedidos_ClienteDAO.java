
package modeloDAO;

import Util.MySQLConexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import modelo.PedidosCliente;

public class Pedidos_ClienteDAO {
    public PedidosCliente agregar(PedidosCliente pc ) {
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "INSERT INTO PEDIDOS_CLIENTE (CLIENT_CODIGO,PED_CODIGO,PC_CELULAR,PC_CORREO) VALUES (?,?,?,?);";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, pc.getCliente_codigo());
            st.setInt(2, pc.getPed_codigo());
            st.setInt(3, pc.getCliente_celular());
            st.setString(4, pc.getCliente_correo());
            st.executeUpdate();
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }
        }
        return pc;
    }
}
