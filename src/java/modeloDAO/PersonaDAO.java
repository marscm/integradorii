package modeloDAO;
import Interfaces.InterfazPersona;
import Util.MySQLConexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Distrito;
import modelo.Persona;
public class PersonaDAO implements InterfazPersona {
    Distrito d;
    Connection conn = null;
    PreparedStatement sentencia;
    ResultSet rs;
    @Override
    public boolean agregar(Persona p) {
        try{
        conn = MySQLConexion.getConexion();
        String solicitud="insert ignore into persona(Pers_Dni,Pers_Nombres,Pers_Apellidos) values(?,?,?)";
        sentencia=conn.prepareStatement(solicitud);
        sentencia.setString(1, p.getDni());
        sentencia.setString(2, p.getNombre());
        sentencia.setString(3, p.getApellido());
        sentencia.executeUpdate();
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }

    @Override
    public boolean eliminar(int dni) {
        try{
        conn = MySQLConexion.getConexion();
        String solicitud="DELETE FROM persona WHERE Pers_Dni="+dni;
        sentencia=conn.prepareStatement(solicitud);
        sentencia.executeUpdate();
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }

    @Override
    public boolean editar(Persona p) {
        try{
        conn = MySQLConexion.getConexion();
        String solicitud="UPDATE persona SET values(?,?,?,?,?)";
        sentencia=conn.prepareStatement(solicitud);
        sentencia.setString(1, p.getDni());
        sentencia.setString(2, p.getNombre());
        sentencia.setString(3, p.getApellido());
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }

}
