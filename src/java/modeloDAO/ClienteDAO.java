/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;
import Interfaces.InterfaceCliente;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Cliente;
public class ClienteDAO implements InterfaceCliente{
    Cliente c;
    Connection conn = null;
    PreparedStatement sentencia;
    ResultSet rs;
    @Override
    public boolean agregar(Cliente c){
        conn = MySQLConexion.getConexion();
        try {
        String sql = "CALL SP_INSERT_CLIENTE(?);";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1,c.getDni());
            st.executeUpdate();
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }

    @Override
    public boolean eliminar(String dni) {
        conn = MySQLConexion.getConexion();
        try{
        String solicitud="DELETE FROM cliente WHERE Pers_Dni="+dni;
        sentencia=conn.prepareStatement(solicitud);
        sentencia.executeUpdate();
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return false;
    }


    @Override
    public boolean editar(Cliente p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String getCodigo(String dni){
        String cod="";
        conn = MySQLConexion.getConexion();
        try{
        String solicitud="SELECT CLIENT_CODIGO FROM cliente WHERE Pers_Dni="+dni;
        sentencia=conn.prepareStatement(solicitud);
        rs=sentencia.executeQuery();
        while(rs.next()){
            cod=rs.getString(1);
            }
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return cod;
    }
   

}
