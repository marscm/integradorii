package modeloDAO;

import Util.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Distrito;

public class DistritoDAO {
    Distrito d;
    Conexion cn=new Conexion();
    Connection con;
    PreparedStatement sentencia;
    ResultSet rs;
    ArrayList<Distrito> Lista=new ArrayList<>();
    public ArrayList<Distrito> TiendasPorProvincia(int id_prov){
        try{
        con=cn.getCon();
        String solicitud="SELECT DISTRITO FROM Distrito WHERE ID_PROVINCIA=?";
        sentencia=con.prepareStatement(solicitud);
        sentencia.setInt(1,id_prov);
        rs=sentencia.executeQuery();
        while(rs.next()){
            d=new Distrito();
            d.setNombre_dist(rs.getString(1));
            Lista.add(d);
            }
        }
        catch(SQLException ex){
            System.out.println(""+ex);
        }
        return Lista;
    }
}
