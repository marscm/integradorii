
package modeloDAO;

import Interfaces.Interface_Negocio_PedidoDomicilio;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Pedido_Domicilio;

public class PedidoDomicilioDAO implements Interface_Negocio_PedidoDomicilio {

    @Override
    public void editar(Pedido_Domicilio pd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pedido_Domicilio listarUno(String codigo) {
        Pedido_Domicilio pedido = null;
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM Pedido_Domicilio where pd_codigo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                pedido = new Pedido_Domicilio();
                
                pedido.setPD_codigo(rs.getString(1));
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return pedido;     
    }

    @Override
    public Pedido_Domicilio agregar(Pedido_Domicilio pd) {
        String ruta=pd.getRuta().getRuta_codigo();//
        String servicio=pd.getServicio().getServ_Codigo();//
        String enco=pd.getEncomienda().getEnco_codigo();//
        int cod_ped=pd.getPedido().getPedido_codigo();//
        double tarifa_final= pd.getPD_tarifa_final();
        ResultSet rs;
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_PEDIDO_DOMICILIO(?,?,?,?,?);";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1, ruta);
            st.setString(2, servicio);
            st.setString(3, enco);
            st.setInt(4, cod_ped);
            st.setDouble(5,tarifa_final);
            st.executeQuery();
            PreparedStatement ps = conn.prepareStatement("SELECT PD_CODIGO FROM "
                    + "PEDIDO_DOMICILIO WHERE PD_CODIGO=(SELECT MAX(PD_CODIGO) "
                    + "FROM PEDIDO_DOMICILIO);");
            rs = ps.executeQuery();
            while (rs.next()) {
                pd.setPD_codigo(rs.getString(1));
            }
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }
        }
        return pd;
    }

}
