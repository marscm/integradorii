
package modeloDAO;

import Interfaces.Interface_Servicio;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Servicio;

public class ServicioDAO implements Interface_Servicio {
   
    @Override
    public void agregarServicio(String serv_tipo,Double serv_precio_base) {
            Connection conn = null;

        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_SERV(?,?);";
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, serv_tipo);
            st.setDouble(2, serv_precio_base);
            
            ResultSet rs = st.executeQuery();

        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }
        }
    }

    @Override
    public Servicio buscarCodigo(String serv_tipo) {
        
        Servicio servicio = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM SERVICIO WHERE serv_tipo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, serv_tipo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                servicio = new Servicio();
                
                servicio.setServ_Codigo(rs.getString(1));
                servicio.setServ_Tipo(rs.getString(2));
                servicio.setServ_PrecioBase(rs.getDouble(3));
               
            }
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (SQLException e2) {
                
            }
        }

        return servicio;
    }

    @Override
    public List<Servicio> listar() {
        List<Servicio> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM SERVICIO";
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Servicio servicio = new Servicio();
                
                servicio.setServ_Codigo(rs.getString(1));
                servicio.setServ_Tipo(rs.getString(2));
                servicio.setServ_PrecioBase(rs.getDouble(3)); 
                lis.add(servicio);
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return lis;           
    }

    @Override
    public void eliminarServicio(String serv_cod) {
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "DELETE FROM SERVICIO WHERE SERV_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, serv_cod);
            st.executeUpdate(); 
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }

    @Override
    public int listarCantidad(){
        return listar().size();
    }
    @Override
    public Servicio listarUno(String serv_cod){
        Servicio servicio = null;
        Connection conn = null;
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM SERVICIO WHERE serv_codigo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, serv_cod);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                servicio = new Servicio();
                servicio.setServ_Codigo(rs.getString(1));
                servicio.setServ_Tipo(rs.getString(2));
                servicio.setServ_PrecioBase(rs.getDouble(3));
               
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return servicio;
    }
    public Servicio listarServicio(String tipo_serv){
        Servicio servicio = null;
        Connection conn = null;
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM SERVICIO WHERE serv_tipo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, tipo_serv);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                servicio = new Servicio();
                servicio.setServ_Codigo(rs.getString(1));
                servicio.setServ_Tipo(rs.getString(2));
                servicio.setServ_PrecioBase(rs.getDouble(3));
               
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return servicio;
    }
    @Override
    public List<Servicio> listar(String parametro_busqueda){

         List<Servicio> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM SERVICIO WHERE SERV_CODIGO LIKE ? OR SERV_TIPO LIKE ? OR SERV_PRECIO_BASE LIKE ?";
            PreparedStatement st = conn.prepareStatement(sql);

           
            st.setString(1, "%" + parametro_busqueda + "%");
            st.setString(2, "%" + parametro_busqueda + "%");
            st.setString(3, "%" + parametro_busqueda + "%");
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                
                Servicio servicio = new Servicio();
                
                servicio.setServ_Codigo(rs.getString("serv_codigo"));
                servicio.setServ_Tipo(rs.getString("serv_tipo"));
                servicio.setServ_PrecioBase(rs.getDouble("serv_precio_base"));
                
                lis.add(servicio);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;   
    }    
    
    @Override
    public void editarServicio(Servicio s){
        Connection conn = null;
        try {
        conn = MySQLConexion.getConexion();
            String sql="UPDATE SERVICIO SET serv_codigo ='"+s.getServ_Codigo()+
            "', serv_tipo='"+s.getServ_Tipo()+"', serv_precio_base='"+s.getServ_PrecioBase()
                    +"'  WHERE serv_codigo='"+ s.getServ_Codigo()+"'";
        PreparedStatement st=conn.prepareStatement(sql);
        st.executeUpdate(); 
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }
    @Override
    public int listarCantidad2(String parametro_busqueda) {
        return listar(parametro_busqueda).size();
    }
    @Override
    public Servicio obtenerUno(String Codigo) {
        
        Servicio servicio = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM SERVICIO WHERE SERV_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, Codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                servicio = new Servicio();
                
                servicio.setServ_Codigo(rs.getString("serv_codigo"));
                servicio.setServ_Tipo(rs.getString("serv_tipo"));
                servicio.setServ_PrecioBase(rs.getDouble("serv_precio_base"));

               
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return servicio;
    }
    
    @Override
    public void actualizaUno(Servicio servicio) {
        
        int resp = 0;
        Connection conn = null;
        try {

            String sql = "UPDATE SERVICIO SET SERV_PRECIO_BASE = ? WHERE SERV_CODIGO = ?";
            conn = MySQLConexion.getConexion();


            PreparedStatement st = conn.prepareStatement(sql);

            st.setDouble(1, servicio.getServ_PrecioBase());
            st.setString(2, servicio.getServ_Codigo());

            resp = st.executeUpdate(); // ejecuta la accionde grabar.

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
        
        
    } 

    @Override
    public void eliminaUno(String codigo) {
        
        int resp = 0;
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM SERVICIO WHERE SERV_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            
            resp = st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (Exception e2) {
                
            }
        }
    }  


    
}