/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import Interfaces.Interface_Negocio_Pedido;
import Util.MySQLConexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Pedido;

public class PedidoDAO implements Interface_Negocio_Pedido{

    @Override
    public Pedido generarPedido() {
        Pedido p=new Pedido();
        Connection conn = null;
        ResultSet rs;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "INSERT INTO PEDIDO(PED_FECHA) VALUES (now());";
            PreparedStatement st = conn.prepareStatement(sql);
            st.executeUpdate();
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM PEDIDO WHERE PED_CODIGO =(SELECT MAX(PED_CODIGO ) FROM PEDIDO);");
            rs = ps.executeQuery();
            while (rs.next()) {
                p.setPedido_codigo(rs.getInt(1));
                p.setPed_fecha(rs.getDate(2));
            }
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }

        }
         return p;
    }
        
    @Override
    public void eliminar(int codigo) {
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "DELETE FROM pedido WHERE PED_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, codigo);
            st.executeUpdate(); 
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }

    @Override
    public Pedido listarUno(int codigo) {

        Pedido pedido = null;
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM Pedido where ped_codigo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, codigo);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                pedido = new Pedido();
                
                pedido.setPedido_codigo(rs.getInt(1));
                pedido.setPed_fecha(rs.getDate(2));
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return pedido;     
    }
        
}
