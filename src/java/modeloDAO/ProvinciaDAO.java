package modeloDAO;
import Interfaces.Interface_Provincia;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Provincia;
public class ProvinciaDAO implements Interface_Provincia {

    @Override
    public void agregarProvincia(String prov_nombre) {

        Connection conn = null;

        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_PROV(?);";
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, prov_nombre);

            ResultSet rs = st.executeQuery();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }

    @Override
    public List<Provincia> listar() {

        List<Provincia> lis = new ArrayList<>();
        Connection conn = null;

        try {

            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM PROVINCIA";
            PreparedStatement st = conn.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {

                Provincia provincia = new Provincia();

                provincia.setProv_codigo(rs.getString(1));
                provincia.setProv_nombre(rs.getString(2));

                lis.add(provincia);
            }
        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {

            try {

                if (conn != null) {
                    conn.close();
                }

            } catch (Exception e2) {

            }
        }

        return lis;
    }

    @Override
    public Provincia obtenerUno(String Codigo) {

        Provincia provincia = null;
        Connection conn = null;

        try {

            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM PROVINCIA WHERE PROV_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, Codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {

                provincia = new Provincia();

                provincia.setProv_codigo(rs.getString(1));
                provincia.setProv_nombre(rs.getString(2));

            }
        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {

            try {

                if (conn != null) {
                    conn.close();
                }

            } catch (Exception e2) {

            }
        }

        return provincia;
    }

    @Override
    public void eliminaUno(String codigo) {

        int resp = 0;
        Connection conn = null;

        try {

            String sql = "DELETE FROM PROVINCIA WHERE PROV_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);

            resp = st.executeUpdate();

        } catch (Exception ex) {

            ex.printStackTrace();

        } finally {

            try {

                if (conn != null) {
                    conn.close();
                }

            } catch (Exception e2) {

            }
        }
    }

    @Override
    public String buscarCodigo(String prov_nombre) {

        for (Provincia provincia : listar()) {
            if (provincia.getProv_nombre().equals(prov_nombre)) {
                return provincia.getProv_codigo();
            }
        }

        return null;
    }

    @Override
    public int listarCantidad() {
        return listar().size();
    }

    @Override
    public void actualizaUno(Provincia o) {

    }

}
