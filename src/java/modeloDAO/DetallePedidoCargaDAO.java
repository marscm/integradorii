package modeloDAO;

import Interfaces.Interface_Negocio_DetallePedidoCarga;
import Util.MySQLConexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Detalle_Pedido_Carga;

public class DetallePedidoCargaDAO implements Interface_Negocio_DetallePedidoCarga{

    @Override
    public Detalle_Pedido_Carga Generar(Detalle_Pedido_Carga dpc) {
        Connection conn = null;
        ResultSet rs;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "INSERT INTO detalle_pedido_carga(PC_CODIGO,ART_CODIGO,DPC_ART_ESP) VALUES (?,?,?);";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, dpc.getPc().getPC_codigo());
            st.setString(2, dpc.getArticulo().getArt_codigo());
            st.setString(3, dpc.getDPC_art_esp());
            st.executeUpdate();
            PreparedStatement ps = conn.prepareStatement("SELECT DPC_CODIGO FROM DETALLE_PEDIDO_CARGA WHERE DPC_CODIGO=(SELECT MAX(DPC_CODIGO) FROM DETALLE_PEDIDO_CARGA);");
            rs = ps.executeQuery();
            while (rs.next()) {
                dpc.setDPC_codigo(rs.getString(1));
            }
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }

        }
         return dpc;
    }

    @Override
    public void eliminar(int codigo) {
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM detalle_pedido_domicilio "
                    + "WHERE DPC_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, codigo);
            
            st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }

    @Override
    public void editar(Detalle_Pedido_Carga pc) {
        Connection conn = null;
        try {
        conn = MySQLConexion.getConexion();
            String sql="UPDATE DETALLE_PEDIDO_CARGA SET DPC_codigo ='"+pc.getDPC_codigo()+
            "', PC_CODIGO='"+pc.getPc().getPC_codigo()+"', ART_CODIGO='"+pc.getArticulo().getArt_codigo()+
                    "', DPC_ART_ESP='"+pc.getDPC_art_esp()+"'  WHERE DPC_codigo='"+ pc.getDPC_codigo()+"'";
        PreparedStatement st=conn.prepareStatement(sql);
        st.executeUpdate(); 
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }

    @Override
    public Object listarUno(int codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
