package modeloDAO;

import Interfaces.Interface_Negocio_DetallePedidoDomicilio;
import Util.MySQLConexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Detalle_Pedido_Domicilio;

public class DetallePedidoDomicilioDAO implements Interface_Negocio_DetallePedidoDomicilio{

    @Override
    public Detalle_Pedido_Domicilio Generar(Detalle_Pedido_Domicilio dpd) {
        Connection conn = null;
        ResultSet rs;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "INSERT INTO detalle_pedido_domicilio(PD_CODIGO,ART_CODIGO,DPD_ART_ESP,DPD_UBICACION,DPD_REFERENCIA) VALUES (?,?,?,?,?);";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, dpd.getPd().getPD_codigo());
            st.setString(2, dpd.getArticulo().getArt_codigo());
            st.setString(3, dpd.getDPD_art_esp());
            st.setString(4, dpd.getDPD_ubicacion());
            st.setString(5, dpd.getDPD_referencia());
            st.executeUpdate();
            PreparedStatement ps = conn.prepareStatement("SELECT DPD_CODIGO FROM DETALLE_PEDIDO_DOMICILIO WHERE DPD_CODIGO=(SELECT MAX(DPD_CODIGO) FROM DETALLE_PEDIDO_DOMICILIO);");
            rs = ps.executeQuery();
            while (rs.next()) {
                dpd.setDPD_codigo(rs.getString(1));
            }
        } catch (SQLException ex) {
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e2) {
            }

        }
         return dpd;
    }

    @Override
    public void eliminar(int codigo) {
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM detalle_pedido_domicilio "
                    + "WHERE DPD_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setInt(1, codigo);
            
            st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }

    @Override
    public void editar(Detalle_Pedido_Domicilio pd) {
        Connection conn = null;
        try {
        conn = MySQLConexion.getConexion();
            String sql="UPDATE Detalle_Pedido_Domicilio SET DPD_codigo ='"+pd.getDPD_codigo()+
            "', PD_CODIGO='"+pd.getPd().getPD_codigo()+"', ART_CODIGO='"+pd.getArticulo().getArt_codigo()+
                    "', DPD_ART_ESP='"+pd.getDPD_art_esp()+"', DPD_UBICACION='"+pd.getDPD_ubicacion()+
                    "', DPD_REFERENCIA='"+pd.getDPD_referencia()+
                    "'  WHERE DPD_codigo='"+ pd.getDPD_codigo()+"'";
        PreparedStatement st=conn.prepareStatement(sql);
        st.executeUpdate(); 
        } catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (SQLException e2) {
                
            }
        }
    }

    @Override
    public Object listarUno(int codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
