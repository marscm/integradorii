/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modeloDAO;

import Interfaces.Interface_Ruta;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Ruta;

public class RutaDAO implements Interface_Ruta {

    @Override
    public Ruta buscarRuta(String RUTA_ORIGEN, String RUTA_SEDE_ORIGEN, String RUTA_DESTINO, String RUTA_SEDE_DESTINO) {
        Ruta r=new Ruta(RUTA_ORIGEN,RUTA_SEDE_ORIGEN,RUTA_DESTINO,RUTA_SEDE_DESTINO);
        ResultSet rs;
        Connection conn = null;
        try{
            conn = MySQLConexion.getConexion();
            String sql = "SELECT RUTA_CODIGO , RUTA_PRECIO_BASE FROM RUTA WHERE "
                    + "RUTA_ORIGEN ='"+RUTA_ORIGEN+"' AND RUTA_SEDE_ORIGEN='"+RUTA_SEDE_ORIGEN+
                    "' AND RUTA_DESTINO='"+RUTA_DESTINO+"' AND RUTA_SEDE_DESTINO='"+RUTA_SEDE_DESTINO+"' ;";
            PreparedStatement st = conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                r.setRuta_codigo(rs.getString(1));
                r.setRuta_precio_base(rs.getDouble(2));
            }
            r.setRuta_origen(RUTA_ORIGEN);
            r.setRuta_sede_origen(RUTA_SEDE_ORIGEN);
            r.setRuta_destino(RUTA_DESTINO);
            r.setRuta_sede_destino(RUTA_SEDE_DESTINO);
        }
        catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (SQLException e2) {
                
            }
        }
        return r;
    }

    @Override
    public Ruta listarUno(String RUTA_COD) {
        ResultSet rs;
        Connection conn = null;
        Ruta r=new Ruta();
        try{
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM RUTA WHERE "
                    + "RUTA_CODIGO ="+RUTA_COD;
            PreparedStatement st = conn.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                r.setRuta_codigo(rs.getString(1));
                r.setRuta_origen(rs.getString(2));
                r.setRuta_sede_origen(rs.getString(3));
                r.setRuta_destino(rs.getString(4));
                r.setRuta_sede_destino(rs.getString(5));
                r.setRuta_precio_base(rs.getDouble(6));
            }
        }
        catch (SQLException ex) {
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (SQLException e2) {
                
            }
        }
        return r;
    }
    
    @Override
    public void agregarRuta(String ruta_origen, String ruta_sede_origen, String ruta_destino, String ruta_sede_destino, double ruta_precio_base){

        Connection conn = null;

        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_RUTA(?,?,?,?,?)";
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, ruta_origen);
            st.setString(2, ruta_sede_origen);
            st.setString(3, ruta_destino);
            st.setString(4, ruta_sede_destino);
            st.setDouble(5, ruta_precio_base);
            
            ResultSet rs = st.executeQuery();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    } 
    
    @Override
    public void actualizaUno(Ruta ruta) {
        int resp = 0;
        Connection conn = null;
        try {

            String sql = "UPDATE RUTA SET RUTA_PRECIO_BASE = ? WHERE RUTA_CODIGO = ?";
            conn = MySQLConexion.getConexion();


            PreparedStatement st = conn.prepareStatement(sql);

            st.setDouble(1, ruta.getRuta_precio_base());
            st.setString(2, ruta.getRuta_codigo());

            resp = st.executeUpdate(); // ejecuta la accionde grabar.

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }    
    
    // Lista todas las rutas
    @Override
    public List<Ruta> listar(){

        List<Ruta> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM RUTA";
            PreparedStatement st = conn.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Ruta ruta = new Ruta();
                
                ruta.setRuta_codigo(rs.getString("ruta_codigo"));
                ruta.setRuta_origen(rs.getString("ruta_origen"));
                ruta.setRuta_sede_origen(rs.getString("ruta_sede_origen"));
                ruta.setRuta_destino(rs.getString("ruta_destino"));
                ruta.setRuta_sede_destino(rs.getString("ruta_sede_destino"));
                ruta.setRuta_precio_base(rs.getDouble("ruta_precio_base"));
 
                lis.add(ruta);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
        
    }
    
    // lista todas las rutas con un filtro de busqueda rapida, le pasamos como parametro el codigo de ruta 
    @Override
    public List<Ruta> listar(String ruta_codigo){
        
        List<Ruta> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM RUTA WHERE RUTA_CODIGO LIKE ?";
            
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ruta_codigo + "%");
            
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Ruta ruta = new Ruta();
                
                ruta.setRuta_codigo(rs.getString("ruta_codigo"));
                ruta.setRuta_origen(rs.getString("ruta_origen"));
                ruta.setRuta_sede_origen(rs.getString("ruta_sede_origen"));
                ruta.setRuta_destino(rs.getString("ruta_destino"));
                ruta.setRuta_sede_destino(rs.getString("ruta_sede_destino"));
                ruta.setRuta_precio_base(rs.getDouble("ruta_precio_base"));
 
                lis.add(ruta);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    } 
    
    // Lista todas las rutas que tienen un mismo origen
    @Override
    public List<Ruta> listarSegunOrigen(String ruta_origen){
        
        List<Ruta> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM RUTA WHERE RUTA_ORIGEN = ?";
            
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ruta_origen);
            
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Ruta ruta = new Ruta();
                
                ruta.setRuta_codigo(rs.getString("ruta_codigo"));
                ruta.setRuta_origen(rs.getString("ruta_origen"));
                ruta.setRuta_sede_origen(rs.getString("ruta_sede_origen"));
                ruta.setRuta_destino(rs.getString("ruta_destino"));
                ruta.setRuta_sede_destino(rs.getString("ruta_sede_destino"));
                ruta.setRuta_precio_base(rs.getDouble("ruta_precio_base"));
 
                lis.add(ruta);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    } 

    // Lista todas las rutas que tienen un mismo destino
    @Override
    public List<Ruta> listarSegunDestino(String ruta_destino){
        
        List<Ruta> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM RUTA WHERE RUTA_DESTINO = ?";
            
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ruta_destino);
            
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Ruta ruta = new Ruta();
                
                ruta.setRuta_codigo(rs.getString("ruta_codigo"));
                ruta.setRuta_origen(rs.getString("ruta_origen"));
                ruta.setRuta_sede_origen(rs.getString("ruta_sede_origen"));
                ruta.setRuta_destino(rs.getString("ruta_destino"));
                ruta.setRuta_sede_destino(rs.getString("ruta_sede_destino"));
                ruta.setRuta_precio_base(rs.getDouble("ruta_precio_base"));
 
                lis.add(ruta);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    }
    
    @Override
    public Ruta obtenerUno(String ruta_codigo) {
        
        Ruta ruta = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM RUTA WHERE RUTA_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ruta_codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                ruta = new Ruta();
                
                ruta.setRuta_codigo(rs.getString("ruta_codigo"));
                ruta.setRuta_origen(rs.getString("ruta_origen"));
                ruta.setRuta_sede_origen(rs.getString("ruta_sede_origen"));
                ruta.setRuta_destino(rs.getString("ruta_destino"));
                ruta.setRuta_sede_destino(rs.getString("ruta_sede_destino"));
                ruta.setRuta_precio_base(rs.getDouble("ruta_precio_base"));
               
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return ruta;
        
        
    }    
    
      
    
    

    // Retorna la cantidad de rutas segun se lista por Origen o Destino
    @Override
    public int listarCantidad(String segun_origen_destino_codigo, String origen_destino_codigo){
        
        if(segun_origen_destino_codigo.equalsIgnoreCase("Origen"))
            return listarSegunOrigen(origen_destino_codigo).size();
        else 
            if(segun_origen_destino_codigo.equalsIgnoreCase("Destino"))
                return listarSegunDestino(origen_destino_codigo).size();
            else
                if(segun_origen_destino_codigo.equalsIgnoreCase("Codigo"))
                    return listar(origen_destino_codigo).size(); 
                else
                    return listar().size();
    }

    @Override
    public void eliminaUno(String ruta_codigo) {
        
        int resp = 0;
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM RUTA WHERE RUTA_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, ruta_codigo);
            
            resp = st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (Exception e2) {
                
            }
        }
    }

    @Override
    public int listarCantidad() {
        return 0;
    }
    
}
