
package modeloDAO;

import Interfaces.Interface_Provincia;
import Interfaces.Interface_Tienda;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.Provincia;
import modelo.Tienda;


public class TiendaDAO implements Interface_Tienda{
    
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;    
    
    Provincia provincia = new Provincia();
    
    
    @Override
    public void agregarTienda(String tiend_nombre, String tiend_ubicacion, String prov_codigo){

        Connection conn = null;

        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_TIEND(?,?,?);";
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, tiend_nombre);
            st.setString(2, tiend_ubicacion);
            st.setString(3, prov_codigo);
            
            ResultSet rs = st.executeQuery();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }

    @Override
    public List<Tienda> listar(){
        
        List<Tienda> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM TIENDA";
            PreparedStatement st = conn.prepareStatement(sql);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Tienda tienda = new Tienda();
                
                tienda.setTiend_codigo(rs.getString("tiend_codigo"));
                tienda.setTiend_nombre(rs.getString("tiend_nombre"));
                tienda.setTiend_ubicacion(rs.getString("tiend_ubicacion"));
                provincia = interface_provincia.obtenerUno(rs.getString("prov_codigo"));
                tienda.setProvincia(provincia);
                
                lis.add(tienda);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    }
    
    @Override
    public List<Tienda> listar(String prov_cod){
        
        List<Tienda> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM TIENDA WHERE PROV_CODIGO = ?";
            
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, prov_cod);
            
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Tienda tienda = new Tienda();
                
                tienda.setTiend_codigo(rs.getString("tiend_codigo"));
                tienda.setTiend_nombre(rs.getString("tiend_nombre"));
                tienda.setTiend_ubicacion(rs.getString("tiend_ubicacion"));
                provincia = interface_provincia.obtenerUno(rs.getString("prov_codigo"));
                tienda.setProvincia(provincia);
                
                lis.add(tienda);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    }
    
    @Override
    public List<Tienda> listar2(String parametro){
        
        List<Tienda> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
//            String sql = "SELECT * FROM TIENDA WHERE TIEND_CODIGO LIKE '%" + parametro + "%' OR TIEND_NOMBRE LIKE '%" + parametro + "%' TIEND_UBICACION LIKE '%" + parametro + "%'";
            String sql = "SELECT * FROM TIENDA WHERE TIEND_CODIGO LIKE ? OR TIEND_NOMBRE LIKE ? OR TIEND_UBICACION LIKE ?";
//             String sql = "SELECT * FROM TIENDA WHERE TIEND_CODIGO LIKE ?";
            
            
            PreparedStatement st = conn.prepareStatement(sql);
            
            
                        
            st.setString(1, "%" + parametro + "%");
            st.setString(2, "%" + parametro + "%");
            st.setString(3, "%" + parametro + "%");
            
            
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                Tienda tienda = new Tienda();
                
                tienda.setTiend_codigo(rs.getString("tiend_codigo"));
                tienda.setTiend_nombre(rs.getString("tiend_nombre"));
                tienda.setTiend_ubicacion(rs.getString("tiend_ubicacion"));
                provincia = interface_provincia.obtenerUno(rs.getString("prov_codigo"));
                tienda.setProvincia(provincia);
                
                lis.add(tienda);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;
    }
    
    @Override
    public Tienda obtenerUno(String Codigo){
        
        Tienda tienda = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM TIENDA WHERE TIEND_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, Codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                tienda = new Tienda();
                
                tienda.setTiend_codigo(rs.getString("tiend_codigo"));
                tienda.setTiend_nombre(rs.getString("tiend_nombre"));
                tienda.setTiend_ubicacion(rs.getString("tiend_ubicacion"));
                provincia = interface_provincia.obtenerUno(rs.getString("prov_codigo"));
                tienda.setProvincia(provincia);
               
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return tienda;
    } 
    
    public void eliminaUno(String codigo) {
        
        int resp = 0;
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM TIENDA WHERE TIEND_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            
            resp = st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (Exception e2) {
                
            }
        }
    }     
    
    @Override
    public int listarCantidad(){
        return listar().size();
    } 
    
    @Override
    public int listarCantidad(String prov_cod){
        return listar(prov_cod).size();
    }     

 
    
    @Override
    public int listarCantidad2(String parametro){
        return listar2(parametro).size();
    }
    
    
    @Override
    public void actualizaUno(Tienda tienda) {
       int resp = 0;
        Connection conn = null;
        try {

            String sql = "UPDATE TIENDA SET TIEND_CODIGO = ?, TIEND_NOMBRE = ?, TIEND_UBICACION = ?, PROV_CODIGO = ? WHERE TIEND_CODIGO = ?";
            conn = MySQLConexion.getConexion();


            PreparedStatement st = conn.prepareStatement(sql);

            st.setString(1, tienda.getTiend_codigo());
            st.setString(2, tienda.getTiend_nombre());
            st.setString(3, tienda.getTiend_ubicacion());
            st.setString(4, tienda.getProvincia().getProv_codigo());
            st.setString(5, tienda.getTiend_codigo());

            resp = st.executeUpdate(); // ejecuta la accionde grabar.

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }
    
}