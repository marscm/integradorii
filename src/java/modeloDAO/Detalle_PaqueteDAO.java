
package modeloDAO;

import Interfaces.Interface_Encomienda;
import Interfaces.Interface_Detalle_Paquete;

import modelo.DetallePaquete;

import Util.MySQLConexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.List;


public class Detalle_PaqueteDAO implements Interface_Detalle_Paquete{

    
    Interface_Encomienda interface_encomienda = new EncomiendaDAO();
    
    @Override
    public List<DetallePaquete> listar() {
        return null;
    }

    @Override
    public DetallePaquete obtenerUno(String Codigo) {
        
        DetallePaquete detalle_paquete = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ENCOMIENDA E, DETALLE_PAQUETE DP WHERE E.ENCO_CODIGO = DP.ENCO_CODIGO AND DP.ENCO_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            
            st.setString(1, Codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                detalle_paquete = new DetallePaquete();
                
                detalle_paquete.setDP_codigo(rs.getString("dp_codigo"));
                detalle_paquete.setDP_anchoMax(rs.getDouble("dp_anchomax_cm"));
                detalle_paquete.setDP_largoMax(rs.getDouble("dp_largomax_cm"));
                detalle_paquete.setDP_anchoMax(rs.getDouble("dp_altomax_cm"));
                detalle_paquete.setEncomienda(interface_encomienda.obtenerUno(rs.getString("enco_codigo")));
               

            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return detalle_paquete;
    }

    @Override
    public int listarCantidad() {
        return 0;
    }

    @Override
    public void eliminaUno(String codigo) {
        
    }

    @Override
    public void actualizaUno(DetallePaquete detalle_paquete) {

       int resp = 0;
        Connection conn = null;
        try {

            String sql = "UPDATE ENCOMIENDA\n"
                    + "INNER JOIN DETALLE_PAQUETE ON DETALLE_PAQUETE.ENCO_CODIGO = ENCOMIENDA.ENCO_CODIGO\n"
                    + "SET ENCOMIENDA.ENCO_PESOMAXGRAMOS = ?,\n"
                    + "    ENCOMIENDA.ENCO_PRECIO_BASE = ?,\n"
                    + "    DETALLE_PAQUETE.DP_ANCHOMAX_CM = ?,\n"
                    + "    DETALLE_PAQUETE.DP_LARGOMAX_CM = ?,\n"
                    + "    DETALLE_PAQUETE.DP_ALTOMAX_CM = ? \n"
                    + "WHERE ENCOMIENDA.ENCO_CODIGO = ?;";
            conn = MySQLConexion.getConexion();


            PreparedStatement st = conn.prepareStatement(sql);

            st.setDouble(1, detalle_paquete.getEncomienda().getEnco_pesoMaxGramos());
            st.setDouble(2, detalle_paquete.getEncomienda().getEnco_precioBase());
            st.setDouble(3, detalle_paquete.getDP_anchoMax());
            st.setDouble(4, detalle_paquete.getDP_largoMax());
            st.setDouble(5, detalle_paquete.getDP_altoMax());
            st.setString(6, detalle_paquete.getEncomienda().getEnco_codigo());

            resp = st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }          
    }

}