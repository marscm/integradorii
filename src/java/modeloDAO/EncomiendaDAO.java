
package modeloDAO;

import Interfaces.Interface_Encomienda;
import Interfaces.Interface_Negocio_Encomienda;
import Util.MySQLConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Encomienda;


public class EncomiendaDAO implements Interface_Negocio_Encomienda,Interface_Encomienda{

    @Override
    public List<Encomienda> listar() {
        Connection conn = null;
        List<Encomienda> lis = new ArrayList<>();
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM Encomienda";
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Encomienda enco = new Encomienda();
                enco.setEnco_codigo(rs.getString(1));
                enco.setEnco_tipo(rs.getString(2));
                enco.setEnco_pesoMaxGramos(rs.getDouble(3));
                enco.setEnco_precioBase(rs.getDouble(4));

                lis.add(enco);
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return lis;          
    }

    @Override
    public Encomienda listarUno(String codigo) {        
        Encomienda enco = null;
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM Encomienda where enco_codigo = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                enco = new Encomienda();
                enco.setEnco_codigo(rs.getString(1));
                enco.setEnco_tipo(rs.getString(2));
                enco.setEnco_pesoMaxGramos(rs.getDouble(3));
                enco.setEnco_precioBase(rs.getDouble(4));
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return enco;     
    } 

    @Override
    public Encomienda getEncomiendaByPeso(Double Peso) {
                Encomienda enco = null;
        Connection conn = null;
        try {
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ENCOMIENDA WHERE ENCO_PESOMAXGRAMOS = "
                    + "(SELECT MIN(ENCO_PESOMAXGRAMOS) FROM encomienda WHERE ENCO_PESOMAXGRAMOS >"+Peso+")";
            PreparedStatement st = conn.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                enco = new Encomienda();
                enco.setEnco_codigo(rs.getString(1));
                enco.setEnco_tipo(rs.getString(2));
                enco.setEnco_pesoMaxGramos(rs.getDouble(3));
                enco.setEnco_precioBase(rs.getDouble(4));        
            }
        } 
        catch (SQLException ex) {} 
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            }catch (SQLException e2) {}
        }
        return enco;     
    }

    @Override
    public void agregarEncomienda(String categoria, String enco_tipo, 
            double enco_PesoMaxGramos, double enco_precio_base, 
            double dp_AnchoMax_cm, double dp_LargoMax_cm, 
            double dp_AltoMax_cm) {
        Connection conn = null;

        try {
            conn = MySQLConexion.getConexion();
            String sql = "CALL SP_INSERT_ENCOMIENDA(?,?,?,?,?,?,?);";
            CallableStatement st = conn.prepareCall(sql);

            st.setString(1, categoria);
            st.setString(2, enco_tipo);
            st.setDouble(3, enco_PesoMaxGramos);
            st.setDouble(4, enco_precio_base);
            st.setDouble(5, dp_AnchoMax_cm);
            st.setDouble(6, dp_LargoMax_cm);
            st.setDouble(7, dp_AltoMax_cm);
            
            ResultSet rs = st.executeQuery();

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    
                    conn.close();
                }
                
            } catch (Exception e2) {
                
            }
        }
    }

    @Override
    public List<Encomienda> listar(String parametro_busqueda) {

         List<Encomienda> lis = new ArrayList<>();
        Connection conn = null;
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ENCOMIENDA WHERE ENCO_CODIGO LIKE ? OR ENCO_TIPO LIKE ?";
            PreparedStatement st = conn.prepareStatement(sql);

           
            st.setString(1, "%" + parametro_busqueda + "%");
            st.setString(2, "%" + parametro_busqueda + "%");
             ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                
                Encomienda encomienda = new Encomienda();
                
                encomienda.setEnco_codigo(rs.getString("enco_codigo"));
                encomienda.setEnco_tipo(rs.getString("enco_tipo"));
                encomienda.setEnco_pesoMaxGramos(rs.getDouble("enco_pesomaxgramos"));
                encomienda.setEnco_precioBase(rs.getDouble("enco_precio_base"));
                
                lis.add(encomienda);
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return lis;   
    }

    @Override
    public int listarCantidad(String parametro_busqueda) {
        return listar(parametro_busqueda).size();
    }

    @Override
    public Encomienda obtenerUno(String Codigo) {
         Encomienda encomienda = null;
        Connection conn = null;
        
        
        try {
            
            conn = MySQLConexion.getConexion();
            String sql = "SELECT * FROM ENCOMIENDA WHERE ENCO_CODIGO = ?";
            PreparedStatement st = conn.prepareStatement(sql);
            
            st.setString(1, Codigo);

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                
                encomienda = new Encomienda();
                
                encomienda.setEnco_codigo(rs.getString("enco_codigo"));
                encomienda.setEnco_tipo(rs.getString("enco_tipo"));
                encomienda.setEnco_pesoMaxGramos(rs.getDouble("enco_pesomaxgramos"));
                encomienda.setEnco_precioBase(rs.getDouble("enco_precio_base"));
               
            }
        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            }catch (Exception e2) {
                
            }
        }

        return encomienda;
    }

    @Override
    public int listarCantidad() {
        return listar().size();
    }

    @Override
    public void eliminaUno(String codigo) {
         int resp = 0;
        Connection conn = null;
        
        try {

            String sql = "DELETE FROM ENCOMIENDA WHERE ENCO_CODIGO = ?";
            conn = MySQLConexion.getConexion();

            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, codigo);
            
            resp = st.executeUpdate(); 

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            
            try {

                if (conn != null) {
                    conn.close();
                }
                
            } catch (Exception e2) {
                
            }
        } 
    }

    @Override
    public void actualizaUno(Encomienda encomienda) {
         int resp = 0;
        Connection conn = null;
        try {

            String sql = "UPDATE ENCOMIENDA SET ENCO_PESOMAXGRAMOS = ?, ENCO_PRECIO_BASE = ? WHERE ENCO_CODIGO = ?";
            conn = MySQLConexion.getConexion();


            PreparedStatement st = conn.prepareStatement(sql);

            st.setDouble(1, encomienda.getEnco_pesoMaxGramos());
            st.setDouble(2, encomienda.getEnco_precioBase());
            st.setString(3, encomienda.getEnco_codigo());

            resp = st.executeUpdate(); // ejecuta la accionde grabar.

        } catch (Exception ex) {
            
            ex.printStackTrace();
            
        } finally {
            try {

                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e2) {
            }
        }
    }
}
