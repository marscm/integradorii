CREATE TABLE `Pedido` (
  `Ped_Codigo` int AUTO_INCREMENT,
  `Ped_Fecha` datetime,
  PRIMARY KEY (`Ped_Codigo`)
);

CREATE TABLE `Servicio` (
  `Serv_Id` char (10),
  `Serv_Tipo` varchar (30),
  `Serv_Precio_Base` decimal (9,2),
  PRIMARY KEY (`Serv_Id`)
);
CREATE TABLE `Encomienda_Tipo` (
  `Encomiend_Id` char (10),
  `Encomiend_Tipo` varchar (50),
  `Encomiend_Precio_Base` decimal (9,2),
  PRIMARY KEY (`Encomiend_Id`)
);
CREATE TABLE `Artículo` (
  `Articulo_ID` char (10),
  `Descripcion` varchar (50),
  PRIMARY KEY (`Articulo_ID`)
);

CREATE TABLE `Ruta` (
  `Ruta_Codigo` char (10),
  `Ruta_Origen` varchar (50),
  `Ruta_Sede_Origen` varchar (50),
  `Ruta_Destino` varchar (50),
  `Ruta_Sede_Destino` varchar (50),
  `Ruta_Precio_Base` decimal (9,2),
  PRIMARY KEY (`Ruta_Codigo`)
);

CREATE TABLE `Persona` (
  `Pers_Dni` int (9),
  `Pers_Nombres` varchar (50),
  `Pers_Apellidos` varchar (50),
  PRIMARY KEY (`Pers_Dni`)
);


CREATE TABLE `Cliente` (
  `Client_Codigo` int auto_increment,
  `Pers_Dni` int (9),
  PRIMARY KEY (`Client_Codigo`),
  FOREIGN KEY (`Pers_Dni`) REFERENCES `Persona`(`Pers_Dni`)
);

CREATE TABLE `Destinatario` (
  `Destinatario_Codigo` int AUTO_INCREMENT not null,
  `Pers_Dni` int (9) not null,
  PRIMARY KEY (`Destinatario_Codigo`),
  FOREIGN KEY (`Pers_Dni`) REFERENCES `Persona`(`Pers_Dni`)
);

CREATE TABLE `Pedidos_Cliente` (
  `Client_Codigo` int,
  `Ped_Codigo` int,
  `Pc_Celular` int (10),
  `Pc_Correo` varchar (100),
  PRIMARY KEY (`Client_Codigo`, `Ped_Codigo`),
  FOREIGN KEY (`Client_Codigo`) REFERENCES `Cliente`(`Client_Codigo`),
  FOREIGN KEY (`Ped_Codigo`) REFERENCES `Pedido`(`Ped_Codigo`)
);

CREATE TABLE `Envios_Destinatario` (
  `Destinatario_Codigo` int,
  `Ped_Codigo` int,
  `Ed_Celular` int,
  PRIMARY KEY (`Destinatario_Codigo`, `Ped_Codigo`),
  FOREIGN KEY (`Destinatario_Codigo`) REFERENCES `Destinatario`(`Destinatario_Codigo`),
  FOREIGN KEY (`Ped_Codigo`) REFERENCES `Pedido`(`Ped_Codigo`)
);
CREATE TABLE `Pedido_Domicilio` (
  `PD_Codigo` int AUTO_INCREMENT,
  `Ruta_Codigo` char (10),
  `Serv_Id` char (10),
  `Encomiend_Id` char (10),
  `Ped_Codigo` int,
  `PC_Tarifa_Final` decimal (9,2),
  PRIMARY KEY (`PD_Codigo`),
  FOREIGN KEY (`Ruta_Codigo`) REFERENCES `Ruta`(`Ruta_Codigo`),
  FOREIGN KEY (`Serv_Id`) REFERENCES `Servicio`(`Serv_Id`),
  FOREIGN KEY (`Encomiend_Id`) REFERENCES `Encomienda_Tipo`(`Encomiend_Id`),
  FOREIGN KEY (`Ped_Codigo`) REFERENCES `Pedido`(`Ped_Codigo`)
);
CREATE TABLE `Pedido_Carga` (
  `PC_Codigo` int AUTO_INCREMENT,
  `Ruta_Codigo` char (10),
  `Serv_Id` char (10),
  `Encomiend_Id` char (10),
  `Ped_Codigo` int,
  `PC_Tarifa_Final` decimal (9,2),
  PRIMARY KEY (`PC_Codigo`),
  FOREIGN KEY (`Ruta_Codigo`) REFERENCES `Ruta`(`Ruta_Codigo`),
  FOREIGN KEY (`Serv_Id`) REFERENCES `Servicio`(`Serv_Id`),
  FOREIGN KEY (`Encomiend_Id`) REFERENCES `Encomienda_Tipo`(`Encomiend_Id`),
  FOREIGN KEY (`Ped_Codigo`) REFERENCES `Pedido`(`Ped_Codigo`)
);

CREATE TABLE `Detalle_Pedido_Domicilio` (
  `PD_Codigo` int,
  `Articulo_ID` char (10),
  `DPC_Artc_Esp` varchar (100),
  `PD_Ubicacion` varchar (100),
  `PD_Referencia` varchar (100),
  PRIMARY KEY (`PD_Codigo`, `Articulo_ID`),
  FOREIGN KEY (`PD_Codigo`) REFERENCES `Pedido_Domicilio`(`PD_Codigo`),
  FOREIGN KEY (`Articulo_ID`) REFERENCES `Artículo`(`Articulo_ID`)
);
CREATE TABLE `Detalle_Pedido_Carga` (
  `PC_Codigo` int,
  `Articulo_ID` char (10),
  `DPC_Artc_Esp` varchar (100),
  PRIMARY KEY (`PC_Codigo`, `Articulo_ID`),
  FOREIGN KEY (`PC_Codigo`) REFERENCES `Pedido_Carga`(`PC_Codigo`),
  FOREIGN KEY (`Articulo_ID`) REFERENCES `Artículo`(`Articulo_ID`)
);


CREATE TABLE PROVINCIA(
  ID_PROVINCIA INT(11) NOT NULL AUTO_INCREMENT,
  PROVINCIA VARCHAR(60) NOT NULL,
  PRIMARY KEY (ID_PROVINCIA)
)

create table DISTRITO(
  ID_DISTRITO INT(11) NOT NULL AUTO_INCREMENT,
  DISTRITO VARCHAR(30) NOT NULL,
  ID_PROVINCIA INT(11) NOT NULL,
  PRIMARY KEY (ID_DISTRITO),
  FOREIGN KEY (ID_PROVINCIA) REFERENCES PROVINCIA (ID_PROVINCIA)
);



INSERT INTO PROVINCIA (PROVINCIA) VALUES('BAGUA')
INSERT  INTO DISTRITO (DISTRITO,ID_PROVINCIA) VALUES ('CHISQUILLA',2),
('CHURUJA',2),('COROSHA',2),('CUISPES',2),('FLORIDA',2),('JAZAN',2)

INSERT  INTO DISTRITO (DISTRITO,ID_PROVINCIA) VALUES ('ARAMANGO',1),('COPALLIN',1),('EL PARCO',1),('IMAZA',1),('LA PECA',1)

INSERT INTO `encomienda_tipo`(`Encomiend_Id`, `Encomiend_Tipo`, `Encomiend_Precio_Base`) VALUES ('E01','Minipaqueteria','3'), ('E02','Paquete mediano','2'),('E03','Paquete grande','4')

INSERT INTO `servicio`(`Serv_Id`, `Serv_Tipo`, `Serv_Precio_Base`) VALUES ('S001','Carga','1'), ('S002','Domicilio','2')

INSERT INTO `artículo`(`Articulo_ID`, `Descripcion`) VALUES ('A001','Paqueteria'),('A002','Sobres')


DELIMITER @@
CREATE PROCEDURE SP_INSERT_SERVICIO(serv_tipo VARCHAR(30), SERV_PRECIO_BASE DECIMAL(5,2))
BEGIN
DECLARE CONTADOR INT;
DECLARE CODIGO CHAR(5);
SELECT RIGHT (MAX(serv_CODIGO),3) + 1 INTO CONTADOR FROM servicio;
SET CODIGO = CONCAT('S',LPAD(CONTADOR,3,'0'));
INSERT INTO servicio VALUES (cODIGO,SERV_TIPO,SERV_PRECIO_BASE);
END @@


DELIMITER @@
CREATE PROCEDURE SP_INSERT_ARTICULO(ART_CODIGO CHAR(5), ART_DESCRIPCION VARCHAR(50))
BEGIN
DECLARE CONTADOR INT;
DECLARE CODIGO CHAR(5);
SELECT RIGHT (MAX(ART_CODIGO),3) + 1 INTO CONTADOR FROM ARTICULO;
SET CODIGO = CONCAT('A',LPAD(CONTADOR,3,'0'));
INSERT INTO ARTICULO VALUES (CODIGO,ART_DESCRIPCION);
END @@


DELIMITER @@
CREATE PROCEDURE SP_INSERT_DETALLE_PAQUETE(DP_ANCHOMAX_CM decimal(4,2), DP_LARGOMAX_CM decimal(4,2), DP_ALTOMAX_CM decimal(4,2), ENCO_CODIGO char(5))
BEGIN
DECLARE CONTADOR INT;
DECLARE CODIGO CHAR(5);
SELECT RIGHT (MAX(DP_CODIGO),3) + 1 INTO CONTADOR FROM DETALLE_PAQUETE;
SET CODIGO = CONCAT('DP',LPAD(CONTADOR,3,'0'));
INSERT INTO DETALLE_PAQUETE VALUES (CODIGO,DP_ANCHOMAX_CM, DP_LARGOMAX_CM, DP_ALTOMAX_CM, ENCO_CODIGO);
END @@


DELIMITER @@
CREATE PROCEDURE SP_INSERT_ENCOMIENDA(ENCO_TIPO varchar(50), ENCO_PESOMAXGRAMOS decimal(5,2), ENCO_PRECIO_BASE decimal(5,2))
BEGIN
DECLARE CONTADOR INT;
DECLARE CODIGO CHAR(5);
SELECT RIGHT (MAX(ENCO_CODIGO),3) + 1 INTO CONTADOR FROM ENCOMIENDA;
SET CODIGO = CONCAT('E',LPAD(CONTADOR,3,'0'));
INSERT INTO ENCOMIENDA VALUES (CODIGO,ENCO_TIPO, ENCO_PESOMAXGRAMOS, ENCO_PRECIO_BASE);
END @@


CREATE TABLE DETALLE_PEDIDO_DOMICILIO(
    DPD_CODIGO int(9) NOT NULL AUTO_INCREMENT,
    PD_CODIGO CHAR(255) NOT NULL,
    ART_CODIGO CHAR(5) NOT NULL,
    DPD_ART_ESP VARCHAR(100),
    DPD_UBICACION VARCHAR (100) NOT NULL,
    DPD_REFERENCIA VARCHAR(100) NOT NULL,
    PRIMARY KEY (DPD_CODIGO),
    FOREIGN KEY (PD_CODIGO) REFERENCES PEDIDO_DOMICILIO (PD_CODIGO) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (ART_CODIGO) REFERENCES ARTICULO (ART_CODIGO) ON DELETE CASCADE ON UPDATE CASCADE
);