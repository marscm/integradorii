
<%@page import="Interfaces.Interface_Detalle_Paquete"%>

<%@page import="modeloDAO.Detalle_PaqueteDAO"%>

<%@page import="modelo.DetallePaquete"%>

<%@page import="com.google.gson.Gson"%>


<%  
    
    Interface_Detalle_Paquete interface_detalle_paquete = new Detalle_PaqueteDAO();

    String accion = request.getParameter("action");
    
    if (!accion.isEmpty()) {

        if (accion.equals("updatePaquete")) {
            
            String enco_codigo = request.getParameter("enco_codigo");
            DetallePaquete detalle_paquete = interface_detalle_paquete.obtenerUno(enco_codigo);

            if(detalle_paquete != null){
                
                String json = new Gson().toJson(detalle_paquete);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                    
            }
        }

    }


%>
