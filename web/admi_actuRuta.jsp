<%-- 
    Document   : admi_actuRuta
    Created on : 25/10/2021, 08:25:39 PM
    Author     : Dell
--%>

<%@page import="modelo.Ruta"%>
<%@page import="modelo.Ruta"%>
<%@page import="modelo.Provincia"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.RutaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Ruta"%>
<%@page import="ModeloDAO.ProvinciaDAO"%>
<%@page import="ModeloDAO.RutaDAO"%>
<%@page import="Modelo.Provincia"%>
<%@page import="Modelo.Ruta"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial- ">
	<title>Adminitrador</title>

	<!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
	<link rel="shortcut icon" href="#" type="image/png">

	<!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

	<!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
	<link rel="stylesheet" type="text/css" href="css/estilo_admi.css">
        <script type="text/javascript" src="js/jquery-3.6.0.min.js"> </script>

</head>
<%  
    Interface_Provincia interface_provincia = new ProvinciaDAO();
    Interface_Ruta interface_ruta = new RutaDAO();
%>

<body id="body-pd">

	<!--========= NAVAR 1=================================================-->
	<div class="l-navbar" id="navbar">
		<nav class="nav">
			<div>
				<div class="nav_brand">
					<i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
					<a href="" class="nav_logo">Fast Courier</a>
				</div>
				<div class="nav_list">
					<div class="nav_link active desplegue">
						<i class="nav_icon ri-truck-fill"></i>
						<span class="nav_name">Ruta</span>	
						<i class="ri-arrow-down-s-line desplegue_link"></i>		

						<ul class="desplegue_menu">
							<a href="admi_actuRuta.html" class="desplegue_sublink">Actualizaciones</a>
							<a href="admi_tiendaRuta.html" class="desplegue_sublink">Tiendas</a>
						</ul>			
					</div>
					<a href="" class="nav_link">
						<i class="nav_icon ri-service-fill"></i>
						<span class="nav_name">Servicios</span>						
					</a>
					<a href="" class="nav_link">
						<i class="nav_icon ri-article-fill"></i>
						<span class="nav_name">Articulo</span>						
					</a>
					<a href="" class="nav_link">
						<i class="nav_icon ri-red-packet-fill"></i>
						<span class="nav_name">Encomienda</span>						
					</a>
					<a href="" class="nav_link">
						<i class="nav_icon ri-home-smile-fill"></i>
						<span class="nav_name">Resumen</span>						
					</a>
				</div>
			</div>

			<a href="" class="nav_link">
				<i class="nav_icon ri-logout-circle-r-fill"></i>
				<span class="nav_name">Salir</span>						
			</a>
		</nav>
	</div>
<!--========= NAVAR 2 TITLE ========================================-->

	<div class="main">
		<div class="nav_vertical">
			<div class="nav_start">
				<h1 class="nav_vertical_title">Actualizar Rutas</h1>
			</div>
			<div class="nav_title">
				<div class="nav_title_buscar">
					<i class="icon_buscar ri-search-2-line"></i>
					<input type="text" name="" placeholder="Buscar Ruta">
				</div>
				<button class="button_normal">Ver Web</button>
				<div class="nav_title_userAdmi">
					<img class="img_userAdmi" src="img/img_userAdmi.jpg">
					<div class="nav_userAdmi_cont">
						<h3>Adminitrador</h3>
						<span>Super Admi</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--========= CONTENIDO =============================================-->
<!--TOTALES Y FILTRO -------------------------->
<main>
<div class="total_filtro">
	<section class="total_rutas">
		<div class="total_info">
			<p class="total_num">240</p>
			<span>Total de Tiendas</span>
		</div>
		<i class=" icon_total ri-store-2-fill"></i>
	</section>

	<section class="total_rutas">
		<div class="total_info">
			<p class="total_num">150</p>
			<span>Total de Rutas</span>
		</div>
		<i class="icon_total ri-route-fill"></i>
	</section>

	<section class="filtro_rutas">
		<div class="filtro_rutas_name filtro_origen">
			<h3>Elige Origen</h3>
			<select class="filtro_rutas_opcion" name="Provincia">
				<option>Lima</option>
				<option>Ayacucho</option>
			</select>
		</div>
		<div class="filtro_rutas_name filtro_destino">
			<h3>Elige Destino</h3>
			<select class="filtro_rutas_opcion" name="Provincia">
				<option>Lima</option>
				<option>Ayacucho</option>
			</select>
		</div>
	</section>
</div>
	
    <!--TABLA -------------------------->
    <div class="tabla">
        <section class="tabla_capa">
            <div class="tabla_ruta">
                <table class="tabla_ruta_cont">
                    <thead>
                        <tr> 
                            <td>  </td>
                            <td>  </td>
                            <td>Código</td>
                            <td>Origen</td>
                            <td>Sede de Origen</td>
                            <td>Destino</td>
                            <td>Sede de Destino</td>
                            <td>Precio</td>
                        </tr>
                    </thead>
                    <tbody>
                       <%
                            for (Ruta ruta: interface_ruta.listar()) {
                        %>         
                        <tr>
                            <td><a href="Control_Ruta?accion=eliminaRuta&ruta_codigo=<%=ruta.getRuta_codigo() %>"><i class="icon_close ri-close-circle-fill"></i></a></td>
                            <td><i class="icon_edit ri-edit-2-fill"></i></td>
                            <td><%= ruta.getRuta_codigo() %></td>
                            <td><%= ruta.getRuta_origen() %></td>
                            <td><%= ruta.getRuta_sede_origen() %></td>
                            <td><%= ruta.getRuta_destino() %></td>
                            <td><%= ruta.getRuta_sede_destino() %></td>
                            <td><%= ruta.getRuta_precio_base() %></td>
                        </tr>
                        <%
                            }
                        %> 
                    </tbody>
                </table>
            </div>
            <a href="#seccion-add"><i class="icon_add ri-add-circle-fill" id="icon-add"  ></i></a>
        </section>

    </div>

<!--TABLA ADD ------------------------------------------>
<div class="tabla_ruta_add" id="seccion-add">
    <section >
        <div class="tabla_add ">
            <form id="">
            <table class="tabla_add_cont">
                <thead >
                    <tr> 
                        <td>  </td>
                      
                        <td>Origen</td>
                        <td>Sede de Origen</td>
                        <td>Destino</td>
                        <td>Sede de Destino</td>
                        <td>Precio</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><button class="button_add" name="accion" type="submit" value="agregarRuta">Agregar</button></td>
                        <td>
                            <form id="id_frmProvincia1"> 
                                <select class="add_rutas_opcion" name="cmbProvincia1" onchange="llenaCmbTienda()">
                                    <option>Elegir</option>
                                <%
                                    for(Provincia provincia: interface_provincia.listar()){
                                %>         
                                    <option> <%= provincia.getProv_nombre() %> </option>  
                                <%
                                    }
                                %> 
                                </select>
                            </form>
                        </td>
                        <td>
                            <select class="add_rutas_opcion" name="Provincia" id="id_cmbTienda1">
                                <option> Elegir </option>
                            </select>
                        </td>
                        <td>
                            <form id="id_frmProvincia2">
                            <select class="add_rutas_opcion" name="cmbProvincia2" onchange="llenaCmbTienda2()">
                                <option>Elegir</option>
                            <%
                                for(Provincia provincia: interface_provincia.listar()){
                            %>         
                                <option> <%= provincia.getProv_nombre() %> </option>  
                            <%
                                }
                            %> 
                            </select>
                            </form>
                        </td>
                        <td>
                            <select class="add_rutas_opcion" name="Provincia" id="id_cmbTienda2">
                                <option>Elegir</option>
                            </select>
                        </td>
                        <td><input type="text" name="precio" placeholder="s/100.00"></td>
                    </tr>
                </tbody>    
            </table>
            </form>
        </div>
    </section>
</div>
</main>

	<!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="js/scrollreveal.min.js"></script>

	<!--========= NUESTRO JS (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="js/main_admi.js"></script>
        
        <script type="text/javascript">
            
            function llenaCmbTienda(){
                
                $.post("JQuery_admi_actuRuta/cmbProvincia1.jsp",$("#id_frmProvincia1").serialize(),function(data){$("#id_cmbTienda1").html(data);});
            }
            
            function llenaCmbTienda2(){
                
                $.post("JQuery_admi_actuRuta/cmbProvincia2.jsp",$("#id_frmProvincia2").serialize(),function(data){$("#id_cmbTienda2").html(data);});
            }            
            

            
        </script>
        
	
</body>
</html>