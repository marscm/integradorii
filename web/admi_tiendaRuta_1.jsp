

<%@page import="Interfaces.Interface_Generica"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Tienda"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.TiendaDAO"%>
<%@page import="modelo.Provincia"%>
<%@page import="modelo.Tienda"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial- ">
	<title>Adminitrador</title>

	<!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
	<link rel="shortcut icon" href="#" type="image/png">

	<!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

	<!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
	<link rel="stylesheet" type="text/css" href="resources/css/estilo_admi.css">
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi_2.css">
        <script type="text/javascript" src="resources/js/jquery-3.6.0.min.js"> </script>
</head> 

<div class="modal_tienda" >
    <div class="bodyModalTienda"> 
        <section>
            <form action="Control_Tienda" method="post" name="form_update_tienda" id="form_update_tienda"  >
            <h1> ACTUALIZAR TIENDA </h1>
            <h2 class="tiendNombre"> </h2> <h2 class="tiendProvincia"> </h2>
            Nombre: <input type="text" name="update_nombre" id="update_nombre">  <br> <!-- NOMBRE -->
            Ubicaci�n: <input type="text" name="update_ubicacion" id="update_ubicacion"> <br><!-- UBICACION -->
            C�digo de Provincia: <input type="text" name="update_provincia" id="update_provincia"><!-- CODIGO DE PROVINCIA -->
            <input type="hidden" name="up_tienda_codigo" id="up_tienda_codigo" required>

            <div class="alertUpdateTienda">  </div>
            <button type="submit" name="accion" value="actualizarTienda"> Actualizar </button>
            <a href="#" class="closeModalTienda" onclick="closeModal()"> Cerrar </a>
        </form>
        </section>
    </div>              
</div> 


<%  
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;
    
    TiendaDAO tiendaDAO = new TiendaDAO();
    Interface_Tienda interface_tienda = tiendaDAO;
%>

<body id="body-pd">    
    
   

	<!--========= NAVAR 1=================================================-->
	<div class="l-navbar" id="navbar">
		<nav class="nav">
			<div>
				<div class="nav_brand">
					<i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
					<a href="" class="nav_logo">Fast Courier</a>
				</div>
				<div class="nav_list">
					<div class="nav_link active desplegue">
						<i class="nav_icon ri-truck-fill"></i>
						<span class="nav_name">Ruta</span>	
						<i class="ri-arrow-down-s-line desplegue_link"></i>		

						<ul class="desplegue_menu">
							<a href="admi_actuRuta_1.jsp" class="desplegue_sublink">Actualizaciones</a>
							<a href="admi_tiendaRuta_1.jsp" class="desplegue_sublink">Tiendas</a>
						</ul>			
					</div>
					<a href="admi_servicio.jsp" class="nav_link">
						<i class="nav_icon ri-service-fill"></i>
						<span class="nav_name">Servicios</span>						
					</a>
					<a href="admi_articulo.jsp" class="nav_link">
						<i class="nav_icon ri-article-fill"></i>
						<span class="nav_name">Articulo</span>						
					</a>
					<a href="admi_encomienda.jsp" class="nav_link">
						<i class="nav_icon ri-red-packet-fill"></i>
						<span class="nav_name">Encomienda</span>						
					</a>
				</div>
			</div>

			<a href="" class="nav_link">
				<i class="nav_icon ri-logout-circle-r-fill"></i>
				<span class="nav_name">Salir</span>						
			</a>
		</nav>
	</div>
<!--========= NAVAR 2 TITLE ========================================-->
	<div class="main">
		<div class="nav_vertical">
			<div class="nav_start">
				<h1 class="nav_vertical_title">Nuestras Tiendas</h1>
			</div>
			<div class="nav_title">
				<div class="nav_title_buscar">
					<i class="icon_buscar ri-search-2-line"></i>
					<input type="text" name="" placeholder="Buscar">
				</div>
				<a href="index.jsp"><button class="button_normal">Ver Web </button></a>
				<div class="nav_title_userAdmi">
					<img class="img_userAdmi" src="resources/img/icon-5359553_960_720.png">
					<div class="nav_userAdmi_cont">
						<h3>Adminitrador</h3>
						<span>Super Admi</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--========= CONTENIDO =============================================-->


<main>
<div class="nuestraTienda">
	<div class="tienda_tablaTotales">
		<!--TOTALES Y FILTRO -------------------------------------------------->
		<div class="tienda_totales">
                        <section class="tienda_total_cont">
                                <div class="total_info" id="idTotalProvincias">
                                    <%
                                        int TotalProvincia = interface_provincia.listarCantidad();
                                    %>    
                                    <p class="total_num"><%= TotalProvincia %></p>
                                    <span>Total de Provincias</span>
				</div>
				<i class=" icon_total ri-landscape-fill"></i>
			</section>
                        <section class="tienda_total_cont">
                                <div class="total_info" id="idTotalTiendas">
                                <%
                                    int TotalTiendas = interface_tienda.listarCantidad();
                                %>      
                                    <p class="total_num"><%= TotalTiendas %></p>
                                    <span>Total Sede de Rutas</span>
				</div>
				<i class="icon_total ri-pin-distance-fill"></i>
			</section>
		</div>
		<!--FITRO BUSCAR---------------------------------------------------->
		<div class="tienda_filtroBusca">
			<section class="tienda_filtroProv_cont">
				<i class="icon_buscar ri-search-2-line"></i>
				<div class="filtro_tienda_name filtro_provincia">
					<h3>Provincia</h3>
                                        <form name="frmProvincia" id="idfrmProvincia" method="post">
                                            <select class="filtro_tienda_opcion" name="cmbProvincia" onchange="listarConSelect()">
                                                <option> Todas </option> 
                                                <%
                                                    for(Provincia provincia: interface_provincia.listar()){
                                                %>         
                                                    <option><%= provincia.getProv_nombre() %></option> 
                                                <%
                                                    }
                                                %> 
                                            </select>
                                        </form>    
				</div>
				<div class="filtro_tienda_name filtro_tienda">
					<h3>Tienda</h3>
                                        <form name="BuscarTienda" id="idBuscarTienda" method="post">
                                        <input type="text" placeholder="Buscar Tienda" id="idtxtTienda" name="txtTienda" onkeyup="busqueda()">
                                        </form>
				</div>
			</section>
		</div>
		<!--TABLAS Y PROINCIAS ---------------------------------------------------->
                <div class="tienda_tablas">
			<!--Tabla 1 ---------------------------------------------------->
                        <div class="tabla">
				<section class="tabla_capa">
					<div class="tabla_tienda">
                                            <table class="tabla_tienda_cont tienda" id="idtblProvincias" name="tbl_provincia">
							<thead>
								<tr> 
                                                                        <td>  </td>
									<td>C�digo</td>
									<td>Provincia</td>
								</tr>
							</thead>
                                                        <tbody>
                                                            <%
                                                                for(Provincia provincia: interface_provincia.listar()){
                                                            %>         
								<tr>
                                                                    <td><a href="Control_Provincia?accion=eliminar&aprovcodigo=<%=provincia.getProv_codigo()%>"><i class="icon_close ri-close-circle-fill"></i></a></td>
                                                                    <td><%= provincia.getProv_codigo() %></td>
                                                                    <td><%= provincia.getProv_nombre() %></td>
								</tr>
                                                            <%
                                                                }
                                                            %>                      
							</tbody>
                                            </table>
					</div>	
				</section>
			</div>

			<!--Tabla 2 ---------------------------------------------------->
			<div class="tabla">
			<section class="tabla_capa">
                        <div class="tabla_tienda" id="iddivTienda">
                            <table class="tabla_tienda_cont tienda_prov" id="idtblTiendas">
					<thead>
						<tr> 
                                                        <td>  </td>
							<td>  </td>
							<td>C�digo</td>
							<td>Tienda</td>
                                                        <td>Ubicaci�n</td>
							<td>Provincia</td>
						</tr>
					</thead>
					<tbody> 
                                            <%
                                                for(Tienda tienda: interface_tienda.listar()){
                                            %>         
						<tr>
                                                    <td><a href="Control_Tienda?accion=eliminar&atiendcodigo=<%=tienda.getTiend_codigo()%>"><i class="icon_close ri-close-circle-fill"></i></a></td>
                                                    <td><a class="tienda_codigo" id="tienda-codigo" codigo="<%= tienda.getTiend_codigo()%>" href="#"><i class="ri-edit-fill"></i></a></td>
                                                        <td><%= tienda.getTiend_codigo() %></td>
							<td><%= tienda.getTiend_nombre() %></td>
                                                        <td><%= tienda.getTiend_ubicacion() %></td>
							<td><%= tienda.getProvincia().getProv_nombre() %></td>
						</tr>
                                            <%
                                                }
                                            %>                                             
					</tbody>
                            </table>
			</div>			
			</section>
		</div>
            </div>
	</div>

	<!--TABLAS ADICIONAR ---------------------------------------------------->
	<div class="tienda_adicionar">
		<section class="add_seccion adicionar_prov">
                    <form action="Control_Provincia" method="post">
			<h2><b>Nuevas<br>Provincias</b></h2>
			<h3>Provincia</h3>
			<input type="text" name="txtprov_nombre" placeholder="Ayacucho">
                        <button type="submit" class="button_add" name="accion" value="agregarProvincia">Agregar</button>
                    </form>    
		</section>
		<section class="add_seccion adicionar_tienda">
                    <form action="Control_Tienda" method="post">
			<h2><b>Nuevas<br>Tiendas</b></h2>
			<h3>Provincia</h3>
			<select class="filtro_provincia_opcion" name="cmbprov_nombre">
                            <%
                                for(Provincia provincia: interface_provincia.listar()){
                            %>         
                                <option><%= provincia.getProv_nombre() %></option> 
                            <%
                                }
                            %>   
			</select>
			<h3>Nueva Tienda</h3>
			<input type="text" name="txttiend_nombre" placeholder="Tienda Molina">
                        <h3>Ubicaci�n</h3>
                        <input type="text" name="txttiend_ubicacion">
                        <button type="submit" class="button_add" name="accion" value="agregarTienda">Agregar</button>
                    </form>     
		</section>
	</div>
                        
</div>
                      
</main>
                        
                        
	<!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/scrollreveal.min.js"></script>

	<!--========= NUESTRO JS (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/main_admi.js"></script>
        
        <script type="text/javascript" src="resources/js/Funciones2.js"></script>
        
        <script type="text/javascript"> 
            
            function listarConSelect(){
                $.post("RedirectJQuery/SelectProvincia.jsp",$("#idfrmProvincia").serialize(),function(data){$("#idtblProvincias").html(data);});
                $.post("RedirectJQuery/SelectTienda.jsp",$("#idfrmProvincia").serialize(),function(data){$("#idtblTiendas").html(data);});
                $.post("RedirectJQuery/TotalProvincias.jsp",$("#idfrmProvincia").serialize(),function(data){$("#idTotalProvincias").html(data);});
                $.post("RedirectJQuery/TotalTiendas.jsp",$("#idfrmProvincia").serialize(),function(data){$("#idTotalTiendas").html(data);});
            }
            
            function busqueda(){
                
                $.post("RedirectJQuery/ConsultarTienda.jsp",$("#idBuscarTienda").serialize(),function(data){$("#idtblTiendas").html(data);});
                $.post("RedirectJQuery/ConsultarTiendaTotalTiendas.jsp",$("#idBuscarTienda").serialize(),function(data){$("#idTotalTiendas").html(data);});
            }  
            
            function modalTienda(){
                
                var tiend_codigo = $(this).attr('codigo');
                alert(tiend_codigo);
            }
            
            function modalTienda2(){
                
                let tien_codigo;
                tien_codigo = document.getElementById("tiend_codigo");
                alert(tiend_codigo);
            }            
            
        </script>        
	
</body>

</html>