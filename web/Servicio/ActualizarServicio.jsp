
<%@page import="com.google.gson.Gson"%>
<%@page import="modelo.Servicio"%>
<%@page import="modeloDAO.ServicioDAO"%>
<%@page import="Interfaces.Interface_Servicio"%>

<%
    Interface_Servicio interface_servicio = new ServicioDAO();
    
    
    String accion = request.getParameter("action");
    
    if (!accion.isEmpty()) {

        if (accion.equals("updateServicio")) {
            
            String serv_codigo = request.getParameter("serv_codigo");
            Servicio servicio = interface_servicio.obtenerUno(serv_codigo);

            if(servicio != null){
                
                String json = new Gson().toJson(servicio);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                    
            }
        }

    }
    

%>
