<%-- 
    Document   : usuario_dashboard
    Created on : 30/09/2022, 08:19:46 PM
    Author     : marsc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial- ">
        <title>Usuario</title>

        <!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
        <link rel="shortcut icon" href="#" type="image/png">

        <!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

        <!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
        <link rel="stylesheet" type="text/css" href="../../resources/css/estilo_admi.css">
        <link rel="stylesheet" type="text/css" href="../../resources/css/estilo_admi_2.css">
        <script type="text/javascript" src="../../resources/js/jquery-3.6.0.min.js"></script>
    </head>
    <div class="modal_tienda" >
        <div class="bodyModalTienda"> 
            <section>


            </section>
        </div>              
    </div> 

    <body id="body-pd">
        <!--========= NAVAR 1=================================================-->
        <div class="l-navbar" id="navbar">
            <nav class="nav">
                <div>
                    <div class="nav_brand">
                        <i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
                        <a href="" class="nav_logo">Fast Courier</a>
                    </div>
                    
                    <div class="nav_list">

                        <a href="funciones/usuario_perfil.jsp" class="nav_link">
                            <i class="nav_icon ri-article-fill"></i>
                            <span class="nav_name">Perfil</span>						
                        </a>
                        <a href="funciones/usuario_encomienda.jsp" class="nav_link">
                            <i class="nav_icon ri-red-packet-fill"></i>
                            <span class="nav_name">Realizar pedido</span>						
                        </a>
                    </div>
                </div>

                <a href="../../index.jsp" class="nav_link">
                    <i class="nav_icon ri-logout-circle-r-fill"></i>
                    <span class="nav_name">Salir</span>						
                </a>
            </nav>
        </div>

        <!--========= NAVAR 2 TITLE ========================================-->

        <div class="main">
            <div class="nav_vertical">
                <div class="nav_start">
                    <h1 class="nav_vertical_title">Perfil</h1>
                </div>
                <div class="nav_title">
                    <a href="../../index.jsp"><button class="button_normal">Ver Web </button></a>
                    <div class="nav_title_userAdmi">
                        <img class="img_userAdmi" src="../../resources/img/icon-5359553_960_720.png">
                        <div class="nav_userAdmi_cont">
                            <h3>Usuario</h3>
                            <%
                                
                            %>
                            <span>Usuario</span>
                            <%
                                
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        
                        
        <!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="../../js/scrollreveal.min.js"></script>

        <!--========= NUESTRO JS (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="../../resources/js/main_admi.js"></script>
        <script type="text/javascript" src="../../resources/js/Funciones2.js"></script>
    </body>
</html>
