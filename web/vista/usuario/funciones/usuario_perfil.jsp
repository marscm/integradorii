<%-- 
    Document   : usuario_perfil
    Created on : 30/09/2022, 08:35:37 PM
    Author     : marsc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial- ">
        <title>Perfil Usuario</title>

        <!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
        <link rel="shortcut icon" href="#" type="image/png">

        <!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

        <!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
        <link rel="stylesheet" type="text/css" href="../../../resources/css/estilo_admi.css">
        <link rel="stylesheet" type="text/css" href="../../../resources/css/estilo_admi_2.css">
        <script type="text/javascript" src="../../../resources/js/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" type="text/css" href="../../../resources/css/estilo_paquete.css">
    </head>

    <body>
        <div class="l-navbar" id="navbar">
            <nav class="nav">
                <div>
                    <div class="nav_brand">
                        <i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
                        <a href="" class="nav_logo">Fast Courier</a>
                    </div>

                    <div class="nav_list">

                        <a href="usuario_perfil.jsp" class="nav_link">
                            <i class="nav_icon ri-article-fill"></i>
                            <span class="nav_name">Perfil</span>						
                        </a>
                        <a href="usuario_encomienda.jsp" class="nav_link">
                            <i class="nav_icon ri-red-packet-fill"></i>
                            <span class="nav_name">Realizar pedido</span>						
                        </a>
                    </div>
                </div>

                <a href="../../../index.jsp" class="nav_link">
                    <i class="nav_icon ri-logout-circle-r-fill"></i>
                    <span class="nav_name">Salir</span>						
                </a>
            </nav>
        </div>

        <!--========= NAVAR 2 TITLE ========================================-->
        <div class="main">
            <div class="nav_vertical">

                <div class="nav_start">

                    <h1 class="nav_vertical_title">Usuario</h1>

                </div>

                <div class="nav_title">
                    
                    <a href="../../../index.jsp"><button class="button_normal">Ver Web </button></a>
                    <div class="nav_title_userAdmi">
                        <img class="img_userAdmi" src="../../../resources/img/icon-5359553_960_720.png">
                        <div class="nav_userAdmi_cont">
                            <h3>Usuario</h3>
                            <span>Nombre Cliente</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--========= CONTENIDO =============================================-->
        <div class="">
            <section> 
                
            </section>
            <section> </section>
            
            <section>
                <form class="col-l-10 col-lg-10 col-md-10"  method="Post" class="forbottum">
                    <div class="">        
                        
                        <h2 class="mt-4">Informacion General</h2>
                        <br>
                        
                        <div class="">
                           
                            <div class="tableEnco" >
                                <div class="form" style="align-items: center">
                                    <div class="form-group">
                                        <label for="firstName">Nombre  </label>
                                        <input type="text" class="form-control mb-4" id="firstName" placeholder="Shaun">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="lastName">Apellido  </label>
                                        <input type="text" class="form-control mb-4" id="lastName" placeholder="Park">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="profession">Celular  </label>
                                        <input type="number" class="form-control mb-4" id="profession" placeholder="123456789">
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="profession">Correo  </label>
                                        <input type="email" class="form-control mb-4" id="profession" placeholder="example@example.com">
                                    </div>
                                    <br>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="save-info">
                        <div class="row">
                            <div class="col-md-11 mx-auto">
                                <button class="btn btn-gradient-warning mb-4 float-right btn-rounded">Guardar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
        </div>       




        <!--========= CONTENIDO =============================================-->


        <!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)==========-->
        <script type="text/javascript" src="../../../js/scrollreveal.min.js"></script>

        <!--========= NUESTRO JS (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="../../../resources/js/main_admi.js"></script>
        <script type="text/javascript" src="../../../resources/js/Funciones2.js"></script>
    </body>
</html>
