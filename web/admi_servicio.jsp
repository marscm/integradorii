<%-- 
    Document   : admi_servicio
    Created on : 02-dic-2021, 18:49:49
    Author     : Usuario
--%>

<%@page import="modelo.Servicio"%>
<%@page import="modeloDAO.ServicioDAO"%>
<%@page import="Interfaces.Interface_Servicio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial- ">
	<title>Adminitrador</title>

	<!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
	<link rel="shortcut icon" href="#" type="image/png">

	<!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

	<!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
	<link rel="stylesheet" type="text/css" href="resources/css/estilo_admi.css">
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi_2.css">
        <script type="text/javascript" src="resources/js/jquery-3.6.0.min.js"> </script>
</head>


     <div class="modal_tienda" >
        <div class="bodyModalTienda"> 
            <section>
                <form action="Control_Servicio" method="post" name="frmUpdateServicio" id="id_frmUpdateServicio" >
                <h1> ACTUALIZAR SERVICIO </h1>
                <h2>
                <label class="serv_tipo"> </label> <br>
                </h2>
                <h2> Precio <input type="text" name="txtPrecio" id="id_txtPrecio"> <br></h2> 
                <input type="hidden" name="txtCodigo" id="id_txtCodigo" required>
                <button type="submit" name="accion" value="actualizaServicio"> Actualizar </button>
                <a href="#" class="closeModalTienda" onclick="closeModal()"> Cerrar </a>
            </form>
            </section>
        </div>              
    </div> 

<%
    
    Interface_Servicio interface_servicio = new ServicioDAO();
%>


<body id="body-pd">

	<!--========= NAVAR 1=================================================-->
	<div class="l-navbar" id="navbar">
		<nav class="nav">
			<div>
				<div class="nav_brand">
					<i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
					<a href="" class="nav_logo">Fast Courier</a>
				</div>
				<div class="nav_list">
					<div class="nav_link desplegue">
						<i class="nav_icon ri-truck-fill"></i>
						<span class="nav_name">Ruta</span>	
						<i class="ri-arrow-down-s-line desplegue_link"></i>		

						<ul class="desplegue_menu">
							<a href="admi_actuRuta_1.jsp" class="desplegue_sublink">Actualizaciones</a>
							<a href="admi_tiendaRuta_1.jsp" class="desplegue_sublink">Tiendas</a>
						</ul>			
					</div>
					<a href="admi_servicio.jsp" class="nav_link active">
						<i class="nav_icon ri-service-fill"></i>
						<span class="nav_name">Servicios</span>						
					</a>
					<a href="admi_articulo.jsp" class="nav_link">
						<i class="nav_icon ri-article-fill"></i>
						<span class="nav_name">Articulo</span>						
					</a>
					<a href="admi_encomienda.jsp" class="nav_link">
						<i class="nav_icon ri-red-packet-fill"></i>
						<span class="nav_name">Encomienda</span>						
					</a>
				</div>
			</div>

			<a href="" class="nav_link">
				<i class="nav_icon ri-logout-circle-r-fill"></i>
				<span class="nav_name">Salir</span>						
			</a>
		</nav>
	</div>
<!--========= NAVAR 2 TITLE ========================================-->
	<div class="main">
		<div class="nav_vertical">
			<div class="nav_start">
				<h1 class="nav_vertical_title">Servicios</h1>
			</div>
			<div class="nav_title">
                            <a href="index.jsp"><button class="button_normal">Ver Web </button></a>
				<div class="nav_title_userAdmi">
					<img class="img_userAdmi" src="resources/img/icon-5359553_960_720.png">
					<div class="nav_userAdmi_cont">
						<h3>Adminitrador</h3>
						<span>Super Admi</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--========= CONTENIDO =============================================-->
<main>
<div class="nuestraTienda">
	<div class="tienda_tablaTotales">
		<!--TOTALES Y FILTRO -------------------------------------------------->
		<div class="tienda_totales">
                        <section class="tienda_total_cont">
                                <div class="total_info" id="id_TotalServicios">
                                    <p class="total_num"><%= interface_servicio.listarCantidad() %></p>
                                    <span>Total de Servicios</span>
				</div>
				<i class=" icon_total ri-truck-fill"></i> 
			</section>
		</div>
		<!--FITRO BUSCAR---------------------------------------------------->
		<div class="tienda_filtroBusca">
                    <section class="tienda_filtroProv_cont">
                        <div class="filtro_tienda_name filtro_tienda">
                            <i class="icon_buscar ri-search-2-line"></i>
                            <h3  style="padding-right:10px">Servicio</h3>
                            <form id="id_frmBuscarServicio">
                                <input type="text" name="txtParametroBusqueda" placeholder="Buscar Servicio" onkeyup="busqueda()" id="buscador">
                            </form>
                        </div>
                    </section>
		</div>
		<!--TABLAS Y PROINCIAS ---------------------------------------------------->
                <div class="tienda_tablas">
			<!--Tabla 2 ---------------------------------------------------->
			<div class="tabla">
			<section class="tabla_capa">
			<div class="tabla_tienda">
                            <table class="tabla_tienda_cont tienda_prov" id="idtblTiendas">
					<thead>
						<tr> 
                                                    <th> </th>
                                                    <th> </th>
                                                    <th>Código</th>
                                                    <th>Tipo</th>
                                                    <th>Precio Base (s/.)</th>
						</tr>
					</thead>
					<tbody> 
                                            <%
                                                for(Servicio servicio: interface_servicio.listar()){
                                            %>
                                            <tr>
                                                <td><a href="Control_Servicio?accion=eliminaServicio&serv_codigo=<%=servicio.getServ_Codigo()%>"><i class="icon_close ri-close-circle-fill"></i></a></td>
                                                <td><a class="update_servicio" id="id_update_servicio" serv_codigo="<%=servicio.getServ_Codigo()%>" href="#"><i class="ri-edit-fill"></i></a></td>
                                                <td><%= servicio.getServ_Codigo() %></td>
                                                <td><%= servicio.getServ_Tipo() %> </td>
                                                <td><%= servicio.getServ_PrecioBase() %> </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                                         
					</tbody>
                            </table>
			</div>			
			</section>
		</div>
            </div>
	</div>

	<!--TABLAS ADICIONAR ---------------------------------------------------->
	<div class="tienda_adicionar">
		<section class="add_seccion adicionar_prov">
                    <form action="Control_Servicio" method="post">
			<h2><b>Nuevos<br>Servicios</b></h2>
			<h3>Tipo de Servicio</h3>
			<input type="text" name="txtTipo" placeholder="Servicio">
			<h3>Precio Base (s/.)</h3>
			<input type="text" name="txtPrecioBase" placeholder="Precio Base">
                        <button type="submit" class="button_add" name="accion" value="agregarServicio">Agregar</button>
                    </form>    
		</section>
	</div>
                        
</div>
</main>
	<!-- SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)-->
	<script type="text/javascript" src="resources/js/scrollreveal.min.js"></script>

	<!-- NUESTRO JS (COPIAR PEGAR)-->
	<script type="text/javascript" src="resources/js/main_admi.js"></script>
        <script type="text/javascript" src="resources/js/Funciones2.js"></script>
        
        <script type="text/javascript"> 
            
            
            function busqueda(){
                var input, filtro, table, tr, referencia1,referencia2,referencia3, i, x,y,z;
                input = document.getElementById("buscador");
                filtro = input.value.toUpperCase();
                table = document.getElementById("idtblTiendas");
                tr = table.getElementsByTagName("tr");
                
                for (i = 0; i < tr.length; i++) {
                referencia1 = tr[i].getElementsByTagName("td")[2];
                referencia2 = tr[i].getElementsByTagName("td")[3];
                referencia3 = tr[i].getElementsByTagName("td")[4];
                if (referencia1) {
                  x = referencia1.textContent || referencia1.innerText;
                  y = referencia2.textContent || referencia2.innerText;
                  z = referencia3.textContent || referencia3.innerText;
                  if (x.toUpperCase().indexOf(filtro) > -1 || 
                          y.toUpperCase().indexOf(filtro) > -1 ||
                          z.toUpperCase().indexOf(filtro) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }
                }
                
                $.post("Servicio/TotalServicios.jsp",$("#id_frmBuscarServicio").serialize(),function(data){$("#id_TotalServicios").html(data);});
                
            }
             
        </script>        
	
</body>
</html>