<%-- 
    Document   : nosotros
    Created on : 10-oct-2021, 23:12:23
    Author     : mello
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial- ">
	<title>Fast Curier</title>

	<!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
	<link rel="shortcut icon" href="#" type="image/png">

	<!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

	<!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
	<link rel="stylesheet" type="text/css" href="resources/css/estilo.css">

</head>
<body>

	<!--========= NAVER--(COPIAR PEGAR)-------------------------------------------- ================================================-->
	<header class="header" id="header">
		<nav class="nav container">
			<a href="#" class="nav_logo">FAST COURIER</a>

			<div class="nav_menu" id="nav-menu">
				<ul class="nav_list">
					<li class="nav_item">
						<a href="index.jsp" class="nav_link  active-link ">Inicio</a>
					</li>
					<li class="nav_item">
						<a href="index.jsp#nuestro_servicio" class="nav_link">Servicio</a>
					</li>
					<li class="nav_item">
						<a href="nosotros.jsp" class="nav_link">Nosotros</a>
					</li>
					<li class="nav_item">
						<a href="contacto.jsp" class="nav_link">Contacto</a>
					</li>
                                                                                                                                   <li class="nav_user_animed" id="buttonUser">
						<a  class="nav_user ri-user-smile-fill " id="icon-user"></a>
						<span class="nav_user_text">Login</span>
					</li>
				</ul>
		
			<div class="nav_close" id="nav-close">
				<i class="ri-close-line"></i>
			</div>	
		</div>
			<!--======= ---Responsibe Hamburguesa====-->
			<div class="nav_toggle" id="nav-toggle">
				<i class="ri-function-line"></i>
			</div>
	</nav>
	</header>
	<!--======= SOCIAL ==(COPIAR PEGAR)========================-->
	<div class="social">
		<ul class="sci">
			
			<li><a href="#" target="_blank" class="home_social-link"><i class="ri-facebook-circle-fill"></i></a></li>
			
			
			<li><a href="#" target="_blank" class="home_social-link"><i class="ri-instagram-fill"></i></a></li>
			
			<li><a href="#" target="_blank" class="home_social-link"><i class="ri-whatsapp-fill"></i></a></li>
			
		</ul>
	</div>
   <!--======= ---------------------------------------------- =============-->
  <main class="main">
  	<!--======= Inicio (COPIAR PEGAR)===================================================================================================-->
  	<section class="home section" id="home">
  		<img src="resources/img/img_inicio1.jpg" class="home_img">

  		<div class="home_container container grid">
  			<div class="home_data">
  				<h1 class="home_data_titulo"><b>Bienvenido!!</b></h1>
  			</div>
  			<div class="home_info">
  				<p>Te invitamos a revisar nuestra página web. Aqui podras cotizar mediante nuestros dos servicios de entrega. Solo has click en boton cotizar!!</p>
  			</div>
  			<div class="home_button">
  				<a href="index.html#nuestro_servicio"class="button">Cotizar</a>
  			</div>
  		</div>
  	</section>
        <!-- LOGIN------------------------------------------------------------------>
  	<div class="modal_login loginNone" id="login">
		   
            <section class="seccion_login">
			<i class="icon_close cerrar_login ri-close-circle-fill" id="icon-close"></i>
                        <form method="post" action="logincontroler">
                            
                        <div>
				<h1><b>Fast Courier</b></h1>
				<i class="icon_userLogin ri-account-circle-fill"></i>
				<div class="login_info">
					<label><b>Usuario</b></label>
					<input maxlength="8" type="text" name="usuario" placeholder="Ingresa DNI">
					<label><b>Contraseña</b></label>
					<input type="password" name="pass" placeholder="Ingresa Constraseña">
				</div>
				<div class="login_recuerda">
					<input type="checkbox" name="recuerda_pass">
					<label>Recuérdame</label>
				</div>
				<div>
                                    <button class="button" name="accion" value="ingresar">Ingresar</button>
				</div>
                                 
				<div class="login_olvPass"><a href=""><i>¿Olvidaste constraseña?</i></a></div>
                             
                        </div>	
                           </form>      
                              
		</section>
	</div>

  	<!--======= Servicio ===============================================================================================-->
  	<section class="section nuestro_servicio" id="nuestro_servicio">
  		<div class="nuestro_servicio_container container grid">
  			
  				<h2 class="nuestro_servicio_titulo">Nuestros Servicios</h2>
  				<span class="nuestro_servicio_info">
  				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
  				
  				<div class="nuestro_servicio_flex">
  				<!-- Servicio 01-->
  				<div class="nuestro_servicio_serivio">
  					
  						<a href="faces/Servicio_Carga.xhtml" class="nuestro_servicio-link ri-building-4-fill nuestro_servicio-link"></a>
  					<div class="nuestro_servicio_card">
  						<h3 class="nuestro_servicio_subtitulo">Servicio Agencia a Agencia</h3>
  						<span class="nuestro_servicio_infoCard">proident, ident, sunt in culpa qui officia desesunt in culpa qui officia deserunt mollit anim id est laborum.</span>
  					</div>
  				</div>

  				<!-- Servicio 02-->
  				<div class="nuestro_servicio_serivio">
  					
  						<a href="faces/Servicio_Domicilio.xhtml" class="nuestro_servicio-link ri-home-2-fill"></a>
  					<div class="nuestro_servicio_card">
  						<h3 class="nuestro_servicio_subtitulo">Servicio Agencia a Domicilio</h3>
  						<span class="nuestro_servicio_infoCard">proidentident, sunt in culpa qui officia dese, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
  					</div>
  				</div>
  			</div>
  		</div>
  	</section>

  	<!--======= Por qué trabajar con nosotros? =========================================================================-->
  	<section class="xq_trabajo section" id="xq_trabajo">
  		<div class="xq_trabajo_container container grid">
  			<div class="xq_trabajo_info">
  				<h2 class="xq_trabajo_titulo">¿Por qué trabajar con nosotros?</h2>
  				<span class="xq_trabajo_respuesta">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
  			</div>
  			<div class="xq_trabajo_cajaImg">
  				<img src="resources/img/img_inicio2.jpg" class="xq_trabajo_img">
  			</div>
  		</div>
  	</section>
  </main>
<!--======= footer (COPIAR PEGAR)=========================================================================-->				
<footer class="footer section">
	<div class="footer_container container grid">
		<div class="footer_content grid">
			<div class="footer_data">

				<h3 class="footer_title">FAST COURIER</h3>

			<div class="footer_data_flex">
				<div class="footer_data">
					<ul>
						<li class="footer_item">
							<a href="#" class="footer_link">PREGUNTAS FRECUENTES</a>
						</li>
						<li class="footer_item">
							<a href="#" class="footer_link">TERMINOS Y CONDICIONES</a>
						</li>
						<li class="footer_item">
							<a href="#" class="footer_link">LIBRO DE RECLAMACIONES</a>
						</li>						
					</ul>
				</div>

				<div class="footer_data">
					<div class="footer_contacto">					

					<ul>
						<li>
							<div class="footer_contacto">
								<div>
									<i class="ri-phone-fill footer_icon"></i>
								</div>
							Teléfono: (01)408-6145
							</div>
						</li>
						<li>
							<div class="footer_contacto">
								<div>
									<i class="ri-building-fill footer_icon"></i>
								</div>
							Av. Los Rosales 103 - La Victoria - Lima
							</div>
						</li>
						<li>
							<div class="footer_contacto">
								<div>
									<i class="ri-mail-fill footer_icon"></i>
								</div>
							fastCurier@gmail.com
							</div>
						</li>
						
					</ul>
				    </div>
			    </div>
			</div>
		</div>

		<div class="footer_rights">
				<p class="footer_copy">&#169; 2022 Grupo 02. All rogth reserved. Lima-Perú</p>
		</div>
	</div>
	
</footer>	


	

  	<!-------- SCROLL UP (Boton arriba) (COPIAR PEGAR)---------------->
	<a href="" class="scrollup" id="scroll-up">
		<i class="ri-arrow-up-s-line scrollup_icon"></i>
	</a>

	<!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/scrollreveal.min.js"></script>

	<!--========= NUESTRO JS (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/main.js"></script>
</body>
</html>