<%-- 
    Document   : contacto
    Created on : 10-oct-2021, 23:13:47
    Author     : mello
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial- ">
	<title>Fast Curier</title>
	<!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
	<link rel="shortcut icon" href="#" type="image/png">
	<!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

	<!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
	<link rel="stylesheet" type="text/css" href="resources/css/estilo.css">
	
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   

</head>
<body>
	<!--========= NAVER--(COPIAR PEGAR)-------------------------------------------- ================================================-->
	<header class="header" id="header">
		<nav class="nav container">
			<a href="#" class="nav_logo">FAST COURIER</a>

                        <div class="nav_menu" id="nav-menu">
                            <ul class="nav_list">
                                <li class="nav_item">
                                    <a href="index.jsp" class="nav_link ">Inicio</a>
                                </li>
                                <li class="nav_item">
                                    <a href="index.jsp#nuestro_servicio" class="nav_link">Servicio</a>
                                </li>
                                <li class="nav_item">
                                    <a href="nosotros.jsp" class="nav_link">Nosotros</a>
                                </li>
                                <li class="nav_item">
                                    <a href="contacto.jsp" class="nav_link active-link">Contacto</a>
                                </li>
                                <li class="nav_user_animed" id="buttonUser">
                                    <a  class="nav_user ri-user-smile-fill " id="icon-user"></a>
                                    <span class="nav_user_text">Login</span>
                                </li>
				</ul>
		
			<div class="nav_close" id="nav-close">
				<i class="ri-close-line"></i>
			</div>	
		</div>
			<!--======= ---Responsibe Hamburguesa====-->
			<div class="nav_toggle" id="nav-toggle">
				<i class="ri-function-line"></i>
			</div>
	</nav>
	</header>
	<!--======= SOCIAL ==(COPIAR PEGAR)========================-->
	<div class="social">
		<ul class="sci">
			
			<li><a href="#" target="_blank" class="home_social-link"><i class="ri-facebook-circle-fill"></i></a></li>
			
			
			<li><a href="#" target="_blank" class="home_social-link"><i class="ri-instagram-fill"></i></a></li>
			
			<li><a href="#" target="_blank" class="home_social-link"><i class="ri-whatsapp-fill"></i></a></li>
			
		</ul>
	</div>
   <!--======= ---------------------------------------------- =============-->
  <main class="main">
  	<!--======= Inicio (COPIAR PEGAR)===================================================================================================-->
  	<section class="home section" id="home">
  		<img src="resources/img/img_contacto1.jpg" class="home_img">

  		<div class="home_container container grid">
  			<div class="home_data">
  				<h1 class="home_data_titulo"><b>Bienvenido!!</b></h1>
  			</div>
  			<div class="home_info">
  				<p>Fast Courier es una empresa dedicada al transporte de carga y encomienda. Actualmente, cuenta con más de 14 años de experiencia realizando envíos dentro del territorio peruano.</p>
  			</div>
  			<div class="home_button">
  				<a href="nosotros.html"class="button">Conocenos</a>
  			</div>
  		</div>

  	</section>

<!-- LOGIN------------------------------------------------------------------>
  	<div class="modal_login loginNone" id="login">
		<section class="seccion_login">
			<i class="icon_close cerrar_login ri-close-circle-fill" id="icon-close"></i>
			<div>
				<h1><b>Fast Courier</b></h1>
				<i class="icon_userLogin ri-account-circle-fill"></i>
				<div class="login_info">
					<label><b>Usuario</b></label>
					<input type="text" name="usuario" placeholder="Ingresa DNI">
					<label><b>Contraseña</b></label>
					<input type="password" name="pass" placeholder="Ingresa Constraseña">
				</div>
				<div class="login_recuerda">
					<input type="checkbox" name="recuerda_pass">
					<label>Recuérdame</label>
				</div>
				<div>
					<button class="button">Ingresar</button>
				</div>
				<div class="login_olvPass"><a href=""><i>¿Olvidaste constraseña?</i></a></div>
			</div>	
		</section>
	</div>

  	
<!---------------------------CONTACTO------------------------->


  	<section class="contact-box" id="contac">
       <div class="row no-gutters nuestro_servicio_serivio">
           <div class="col-xl-5 col-lg-12 ">
           	<img src="resources/img/img_contacto2.png" class="form-control">
           </div>
           <div class="col-xl-7 col-lg-12 d-flex">

                <div class="container align-self-center p-6">
                    <h1 class="font-weight-bold mb-3">Contactanos</h1>
                    <form>
                        <div class="form-row mb-2">
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Apellido">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold"> Nro° DNI <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="DNI">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold"> AsUnto <span class="text-danger">*</span></label>
                                <select class="form-control">
                                	<option>Eligir asunto</option>
                                	<option>--------------</option>
                                	<option>--------------</option>
                                	
                                </select>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label class="font-weight-bold">Correo electrónico <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" placeholder="Ingresa tu correo electrónico">
                        </div>
                        <div class="form-group mb-3">
                            <label class="font-weight-bold">Mensaje <span class="text-danger">*</span></label>
                            <textarea class="form-control" placeholder="Escriba su mensaje"></textarea>
                        </div>
                        <button class=" btn width-100  button">Enviar</button>
                    </form>
                </div>
           </div>
       </div>
   </section>

  	<!--======= footer (COPIAR PEGAR)=========================================================================-->				
<footer class="footer section">
	<div class="footer_container container grid">
		<div class="footer_content grid">
			<div class="footer_data">

				<h3 class="footer_title">FAST COURIER</h3>

			<div class="footer_data_flex">
				<div class="footer_data">
					<ul>
						<li class="footer_item">
							<a href="#" class="footer_link">PREGUNTAS FRECUENTES</a>
						</li>
						<li class="footer_item">
							<a href="#" class="footer_link">TERMINOS Y CONDICIONES</a>
						</li>
						<li class="footer_item">
							<a href="#" class="footer_link">LIBRO DE RECLAMACIONES</a>
						</li>						
					</ul>
				</div>

				<div class="footer_data">
					<div class="footer_contacto">					

					<ul>
						<li>
							<div class="footer_contacto">
								<div>
									<i class="ri-phone-fill footer_icon"></i>
								</div>
							Teléfono: (01)408-6145
							</div>
						</li>
						<li>
							<div class="footer_contacto">
								<div>
									<i class="ri-building-fill footer_icon"></i>
								</div>
							Av. Los Rosales 103 - La Victoria - Lima
							</div>
						</li>
						<li>
							<div class="footer_contacto">
								<div>
									<i class="ri-mail-fill footer_icon"></i>
								</div>
							fastCurier@gmail.com
							</div>
						</li>
						
					</ul>
				    </div>
			    </div>
			</div>
		</div>

		<div class="footer_rights">
				<p class="footer_copy">&#169; 2022 Grupo 02. All rogth reserved. Lima-Perú</p>
		</div>
	</div>
	
</footer>	


	

  	<!-------- SCROLL UP (Boton arriba) (COPIAR PEGAR)---------------->
	<a href="" class="scrollup" id="scroll-up">
		<i class="ri-arrow-up-s-line scrollup_icon"></i>
	</a>

	<!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/scrollreveal.min.js"></script>

	<!--========= NUESTRO JS (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/main.js"></script>

</body>
</html>