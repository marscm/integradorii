

<%@page import="com.google.gson.Gson"%>
<%@page import="modelo.Articulo"%>
<%@page import="modeloDAO.ArticuloDAO"%>
<%@page import="Interfaces.Interface_Articulo"%>


<%
    Interface_Articulo interface_articulo = new ArticuloDAO();
    String accion = request.getParameter("action");
    
    
    if (!accion.isEmpty()) {

        if (accion.equals("updateArticulo")) {
            
            String art_codigo = request.getParameter("art_codigo");
            Articulo articulo = interface_articulo.obtenerUno(art_codigo);

            if(articulo != null){
                
                String json = new Gson().toJson(articulo);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                    
            }
        }

    }
    

%>