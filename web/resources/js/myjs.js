function paso1() {
    var lapiz = document.getElementById('lapiz');
    lapiz.setAttribute("style", "fill:white");
    var circulo1Datos = document.getElementById('circulo1Datos');
    circulo1Datos.setAttribute("style", "fill:none;stroke:black;stroke-width:2");
    var circulo2Datos = document.getElementById('circulo2Datos');
    circulo2Datos.setAttribute("style", "fill:none;stroke:black;stroke-width:2");
    var pDatos = document.getElementById('pDatos');
    pDatos.setAttribute("style", "fill:none;stroke:black;stroke-width:2");
}

function primerSgte() {
    document.getElementById('paso1').style.display = "none";
    document.getElementById('paso2').style.display = "block";
    document.getElementByName('primerbtn').style.display = "none";

}


function tabla_verPaso2() {
    var pd = document.getElementById('paso2');
    var tabla = document.getElementById('paso3');
    var btnsT = document.getElementById('btnPaso3');
    var lapiz = document.getElementById('lapiz');
    lapiz.setAttribute("style", "fill:white");
    var pRegistro = document.getElementById('pRegistro');
    pRegistro.setAttribute("style", "fill:black");
    var rRegistro = document.getElementById('rRegistro');
    rRegistro.setAttribute("style", "fill:black");
    var polyRegistro = document.getElementById('polyRegistro');
    polyRegistro.setAttribute("style", "fill:black");
    tabla.style.display = "none";
    btnsT.style.display = "none";
    pd.style.display = "block";
}


function verPaso3() {
    var paso3 = document.getElementById('paso3');
    var formulario = document.getElementById('formularioPrincipal');
    var btn = document.getElementById('btnsCaja');
    formulario.style.display = "none";
    btn.style.display = "none";
    paso3.style.display = "block";
}

function ingresarPedido() {
    var tabla = document.getElementById('paso3');
    var pi = document.getElementById('paso1');
    var btnsT = document.getElementById('btnPaso3');
    var z = document.getElementById('primerbtn');
    var pRegistro = document.getElementById('pRegistro');
    pRegistro.setAttribute("style", "fill:black");
    var rRegistro = document.getElementById('rRegistro');
    rRegistro.setAttribute("style", "fill:black");
    var polyRegistro = document.getElementById('polyRegistro');
    polyRegistro.setAttribute("style", "fill:black");
    var circulo1Datos = document.getElementById('circulo1Datos');
    circulo1Datos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    var circulo2Datos = document.getElementById('circulo2Datos');
    circulo2Datos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    var pDatos = document.getElementById('pDatos');
    pDatos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    tabla.style.display = "none";
    pi.style.display = "block";
    btnsT.style.display = "none";
    z.style.display = "flex";
}


function showSobre() {
    var envelope = document.getElementById('sobre');
    var boxx = document.getElementById('caja');
    var btnsS = document.getElementById('btnsSobre');
    var btnsC = document.getElementById('btnsCaja');
    var sobreIcono = document.getElementById('sobreIcono');
    sobreIcono.setAttribute("style", "fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
    var cajaIcono = document.getElementById('contornoCaja');
    cajaIcono.setAttribute("style", "fill:black;stroke:black;stroke-width:0.5");
    boxx.style.display = "none";
    envelope.style.display = "block";
    btnsS.style.display = "flex";
    btnsC.style.display = "none";
}
function showCaja() {
    var boxx = document.getElementById('caja');
    boxx.style.display = "block";
}

function colorCaja() {
    var cajaIcono = document.getElementById('contornoCaja');
    cajaIcono.setAttribute("style", "fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
    var sobreIcono = document.getElementById('sobreIcono');
    sobreIcono.setAttribute("style", "fill:black;stroke:hsl(190, 24%, 97%);stroke-width:0.5");
}
function colorSobre() {
    var sobreIcono = document.getElementById('sobreIcono');
    sobreIcono.setAttribute("style", "fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
    var cajaIcono = document.getElementById('contornoCaja');
    cajaIcono.setAttribute("style", "fill:black;stroke:black;stroke-width:0.5");
}

function caja_verPaso3() {
    var xs = document.getElementById('paso2');
    var ys = document.getElementById('paso3');
    var btn3 = document.getElementById('btnPaso3');
    var rs = document.getElementById('caja');
    var zs = document.getElementById('btnsCaja');
    var lapiz = document.getElementById('lapiz');
    lapiz.setAttribute("style", "fill:black");
    var pRegistro = document.getElementById('pRegistro');
    pRegistro.setAttribute("style", "fill:white");
    var rRegistro = document.getElementById('rRegistro');
    rRegistro.setAttribute("style", "fill:white");
    var polyRegistro = document.getElementById('polyRegistro');
    polyRegistro.setAttribute("style", "fill:white");
    xs.style.display = "none";
    zs.style.display = "none";
    rs.style.display = "none";
    ys.style.display = "block";
    btn3.style.display = "flex";

}

function sobre_verPaso3() {
    var xs = document.getElementById('paso2');
    var ys = document.getElementById('paso3');
    var btn3 = document.getElementById('btnPaso3');
    var rs = document.getElementById('sobre');
    var zs = document.getElementById('btnsSobre');
    var lapiz = document.getElementById('lapiz');
    lapiz.setAttribute("style", "fill:black");
    var pRegistro = document.getElementById('pRegistro');
    pRegistro.setAttribute("style", "fill:white");
    var rRegistro = document.getElementById('rRegistro');
    rRegistro.setAttribute("style", "fill:white");
    var polyRegistro = document.getElementById('polyRegistro');
    polyRegistro.setAttribute("style", "fill:white");
    xs.style.display = "none";
    zs.style.display = "none";
    rs.style.display = "none";
    ys.style.display = "block";
    btn3.style.display = "flex";
}

function caja_verPaso1() {
    var pi = document.getElementById('paso1');
    var ptipo = document.getElementById('paso2');
    var z = document.getElementById('primerbtn');
    var rs = document.getElementById('caja');
    var zs = document.getElementById('btnsCaja');
    var btnsS = document.getElementById('btnsSobre');
    var btnsC = document.getElementById('btnsCaja');
    var lapiz = document.getElementById('lapiz');
    lapiz.setAttribute("style", "fill:black");
    var circulo1Datos = document.getElementById('circulo1Datos');
    circulo1Datos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    var circulo2Datos = document.getElementById('circulo2Datos');
    circulo2Datos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    var pDatos = document.getElementById('pDatos');
    pDatos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    ptipo.style.display = "none";
    rs.style.display = "none";
    zs.style.display = "none";
    btnsS.style.display = "none";
    btnsC.style.display = "none";
    pi.style.display = "block";
    z.style.display = "flex";
}

function sobre_verPaso1() {
    var pi = document.getElementById('paso1');
    var ptipo = document.getElementById('paso2');
    var z = document.getElementById('primerbtn');
    var rs = document.getElementById('sobre');
    var zs = document.getElementById('btnsCaja');
    var btnsS = document.getElementById('btnsSobre');
    var btnsC = document.getElementById('btnsCaja');
    var lapiz = document.getElementById('lapiz');
    lapiz.setAttribute("style", "fill:black");
    var circulo1Datos = document.getElementById('circulo1Datos');
    circulo1Datos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    var circulo2Datos = document.getElementById('circulo2Datos');
    circulo2Datos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    var pDatos = document.getElementById('pDatos');
    pDatos.setAttribute("style", "fill:none;stroke:white;stroke-width:2");
    ptipo.style.display = "none";
    rs.style.display = "none";
    zs.style.display = "none";
    btnsS.style.display = "none";
    btnsC.style.display = "none";
    pi.style.display = "block";
    z.style.display = "flex";
}

function pagar_verPaso3() {
    var tabla = document.getElementById('paso3');
    var pag = document.getElementById('paso4');
    var btnsT = document.getElementById('btnPaso3');
    var btnsP = document.getElementById('btnPaso4');
    var pRegistro = document.getElementById('pRegistro');
    pRegistro.setAttribute("style", "fill:white");
    var rRegistro = document.getElementById('rRegistro');
    rRegistro.setAttribute("style", "fill:white");
    var polyRegistro = document.getElementById('polyRegistro');
    polyRegistro.setAttribute("style", "fill:white");
    var p1Enviado = document.getElementById('p1Enviado');
    p1Enviado.setAttribute("style", "fill:black");
    var p2Enviado = document.getElementById('p2Enviado');
    p2Enviado.setAttribute("style", "fill:black");
    tabla.style.display = "block";
    pag.style.display = "none";
    btnsT.style.display = "flex";
    btnsP.style.display = "none";
}



