

/*=== MENU ADMI - EXPANDIR ============================================*/

const shoMenuAdmi = (toggleId, navbarId, bodyId)=>{
	const toggleAdmi = document.getElementById(toggleId),
	navbar = document.getElementById(navbarId),
	bodypadding = document.getElementById(bodyId)

	if (toggleAdmi && navbar){
		toggleAdmi.addEventListener('click', ()=>{
			navbar.classList.toggle('expander')

			bodypadding.classList.toggle('body-pd')
		})
	} 
			
}
shoMenuAdmi('nav-toggle','navbar','body-pd')

/*=== LINKS ACTIVOS ============================================*/
const linkAdmiColor = document.querySelectorAll('.nav_link')
function colorLink(){
	linkAdmiColor.forEach(l=> l.classList.remove('active'))
	this.classList.add('active')
}
linkAdmiColor.forEach(l=> l.addEventListener('click', colorLink))

/*=== MENU ADMI DESPLEGUE ============================================*/
const linkDesplegue = document.getElementsByClassName('desplegue_link')
var i

for (var i=0;i<linkDesplegue.length;i++) {
	linkDesplegue[i].addEventListener('click', function(){
		const desplegueMenu = this.nextElementSibling
		desplegueMenu.classList.toggle('showDesplegue')
	})
}

/*=== Ocultar Sección - Agregar Ruta ============================================*/
const buttonAdd = document.getElementById('icon-add')
const seccionAdd = document.getElementById('seccion-add')

	if (buttonAdd) {
		buttonAdd.addEventListener('click', ()=>{			
				seccionAdd.classList.toggle('add-active')

				if ('add-active') {
					buttonAdd.classList.remove('ri-add-circle-fill')
					buttonAdd.classList.remove('icon-add')
					buttonAdd.classList.toggle('ri-indeterminate-circle-fill')
					buttonAdd.classList.toggle('icon_menos')

				}
				buttonAdd.classList.toggle('ri-add-circle-fill')
				buttonAdd.classList.remove('icon-add')		
		})

	}
        

/*=== Ocultar Sección - Agregar Volumen ============================================*/
 const buttonAddVolum = document.getElementById('icon-add-volum')	
const seccionAddVolum = document.getElementById('seccion-add-volum')

if (buttonAddVolum) {
		buttonAddVolum.addEventListener('click', ()=>{			
				seccionAddVolum.classList.toggle('add-active-volum')

				if ('add-active-volum') {
					buttonAddVolum.classList.remove('ri-add-circle-fill')
					buttonAddVolum.classList.remove('icon-add-volum')
                                                                                                                                  buttonAddVolum.classList.toggle('ri-indeterminate-circle-fill')
					buttonAddVolum.classList.toggle('icon_menos')

				}
				buttonAddVolum.classList.toggle('ri-add-circle-fill')
				buttonAddVolum.classList.remove('icon-add-volum')		
		})

	}
	
	

