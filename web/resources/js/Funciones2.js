$(document).ready(function(){
// Modal form 
    $('.tienda_codigo').click(function (e){
        e.preventDefault();
        var codigo = $(this).attr('codigo');
        var action = 'updateTienda';
        $.ajax({
            url:'Tienda/ActualizarTienda.jsp',
            type:'POST',
            async: true,
            data:{action:action, codigo: codigo},
        
            success: function(response){
//                console.log(response);
                
                if(response !== 'error' ){
                    var string1 = JSON.stringify(response);
                    var info = JSON.parse(string1);
//                    console.log(info);
                    
                     $('#up_tienda_codigo').val(info.tiend_codigo);
                     $('.tiendNombre').html(info.tiend_nombre);
                     $('.tiendProvincia').html(info.provincia.prov_nombre);
                }

            },        
            error: function(error){
                console.log(error);
            }    
        });
        
        $('.modal_tienda').fadeIn();
        
    });
    
    $('.update_ruta').click(function (e){
        e.preventDefault();
        var ruta_codigo = $(this).attr('ruta_codigo');
        var action = 'updateRuta';
        $.ajax({
            url:'Ruta/ActualizarRuta.jsp',
            type:'POST',
            async: true,
            data:{action:action, ruta_codigo: ruta_codigo},
        
            success: function(response){
//                console.log(response);
                
                if(response !== 'error' ){
                    var string1 = JSON.stringify(response);
                    var info = JSON.parse(string1);
//                    console.log(info);
                    
                     $('#id_txtCodigo').val(info.ruta_codigo);
                     $('.ruta_origen').html(info.ruta_origen);
                     $('.ruta_sede_origen').html(info.ruta_sede_origen);
                     $('.ruta_destino').html(info.ruta_destino);
                     $('.ruta_sede_destino').html(info.ruta_sede_destino);
                }

            },        
            error: function(error){
                console.log(error);
            }    
        });        
        
        $('.modal_tienda').fadeIn();
        
    }); 
    
    
    $('.update_encomienda').click(function (e){
        e.preventDefault();    
        var enco_codigo = $(this).attr('enco_codigo');
        var action = 'updateEncomienda';
        $.ajax({
            url:'Encomienda/ActualizarEncomienda.jsp',
            type:'POST',
            async: true,
            data:{action:action, enco_codigo: enco_codigo},
        
            success: function(response){
//                console.log(response);
                
                if(response !== 'error' ){
                    var string1 = JSON.stringify(response);
                    var info = JSON.parse(string1);
                    console.log(info);
                    
                     $('#id_txtCodigo').val(info.enco_codigo);
                     $('.enco_tipo').html(info.enco_tipo);
                     $('.enco_pesomaxgramos').html(info.enco_pesomaxgramos);
                     $('.enco_precio_base').html(info.enco_precio_base);
                }

            },        
            error: function(error){
                console.log(error);
            }    
        });         
        
        $('.modal_tienda').fadeIn();
        
    });  
    
    
    $('.update_paquete').click(function (e){
        e.preventDefault();    
        var enco_codigo = $(this).attr('enco_codigo');
        var action = 'updatePaquete';
        
        $.ajax({
            url:'Detalle_Paquete/ActualizarDetallePaquete.jsp',
            type:'POST',
            async: true,
            data:{action:action, enco_codigo: enco_codigo},
        
            success: function(response){
                console.log(response);
                
                if(response !== 'error' ){
                    var string1 = JSON.stringify(response);
                    var info = JSON.parse(string1);
                    console.log(info);
                    
                     $('#id_txtCodigo').val(info.encomienda.enco_codigo);
                     $('.enco_tipo').html(info.encomienda.enco_tipo);
                     $('.enco_pesomaxgramos').html(info.encomienda.enco_pesomaxgramos);
                     $('.enco_precio_base').html(info.encomienda.enco_precio_base);
                     $('.dp_anchomax_cm').html(info.encomienda.dp_anchomax_cm);
                     $('.dp_largomax_cm').html(info.encomienda.dp_largomax_cm);
                     $('.dp_altomax_cm').html(info.encomienda.dp_altomax_cm);
                }

            },        
            error: function(error){
                console.log(error);
            }    
        });         
                
    
        
        $('.modal_paquete').fadeIn();
        
    });
    
    $('.update_servicio').click(function (e){
        e.preventDefault();    
        var serv_codigo = $(this).attr('serv_codigo');
        var action = 'updateServicio';
        
        $.ajax({
            url:'Servicio/ActualizarServicio.jsp',
            type:'POST',
            async: true,
            data:{action:action, serv_codigo: serv_codigo},
        
            success: function(response){
                console.log(response);
                
                if(response !== 'error' ){
                    var string1 = JSON.stringify(response);
                    var info = JSON.parse(string1);
                    console.log(info);
                    
                     $('#id_txtCodigo').val(info.serv_codigo);
                     $('.serv_tipo').html(info.serv_tipo);
                     $('.serv_precio_base').html(info.serv_precio_base);
                }

            },        
            error: function(error){
                console.log(error);
            }    
        });         
                
    
        
       $('.modal_tienda').fadeIn();
        
    }); 
    
    $('.update_articulo').click(function (e){
        e.preventDefault();    
        var art_codigo = $(this).attr('art_codigo');
        var action = 'updateArticulo';
        
        $.ajax({
            url:'Articulo/ActualizarArticulo.jsp',
            type:'POST',
            async: true,
            data:{action:action, art_codigo: art_codigo},
        
            success: function(response){
                console.log(response);
                
                if(response !== 'error' ){
                    var string1 = JSON.stringify(response);
                    var info = JSON.parse(string1);
                    console.log(info);
                    
                     $('#id_txtCodigo').val(info.art_codigo);
                     $('.art_codigo').html(info.art_codigo);
                     $('.art_descripcion').html(info.art_descripcion);
                }

            },        
            error: function(error){
                console.log(error);
            }    
        });         
                
       $('.modal_tienda').fadeIn();
        
    });        
       
    
});

function closeModal(){
    $('.modal_tienda').fadeOut();
}

function closeModalPaquete(){
    $('.modal_paquete').fadeOut();
}


