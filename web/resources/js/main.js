/*=========== SHOW MENU (mostrar menu version celular)==========================================================*/
const navMenu = document.getElementById('nav-menu'), /*id del menu*/
	  navToggle = document.getElementById('nav-toggle'), /*id del icono menu cel*/
	  navClose = document.getElementById('nav-close') /*icono de cerrar para el menu cel*/

 /*----- MENU SHOW ------*/
 /* Validar si el constante existe (abrir menu)*/
 if(navToggle){
 	navToggle.addEventListener('click', () =>{ /*hace click en icono toggle */
 		navMenu.classList.add('show-menu')    /* y muestra el menu*/
 	})

 }
/* Validar si el constante existe (cerrar menu)*/
if(navClose){
	navClose.addEventListener('click', () =>{ /*hace click en icono X*/
		navMenu.classList.remove('show-menu') /*remueve el mostrar menu (desarece el menu , cierra)*/
	})
}

/*-----Remove menu mobile---------*/
const navLink = document.querySelectorAll('.nav_link') /*clase del menu (contenido)*/

function linkAction(){
	const navMenu = document.getElementById('nav-menu') /*id del menu*/
	//cuando hacemos click en nav_link, se removera el show-menu class
	navMenu.classList.remove('show-menu')	
}
navLink.forEach(n => n.addEventListener('click', linkAction)) /*que corra todo lo dicho*/

/*=========== CHANGE BACKGROUND HEADER (cambio del fondo de color en el menu cuando deslizas)=======================*/
function scrollHeader(){
	const header= document.getElementById('header')/*todo el header*/
	//cuando la pantalla es 100 viewport (100 = distancia que bajara desde el inicio)
	if (this.scrollY >= 100)
	  header.classList.add('scroll-header'); /*cambiara color*/ /*nombramos este cambio con id inventado*/
	else 
		header.classList.remove('scroll-header') /*no cambia*/
}
window.addEventListener('scroll',scrollHeader) /*corre todo lo dicho*/

/*=========== SHOW SCROLL UP (boton al hacer click te lleva arriba) ===============================================*/
function scrollUp(){
	const scrollUp = document.getElementById('scroll-up'); /*nombramos el id uno inventado*/
	//cuando el scroll esta a 200 de distancia de la pantalla, mostrar icon
	if (this.scrollY >= 200)
		scrollUp.classList.add('show-scroll'); /*si es mas de 200 muestra*/
	else
		scrollUp.classList.remove('show-scroll') /* no muestra*/ 
}

window.addEventListener('scroll', scrollUp) /*corre todo lo dicho*/

/*=========== SCROLL SECTIONS ACTIVE LINK (para q se seleccione el link del menu (apariencia))===================*/
const sections = document.querySelectorAll('section[id]') /*se llamara desde la sección id*/

function scrollActive(){
    const scrollY = window.pageYOffset 

    sections.forEach(current => {
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop - 50; 
        sectionId = current.getAttribute('id')

        if(scrollY > sectionTop && scrollY <= sectionTop + sectionHeight){ /**cuando la pantalla e*/
            document.querySelector('.nav_menu a[href*=' + sectionId + ']').classList.add('active-link')
        }else{                    /*del clase menu a [hace referencia + a la seleccio id de la section] esto se pondra activo*/
            document.querySelector('.nav_menu a[href*=' + sectionId + ']').classList.remove('active-link')
        }						  /*del clase menu a [hace referencia + a la seleccio id de la section] esto se pondra inactivo*/
    })
}
window.addEventListener('scroll', scrollActive) /*corre todo lo dicho*/


/*=== Ocultar Sección -LOGIN============================================*/
const buttonLogin = document.getElementById('buttonUser')
const seccionLogin = document.getElementById('login')
const buttonClose = document.getElementById('icon-close')

	if (buttonLogin) {
		buttonLogin.addEventListener('click', ()=>{			
				seccionLogin.classList.toggle('login-active')

				if ('login-active') {
					seccionLogin.classList.remove('.loginNone')

				}
						
		})
                

	}
        
        if (buttonClose ) {
		buttonClose .addEventListener('click', ()=>{			
				seccionLogin.classList.toggle('login-active')

				if ('login-active') {
					seccionLogin.classList.remove('.loginNone')

				}
						
		});
                

	}



/*========= SCROLL REVEAL ANIMATION (animacion al entrar por primera vez)====================*/
const sr = ScrollReveal({
	distance: '60px', /*la animacion empesara a una distancia de 60px*/
	duration: 2800, /*durara unos segundos*/
	reset: false, /*falso para q solo sea una vez al entrar, 
					verdadero para que cada vez q vallas a esa seccion suceda la animacion
					(verdadero, no recomendar xq los iconos_social desaparece)*/
})
/*Animacion Desde arriba*/      /*se pone las clases q deseas q tengan esa animacion, "" es dolo para 1, `` para muchos
								al final no se pone ",", si sucede no funciona*/
sr.reveal(`.home_data,      
	        .home_info,
	        .nuestro_servicio_titulo,
	        .nuestro_servicio_info,
	        .footer_data,
	        .footer_rights,
	        .columnaBarra,
	        .tituloRegistro,
	        .boton`,{
	origin: 'top', /*desde arriba*/
	interval: 100, /*un intervalo de 100segundos*/
})
/*Animacion Desde izquierda*/
sr.reveal(`.home_social-link,
	       .nuestro_servicio_serivio,
	       .xq_trabajo_titulo,
	       .xq_trabajo_respuesta,
	       .empresa_titulo,
	       .empresa_img,
	       .mision_vision_superCard`,{
	origin: 'left',
})
/* Animacion Desde derecha*/
sr.reveal(`.home_button,
	       .xq_trabajo_cajaImg,
	       .empresa_subtitulo,
	       .empresa_respuesta

	       `,{
	origin: 'right',
	interval: 100,
})