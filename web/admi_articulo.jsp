<%-- 
    Document   : admi_articulo
    Created on : 02-dic-2021, 21:28:30
    Author     : Usuario
--%>

<%@page import="modelo.Articulo"%>
<%@page import="modeloDAO.ArticuloDAO"%>
<%@page import="Interfaces.Interface_Articulo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial- ">
	<title>Adminitrador</title>

	<!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
	<link rel="shortcut icon" href="#" type="image/png">

	<!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
	<link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

	<!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
	<link rel="stylesheet" type="text/css" href="resources/css/estilo_admi.css">
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi_2.css">
        <script type="text/javascript" src="resources/js/jquery-3.6.0.min.js"> </script>
</head>

<div class="modal_tienda" >
    <div class="bodyModalTienda"> 
        <section>
            <form action="Control_Articulo" method="post" name="frmUpdateArticulo" id="id_frmUpdateArticulo" >
                <h1> ACTUALIZAR ARTICULO </h1>
                <h2>
                    <label class="art_codigo"> </label> <br>
                </h2>
                <h2> Descripcion <input type="text" name="txtDescripcion" id="id_txtDescripcion"> <br></h2> 
                <input type="hidden" name="txtCodigo" id="id_txtCodigo" required>
                <button type="submit" name="accion" value="actualizaArticulo"> Actualizar </button>
                <a href="#" class="closeModalTienda" onclick="closeModal()"> Cerrar </a>
            </form>
        </section>
    </div>              
</div> 


<%
    
 Interface_Articulo interface_articulo = new ArticuloDAO();
%>


<body id="body-pd">

	<!--========= NAVAR 1=================================================-->
	<div class="l-navbar" id="navbar">
		<nav class="nav">
			<div>
				<div class="nav_brand">
					<i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
					<a href="" class="nav_logo">Fast Courier</a>
				</div>
				<div class="nav_list">
					<div class="nav_link desplegue">
						<i class="nav_icon ri-truck-fill"></i>
						<span class="nav_name">Ruta</span>	
						<i class="ri-arrow-down-s-line desplegue_link"></i>		

						<ul class="desplegue_menu">
							<a href="admi_actuRuta_1.jsp" class="desplegue_sublink">Actualizaciones</a>
							<a href="admi_tiendaRuta_1.jsp" class="desplegue_sublink">Tiendas</a>
						</ul>			
					</div>
					<a href="admi_servicio.jsp" class="nav_link">
						<i class="nav_icon ri-service-fill"></i>
						<span class="nav_name">Servicios</span>						
					</a>
					<a href="admi_articulo.jsp" class="nav_link active">
						<i class="nav_icon ri-article-fill"></i>
						<span class="nav_name">Articulo</span>						
					</a>
					<a href="admi_encomienda.jsp" class="nav_link">
						<i class="nav_icon ri-red-packet-fill"></i>
						<span class="nav_name">Encomienda</span>						
					</a>
				</div>
			</div>

			<a href="" class="nav_link">
				<i class="nav_icon ri-logout-circle-r-fill"></i>
				<span class="nav_name">Salir</span>						
			</a>
		</nav>
	</div>
<!--========= NAVAR 2 TITLE ========================================-->
	<div class="main">
		<div class="nav_vertical">
			<div class="nav_start">
				<h1 class="nav_vertical_title">Nuestros Articulos</h1>
			</div>
			<div class="nav_title">
				<a href="index.jsp"><button class="button_normal">Ver Web </button></a>
				<div class="nav_title_userAdmi">
					<img class="img_userAdmi" src="resources/img/icon-5359553_960_720.png">
					<div class="nav_userAdmi_cont">
						<h3>Adminitrador</h3>
						<span>Super Admi</span>
					</div>
				</div>
			</div>
		</div>
	</div>
<!--========= CONTENIDO =============================================-->
<main>
<div class="nuestraTienda">
	<div class="tienda_tablaTotales">
		<!--TOTALES Y FILTRO -------------------------------------------------->
		<div class="tienda_totales">
                        <section class="tienda_total_cont">
                                <div class="total_info" id="id_TotalArticulos">
                                    <p class="total_num"> <%= interface_articulo.listarCantidad() %></p>
                                    <span>Total de Articulos</span>
				</div>
				<i class=" icon_total ri-landscape-fill"></i>
			</section>
                       
		</div>
		<!--FITRO BUSCAR---------------------------------------------------->
		<div class="tienda_filtroBusca">
			<section class="tienda_filtroProv_cont">
				<div class="filtro_tienda_name filtro_tienda">
                                    <i class="icon_buscar ri-search-2-line"></i>
					<h3>Tienda</h3>
                                        <form id="id_frmBuscarArticulo" method="post">
                                            <input type="text" placeholder="Buscar Articulo" id="buscador" name="txtParametroBusqueda" onkeyup="busqueda()">
                                        </form>
				</div>
			</section>
		</div>
		<!--TABLAS Y PROINCIAS ---------------------------------------------------->
                <div class="tienda_tablas">
			<!--Tabla 1 ---------------------------------------------------->
                        <div class="tabla">
				<section class="tabla_capa">
					<div class="tabla_tienda">
                                            <table class="tabla_tienda_cont tienda" id="id_tblArticulo" name="tblArticulo">
							<thead>
								<tr> 
                                                                    <td>  </td>
                                                                    <td>  </td>
                                                                    <td>Código</td>
                                                                    <td>Articulo</td>
								</tr>
							</thead>
                                                        <tbody>
                                                            <%
                                                                for(Articulo articulo: interface_articulo.listar()){
                                                            %>    
								<tr>
                                                                    <td><a href="Control_Articulo?accion=eliminaArticulo&art_codigo=<%= articulo.getArt_codigo() %>"><i class="icon_close ri-close-circle-fill"></i></a></td>
                                                                    <td><a class="update_articulo" id="id_update_articulo" art_codigo="<%=articulo.getArt_codigo()%>" href="#"><i class="ri-edit-fill"></i></a></td>
                                                                    <td> <%= articulo.getArt_codigo() %></td>
                                                                    <td> <%= articulo.getArt_descripcion() %> </td>
								</tr>
                                                            <%
                                                               }
                                                            %>   
							</tbody>
                                            </table>
					</div>	
				</section>
			</div>

			<!--Tabla 2 ---------------------------------------------------->
			
            </div>
	</div>

	<!--TABLAS ADICIONAR ---------------------------------------------------->
	<div class="tienda_adicionar">
		<section class="add_seccion adicionar_prov">
                    <form action="Control_Articulo" method="post">
			<h2><b>Nuevos<br>Articulos</b></h2>
			<h3>Articulo</h3>
			<input type="text" name="txtDescripcion" placeholder="Descripcion">
                        <button type="submit" class="button_add" name="accion" value="agregarArticulo">Agregar</button>
                    </form>    
		</section>
		
	</div>                        
</div>
</main>
	<!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/scrollreveal.min.js"></script>

	<!--========= NUESTRO JS (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/main_admi.js"></script>
        <script type="text/javascript" src="resources/js/Funciones2.js"></script>
        <script type="text/javascript"> 
            
            function busqueda(){
                var input, filtro, table, tr, referencia1,referencia2, i, x,y;
                input = document.getElementById("buscador");
                filtro = input.value.toUpperCase();
                table = document.getElementById("id_tblArticulo");
                tr = table.getElementsByTagName("tr");
                
                for (i = 1; i < tr.length; i++) {
                referencia1 = tr[i].getElementsByTagName("td")[2];
                referencia2 = tr[i].getElementsByTagName("td")[3];
                if (referencia1) {
                  x = referencia1.textContent || referencia1.innerText;
                  y = referencia2.textContent || referencia2.innerText;
                  if (x.toUpperCase().indexOf(filtro) > -1 || 
                          y.toUpperCase().indexOf(filtro) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }
                }
                
                 $.post("Articulo/TotalArticulos.jsp",$("#id_frmBuscarArticulo").serialize(),function(data){$("#id_TotalArticulos").html(data);});
                
            }
            
        </script>   
	
</body>
</html>