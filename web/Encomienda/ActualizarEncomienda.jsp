
<%@page import="Interfaces.Interface_Encomienda"%>

<%@page import="modeloDAO.EncomiendaDAO"%>

<%@page import="modelo.Encomienda"%>

<%@page import="com.google.gson.Gson"%>


<%  
    
    Interface_Encomienda interface_encomienda = new EncomiendaDAO();

    String accion = request.getParameter("action");
    
    if (!accion.isEmpty()) {

        if (accion.equals("updateEncomienda")) {
            
            String enco_codigo = request.getParameter("enco_codigo");
            Encomienda encomienda = interface_encomienda.obtenerUno(enco_codigo);

            if(encomienda != null){
                
                String json = new Gson().toJson(encomienda);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                    
            }
        }

    }


%>
