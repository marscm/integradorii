<%@page import="Interfaces.Interface_Encomienda"%>

<%@page import="modeloDAO.EncomiendaDAO"%>

<%@page import="modelo.Encomienda"%>


<%
    Interface_Encomienda interface_encomienda = new EncomiendaDAO();
    
    String parametro_busqueda = request.getParameter("txtParametroBusqueda");
%>


<div id="id_TotalEncomiendas">
    <section class="totalEnco">
        <div class="total_info">
            <p class="total_num"> <%= interface_encomienda.listarCantidad(parametro_busqueda)%></p>
            <span>Total de Encomiendas</span>
        </div>
        <i class="icon_total ri-archive-fill"></i>
    </section>
</div>