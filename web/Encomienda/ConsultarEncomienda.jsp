
<%@page import="modeloDAO.Detalle_PaqueteDAO"%>
<%@page import="modeloDAO.EncomiendaDAO"%>
<%@page import="modelo.Encomienda"%>
<%@page import="modelo.DetallePaquete"%>
<%@page import="Interfaces.Interface_Detalle_Paquete"%>
<%@page import="Interfaces.Interface_Encomienda"%>

<%

    Interface_Encomienda interface_encomienda = new EncomiendaDAO();
    Interface_Detalle_Paquete interface_detalle_paquete = new Detalle_PaqueteDAO();
    
    String parametro_busqueda = request.getParameter("txtParametroBusqueda");

%>


<table class="tabla_enco_cont" id="id_tblEncomienda">
    <thead>
        <tr> 
            <td>  </td>
            <td>  </td>
            <td>C�digo</td>
            <td>Tipo</td>
            <td>Peso</td>
            <td>Precio</td>
            <td>Detalle</td>
        </tr>
    </thead>
    <tbody>
        <%
            for (Encomienda encomienda : interface_encomienda.listar(parametro_busqueda)) {
        %>  

        <tr>
            <td><a href="Control_Encomienda?accion=eliminaEncomienda&codigo=<%= encomienda.getEnco_codigo()%>"><i class="icon_close ri-close-circle-fill"></a></i></td>
            <%
                if (interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo()) != null) {
                    DetallePaquete detalle_paquete = interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo());
            %>       
            <td><a class="update_paquete" id="id_update_paquete" enco_codigo="<%=encomienda.getEnco_codigo()%>" href="#"><i class="icon_edit ri-edit-2-fill"></i></a></td> 
                    <%
                    } else {
                    %>
            <td><a class="update_encomienda" id="id_update_encomienda" enco_codigo="<%=encomienda.getEnco_codigo()%>" href="#"><i class="icon_edit ri-edit-2-fill"></i></a></td> 
                    <%
                        }
                    %>
            <td> <%= encomienda.getEnco_codigo()%> </td>
            <td> <%= encomienda.getEnco_tipo()%> </td>
            <td> <%= encomienda.getEnco_pesoMaxGramos() %> </td>
            <td> <%= encomienda.getEnco_precioBase() %> </td>
            <%
                if (interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo()) != null) {
                    DetallePaquete detalle_paquete = interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo());
            %>       
            <td> <%= detalle_paquete.getDP_anchoMax() + " ancho,  " + detalle_paquete.getDP_largoMax() + " largo, " + detalle_paquete.getDP_altoMax() + " alto."%> </td> 
            <%
            } else {
            %>
            <td> Sin detalle </td> 
            <%
                }
            %>
        </tr>

        <%
            }
        %>
    </tbody>
</table>
<script type="text/javascript" src="resources/js/Funciones2.js"></script>
