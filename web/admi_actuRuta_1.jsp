<%-- 
    Document   : admi_actuRuta
    Created on : 25/10/2021, 08:25:39 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Ruta"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.RutaDAO"%>
<%@page import="modelo.Provincia"%>
<%@page import="modelo.Ruta"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial- ">
        <title>Adminitrador</title>

        <!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
        <link rel="shortcut icon" href="#" type="image/png">

        <!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">

        <!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi.css">
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi_2.css">
        <script type="text/javascript" src="resources/js/jquery-3.6.0.min.js"></script>

    </head>

    <div class="modal_tienda" >
        <div class="bodyModalTienda"> 
            <section>
                <form action="Control_Ruta" method="post" name="frmUpdateRuta" id="id_frmUpdateRuta" >
                    <h1> ACTUALIZAR PRECIO DE RUTA </h1>
                    <h2>
                        <label class="ruta_origen"> </label> - <label class="ruta_sede_origen"></label> -> <label class="ruta_destino"></label> - <label class="ruta_sede_destino"></label> <br>
                    </h2>
                    <h2>Precio <input type="text" name="txtPrecio" id="id_txtPrecio">  <br></h2> 
                    <input type="hidden" name="txtCodigo" id="id_txtCodigo" required>
                    <div class="alertUpdateTienda">  </div>
                    <button type="submit" name="accion" value="actualizaRuta"> Actualizar </button>
                    <a href="#" class="closeModalTienda" onclick="closeModal()"> Cerrar </a>
                </form>
            </section>
        </div>              
    </div> 

    <%
        Interface_Provincia interface_provincia = new ProvinciaDAO();
        Interface_Ruta interface_ruta = new RutaDAO();
    %>

    <body id="body-pd">

        <!--========= NAVAR 1=================================================-->
        <div class="l-navbar" id="navbar">
            <nav class="nav">
                <div>
                    <div class="nav_brand">
                        <i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
                        <a href="" class="nav_logo">Fast Courier</a>
                    </div>
                    <div class="nav_list">
                        <div class="nav_link active desplegue">
                            <i class="nav_icon ri-truck-fill"></i>
                            <span class="nav_name">Ruta</span>	
                            <i class="ri-arrow-down-s-line desplegue_link"></i>		

                            <ul class="desplegue_menu">
                                <a href="admi_actuRuta_1.jsp" class="desplegue_sublink">Actualizaciones</a>
                                <a href="admi_tiendaRuta_1.jsp" class="desplegue_sublink">Tiendas</a>
                            </ul>			
                        </div>
                        <a href="admi_servicio.jsp" class="nav_link">
                            <i class="nav_icon ri-service-fill"></i>
                            <span class="nav_name">Servicios</span>						
                        </a>
                        <a href="admi_articulo.jsp" class="nav_link">
                            <i class="nav_icon ri-article-fill"></i>
                            <span class="nav_name">Articulo</span>						
                        </a>
                        <a href="admi_encomienda.jsp" class="nav_link">
                            <i class="nav_icon ri-red-packet-fill"></i>
                            <span class="nav_name">Encomienda</span>						
                        </a>
                    </div>
                </div>

                <a href="index.jsp" class="nav_link">
                    <i class="nav_icon ri-logout-circle-r-fill"></i>
                    <span class="nav_name">Salir</span>						
                </a>
            </nav>
        </div>
        <!--========= NAVAR 2 TITLE ========================================-->

        <div class="main">
            <div class="nav_vertical">
                <div class="nav_start">
                    <h1 class="nav_vertical_title">Actualizar Rutas</h1>
                </div>
                <div class="nav_title">
                    <a href="index.jsp"><button class="button_normal">Ver Web </button></a>
                    <div class="nav_title_userAdmi">
                        <img class="img_userAdmi" src="resources/img/icon-5359553_960_720.png">
                        <div class="nav_userAdmi_cont">
                            <h3>Adminitrador</h3>
                            <span>Super Admi</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--========= CONTENIDO =============================================-->
        <!--TOTALES Y FILTRO -------------------------->
        <main>
            <div class="total_filtro"> 

                <section class="total_rutas">
                    <div class="total_info" id="id_dvTotalRutas">
                        <p class="total_num"> <%= interface_ruta.listarCantidad("Todas", "")%></p>
                        <span>Total de Rutas</span>
                    </div>
                    <i class="icon_total ri-route-fill"></i>
                </section>

                <section class="filtro_rutas">
                    <form id="id_frmBuscarOrigenDestino" method="post">
                        <div class="filtro_rutas_name filtro_origen">
                            <h3>Elige Origen</h3>
                            <select class="filtro_rutas_opcion" id="id_cmbOrigen" name="cmbOrigen" onchange="buscarOrigen()">
                                <option> Elegir </option>
                                <%
                                    for (Provincia provincia : interface_provincia.listar()) {
                                %>         
                                <option> <%= provincia.getProv_nombre()%> </option>  
                                <%
                                    }
                                %> 
                            </select>
                        </div>
                        <div class="filtro_rutas_name filtro_destino">
                            <h3>Elige Destino</h3>
                            <select class="filtro_rutas_opcion" id="id_cmbDestino" name="cmbDestino" onchange="buscarDestino()">
                                <option> Elegir </option>
                                <%
                                    for (Provincia provincia : interface_provincia.listar()) {
                                %>         
                                <option> <%= provincia.getProv_nombre()%> </option>  
                                <%
                                    }
                                %> 
                            </select>
                        </div>
                    </form>            
                </section>
            </div>

            <!--TABLA -------------------------->
            <div class="tabla">
                <section class="tabla_capa">
                    <div class="tabla_ruta">
                        <table class="tabla_ruta_cont" id="id_tblRuta">
                            <thead>
                                <tr> 
                                    <td>  </td>
                                    <td>  </td>
                                    <td>Código</td>
                                    <td>Origen</td>
                                    <td>Sede de Origen</td>
                                    <td>Destino</td>
                                    <td>Sede de Destino</td>
                                    <td>Precio</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (Ruta ruta : interface_ruta.listar()) {
                                %>         
                                <tr>
                                    <td><a href="Control_Ruta?accion=eliminaRuta&ruta_codigo=<%=ruta.getRuta_codigo()%>"><i class="icon_close ri-close-circle-fill"></i></a></td>
                                    <td><a class="update_ruta" id="id_update_ruta" ruta_codigo="<%=ruta.getRuta_codigo()%>" href="#"><i class="icon_edit ri-edit-2-fill"></i></a></td>
                                    <td><%= ruta.getRuta_codigo()%></td>
                                    <td><%= ruta.getRuta_origen()%></td>
                                    <td><%= ruta.getRuta_sede_origen()%></td>
                                    <td><%= ruta.getRuta_destino()%></td>
                                    <td><%= ruta.getRuta_sede_destino()%></td>
                                    <td><%= ruta.getRuta_precio_base()%></td>
                                </tr>
                                <%
                                    }
                                %> 
                            </tbody>
                        </table>
                    </div>
                    <a href="#seccion-add"><i class="icon_add ri-add-circle-fill" id="icon-add"  ></i></a>
                </section>

            </div>

            <!--TABLA ADD ------------------------------------------>
            <div class="tabla_ruta_add" id="seccion-add">
                <section >
                    <div class="tabla_add ">
                        <form action="Control_Ruta" id="id_frmProvincia" method="post">
                            <table class="tabla_add_cont">
                                <thead >
                                    <tr> 
                                        <td>  </td>
                                        <td>Origen</td>
                                        <td>Sede de Origen</td>
                                        <td>Destino</td>
                                        <td>Sede de Destino</td>
                                        <td>Precio</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><button class="button_add" name="accion" type="submit" value="agregarRuta">Agregar</button></td>
                                        <td>
                                            <select class="add_rutas_opcion" name="cmbProvincia1" onchange="llenaCmbTienda()">
                                                <option>Elegir</option>
                                                <%
                                                    for (Provincia provincia : interface_provincia.listar()) {
                                                %>         
                                                <option> <%= provincia.getProv_nombre()%> </option>  
                                                <%
                                                    }
                                                %> 
                                            </select>
                                        </td>
                                        <td>
                                            <select class="add_rutas_opcion" name="cmbTienda1" id="id_cmbTienda1">
                                                <option> Elegir </option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="add_rutas_opcion" name="cmbProvincia2" onchange="llenaCmbTienda2()">
                                                <option>Elegir</option>
                                                <%
                                                    for (Provincia provincia : interface_provincia.listar()) {
                                                %>         
                                                <option> <%= provincia.getProv_nombre()%> </option>  
                                                <%
                                                    }
                                                %> 
                                            </select>
                                        </td>
                                        <td>
                                            <select class="add_rutas_opcion" name="cmbTienda2" id="id_cmbTienda2">
                                                <option>Elegir</option>
                                            </select>
                                        </td>
                                        <td><input type="text" name="txtPrecio" placeholder="s/100.00"></td>
                                    </tr>
                                </tbody>    
                            </table>
                        </form>
                    </div>
                </section>
            </div>
        </main>

        <!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="js/scrollreveal.min.js"></script>

        <!--========= NUESTRO JS (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="resources/js/main_admi.js"></script>
        <script type="text/javascript" src="resources/js/Funciones2.js"></script>
        <script type="text/javascript">

                                function llenaCmbTienda() {

                                    $.post("Ruta/cmbProvincia1.jsp", $("#id_frmProvincia").serialize(), function (data) {
                                        $("#id_cmbTienda1").html(data);
                                    });
                                }

                                function llenaCmbTienda2() {

                                    $.post("Ruta/cmbProvincia2.jsp", $("#id_frmProvincia").serialize(), function (data) {
                                        $("#id_cmbTienda2").html(data);
                                    });
                                }

                                function buscarRuta() {

                                    $.post("Ruta/txtBuscarRuta.jsp", $("#id_frmBuscarRuta").serialize(), function (data) {
                                        $("#id_tblRuta").html(data);
                                    });
                                    $.post("Ruta/txtBuscarRutaTotalRutas.jsp", $("#id_frmBuscarRuta").serialize(), function (data) {
                                        $("#id_dvTotalRutas").html(data);
                                    });

                                }

                                function buscarOrigen() {

                                    $.post("Ruta/cmbOrigen.jsp", $("#id_frmBuscarOrigenDestino").serialize(), function (data) {
                                        $("#id_tblRuta").html(data);
                                    });
                                    $.post("Ruta/cmbOrigenTotalRutas.jsp", $("#id_frmBuscarOrigenDestino").serialize(), function (data) {
                                        $("#id_dvTotalRutas").html(data);
                                    });
                                }

                                function buscarDestino() {

                                    $.post("Ruta/cmbDestino.jsp", $("#id_frmBuscarOrigenDestino").serialize(), function (data) {
                                        $("#id_tblRuta").html(data);
                                    });
                                    $.post("Ruta/cmbDestinoTotalRutas.jsp", $("#id_frmBuscarOrigenDestino").serialize(), function (data) {
                                        $("#id_dvTotalRutas").html(data);
                                    });

                                }


        </script>


    </body>
</html>