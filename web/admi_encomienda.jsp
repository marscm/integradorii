<%-- 
    Document   : admi_encomienda
    Created on : 18/11/2021, 01:00:16 AM
    Author     : Dell
--%>

<%@page import="modelo.DetallePaquete"%>
<%@page import="modeloDAO.Detalle_PaqueteDAO"%>
<%@page import="Interfaces.Interface_Detalle_Paquete"%>
<%@page import="modelo.Encomienda"%>
<%@page import="modeloDAO.EncomiendaDAO"%>
<%@page import="Interfaces.Interface_Encomienda"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial- "/>
        <title>Administrador</title>
        
        <!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============-->
        <link rel="shortcut icon" href="" type="image/png">
        
        <!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" >
        
        <!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi.css">
        <script type="text/javascript" src="resources/js/jquery-3.6.0.min.js"> </script>
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_admi_2.css">
        <link rel="stylesheet" type="text/css" href="resources/css/estilo_paquete.css">
        
    </head>
    

     <div class="modal_tienda" >
        <div class="bodyModalTienda"> 
            <section>
                <form action="Control_Encomienda" method="post" name="frmUpdateEncomienda" id="id_frmUpdateEncomienda" >
                <h1> ACTUALIZAR ENCOMIENDA </h1>
                <h2>
                <label class="enco_tipo"> </label> <br>
                </h2>
                <h2> Peso <input type="text" name="txtPeso" id="id_txtPeso"> <br></h2> 
                <h2> Precio <input type="text" name="txtPrecio" id="id_txtPrecio"> <br></h2> 
                <input type="hidden" name="txtCodigo" id="id_txtCodigo" required>
                <div class="alertUpdateTienda">  </div>
                <button type="submit" name="accion" value="actualizaEncomienda"> Actualizar </button>
                <a href="#" class="closeModalTienda" onclick="closeModal()"> Cerrar </a>
            </form>
            </section>
        </div>              
    </div>  
    
    
    <div class="modal_paquete" >
        <div class="bodyModalPaquete"> 
            <section>
                <form action="Control_DetallePaquete" method="post" name="frmUpdatePaquete" id="id_frmUpdatePaquete" >
                <h1> ACTUALIZAR ENCOMIENDA </h1>
                <h2>
                <label class="enco_tipo"> </label> <br>
                </h2>
                <h2> Peso <input type="text" name="txtPeso" id="id_txtPeso"> <br></h2> 
                <h2> Precio <input type="text" name="txtPrecio" id="id_txtPrecio"> <br></h2> 
                <h2> Ancho <input type="text" name="txtAncho" id="id_txtPrecio"> <br></h2> 
                <h2> Largo <input type="text" name="txtLargo" id="id_txtPrecio"> <br></h2> 
                <h2> Alto <input type="text" name="txtAlto" id="id_txtPrecio"> <br></h2> 
                <input type="hidden" name="txtCodigo" id="id_txtCodigo" required>
                <button type="submit" name="accion" value="actualizaDetallePaquete"> Actualizar </button>
                <a href="#" class="closeModalPaquete" onclick="closeModalPaquete()"> Cerrar </a>
            </form>
            </section>
        </div>              
    </div>      
    
    
     
    <%
        
        Interface_Encomienda interface_encomienda = new EncomiendaDAO();
        Interface_Detalle_Paquete interface_detalle_paquete = new Detalle_PaqueteDAO();
        
    %>
    
    <body id="body-pd">
          <div class="l-navbar" id="navbar">
            <nav class="nav">
                <div>
                    <div class="nav_brand">
                        <i class="nav_toggle ri-menu-line" id="nav-toggle"></i>
                        <a href="" class="nav_logo">Fast Courier</a>
                    </div>
                    
				<div class="nav_list">
					<div class="nav_link desplegue">
						<i class="nav_icon ri-truck-fill"></i>
						<span class="nav_name">Ruta</span>	
						<i class="ri-arrow-down-s-line desplegue_link"></i>		

						<ul class="desplegue_menu">
							<a href="admi_actuRuta_1.jsp" class="desplegue_sublink">Actualizaciones</a>
							<a href="admi_tiendaRuta_1.jsp" class="desplegue_sublink">Tiendas</a>
						</ul>			
					</div>
					<a href="admi_servicio.jsp" class="nav_link">
						<i class="nav_icon ri-service-fill"></i>
						<span class="nav_name">Servicios</span>						
					</a>
					<a href="admi_articulo.jsp" class="nav_link">
						<i class="nav_icon ri-article-fill"></i>
						<span class="nav_name">Articulo</span>						
					</a>
					<a href="admi_encomienda.jsp" class="nav_link active">
						<i class="nav_icon ri-red-packet-fill"></i>
						<span class="nav_name">Encomienda</span>						
					</a>
				</div>
                </div>

                <a href="" class="nav_link">
                    <i class="nav_icon ri-logout-circle-r-fill"></i>
                    <span class="nav_name">Salir</span>						
                </a>
            </nav>
        </div>
        
        <!--========= NAVAR 2 TITLE ========================================-->
        <div class="main">
            <div class="nav_vertical">
                
                <div class="nav_start">
 
                    <h1 class="nav_vertical_title">Encomienda</h1>

                </div>
                
                <div class="nav_title">
                    <div class="nav_title_buscar">
                        <form id="id_frmBuscarEncomienda">
                            <i class="icon_buscar ri-search-2-line"></i>
                            <input type="text" name="txtParametroBusqueda" placeholder="Buscar Encomienda" onkeyup="buscarEncomienda()">
                        </form>
                    </div>
                    <a href="index.jsp"><button class="button_normal">Ver Web </button></a>
                    <div class="nav_title_userAdmi">
                        <img class="img_userAdmi" src="resources/img/icon-5359553_960_720.png">
                            <div class="nav_userAdmi_cont">
                                <h3>Adminitrador</h3>
                                <span>Super Admi</span>
                            </div>
                    </div>
                </div>
            </div>
        </div>
<!--========= CONTENIDO =============================================-->
<main>
    <!--========= TOTALES Y BUSCA==========================================-->
     <!--========= TOTALES =====-->
    <div class=" buscar_total_Enco" >
         <!--====BUSCA================-->
         <div id="id_TotalEncomiendas">
                <section class="totalEnco">
                    <div class="total_info">
                        <p class="total_num"> <%= interface_encomienda.listarCantidad() %></p>
                        <span>Total de Encomiendas</span>
                    </div>
                    <i class="icon_total ri-archive-fill"></i>
                </section>
          </div>
    </div>
    <!--========= NEW Y TABLE================================================-->
     <!--====NEW ENCOMIENDA================-->
    <div class=" new_table_Enco">
         <div class="newEnco">
                <section>
                    <h3>Nueva Encomienda</h3>
                    <form action="Control_Encomienda" class="forbottum" method="post">
                        <div class="alinearEnco">
                            
                       <div >
                                <label>Tipo</label>
                                <input type="text" name="txtTipo" placeholder="Caja Grande">
                          </div>
                          <div>
                                <label>Peso</label>
                               <input type="text" name="txtPeso" placeholder="840">
                                <span>gr</span>
                          </div>
                       <div>
                                <label>Precio</label>
                                <input type="text" name="txtPrecio" placeholder="45.00">
                                <span>soles</span>
                          </div>
                        </div>
                        <div class="button_volumen">     
                            <i class="icon_add ri-add-circle-fill" id="icon-add-volum"><span>Volumen<span></i>
                           
                       </div >
                        
                       <div class="new_volumen" id="seccion-add-volum">
                           <div class="alinearEnco">
                            <div>
                                     <label>Ancho</label>
                                    <input type="text" name="txtAncho" placeholder="458">
                                     <span>cm</span>
                               </div>
                           <div>
                                     <label>Largo</label>
                                     <input type="text" name="txtLargo" placeholder="784">
                                     <span>cm</span>
                               </div>
                           <div>
                                     <label>Alto</label>
                                   <input type="text" name="txtAlto" placeholder="540">
                                    <span>cm</span>
                               </div>
                        </div>
                           </div>
                       <button type="submit" class="button_add" name="accion" value="agregarEncomienda">Agregar</button>
                    </form>
                    
                </section>
          </div>
    </div>   
         <!--====TABLA ENCOMIENDA================-->
        <div class="tableEnco">
            <section class="tabla_capa">
                <div class="tabla_enco">
                    <table class="tabla_enco_cont" id="id_tblEncomienda">
                        <thead>
                            <tr> 
                                <td>  </td>
                                <td>  </td>
                                <td>Código</td>
                                <td>Tipo</td>
                                <td>Peso</td>
                                <td>Precio</td>
                                <td>Detalle</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                               for(Encomienda encomienda: interface_encomienda.listar()){ 
                            %>  
                            
                            <tr>
                                <td><a href="Control_Encomienda?accion=eliminaEncomienda&codigo=<%= encomienda.getEnco_codigo()%>"><i class="icon_close ri-close-circle-fill"></a></i></td>
                                <%
                                    if(interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo()) != null){
                                        DetallePaquete detalle_paquete = interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo());
                                %>       
                                        <td><a class="update_paquete" id="id_update_paquete" enco_codigo="<%=encomienda.getEnco_codigo() %>" href="#"><i class="icon_edit ri-edit-2-fill"></i></a></td> 
                                <%       
                                    }else{
                                %>
                                    <td><a class="update_encomienda" id="id_update_encomienda" enco_codigo="<%=encomienda.getEnco_codigo() %>" href="#"><i class="icon_edit ri-edit-2-fill"></i></a></td> 
                                <%
                                    }
                                %>
                                <td> <%= encomienda.getEnco_codigo() %> </td>
                                <td> <%= encomienda.getEnco_tipo() %> </td>
                                <td> <%= encomienda.getEnco_pesoMaxGramos() %> </td>
                                <td> <%= encomienda.getEnco_precioBase() %> </td>
                                <%
                                    if(interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo()) != null){
                                        DetallePaquete detalle_paquete = interface_detalle_paquete.obtenerUno(encomienda.getEnco_codigo());
                                %>       
                                        <td> <%= detalle_paquete.getDP_anchoMax() +  " ancho,  " + detalle_paquete.getDP_largoMax() + " largo, " + detalle_paquete.getDP_altoMax() + " alto."  %> </td> 
                                <%       
                                    }else{
                                %>
                                    <td> Sin detalle </td> 
                                <%
                                    }
                                %>
                            </tr>
                            
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                </div>
               
            </section>
    </div>
</main>
        <!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)==========
	<script type="text/javascript" src="js/scrollreveal.min.js"></script>===-->

	<!--========= NUESTRO JS (COPIAR PEGAR)=============-->
	<script type="text/javascript" src="resources/js/main_admi.js"></script>
        <script type="text/javascript" src="resources/js/Funciones2.js"></script>
        <script type="text/javascript">
            
            function buscarEncomienda(){
                
                $.post("Encomienda/ConsultarEncomienda.jsp",$("#id_frmBuscarEncomienda").serialize(),function(data){$("#id_tblEncomienda").html(data);});
                $.post("Encomienda/TotalEncomiendas.jsp",$("#id_frmBuscarEncomienda").serialize(),function(data){$("#id_TotalEncomiendas").html(data);});
            }
        </script>
        
        
    </body>
</html>