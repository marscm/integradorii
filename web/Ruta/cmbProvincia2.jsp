<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Tienda"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.TiendaDAO"%>
<%@page import="modelo.Tienda"%>


<%  
    Interface_Provincia interface_provincia = new ProvinciaDAO();
    Interface_Tienda interface_tienda = new TiendaDAO();
    
    String prov_nombre = request.getParameter("cmbProvincia2");
    String prov_codigo = interface_provincia.buscarCodigo(prov_nombre);
%>


<select class="add_rutas_opcion" name="Provincia" id="id_cmbTienda2">
    <%
        for (Tienda tienda: interface_tienda.listar(prov_codigo)) {
    %>         
        <option> <%= tienda.getTiend_nombre() %> </option>  
    <%
        }
    %> 
</select>