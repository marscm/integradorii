<%@page import="Interfaces.Interface_Ruta"%>
<%@page import="modeloDAO.RutaDAO"%>
<%@page import="modelo.Ruta"%>

<%  
    Interface_Ruta interface_ruta = new RutaDAO();
    String ruta_origen = request.getParameter("cmbOrigen");   
%>


<table class="tabla_ruta_cont" id="id_tblRuta">
    <thead>
        <tr> 
            <td>  </td>
            <td>  </td>
            <td>C�digo</td>
            <td>Origen</td>
            <td>Sede de Origen</td>
            <td>Destino</td>
            <td>Sede de Destino</td>
            <td>Precio</td>
        </tr>
    </thead>
    <tbody>
        <%
            for (Ruta ruta : interface_ruta.listarSegunOrigen(ruta_origen)) {
        %>         
        <tr>
            <td><a href="Control_Ruta?accion=eliminaRuta&ruta_codigo=<%=ruta.getRuta_codigo() %>"><i class="icon_close ri-close-circle-fill"></i></a></td>
            <td><a class="update_ruta" id="id_update_ruta" ruta_codigo="<%=ruta.getRuta_codigo()%>" href="#"><i class="icon_edit ri-edit-2-fill"></i></a></td>
            <td><%= ruta.getRuta_codigo()%></td>
            <td><%= ruta.getRuta_origen()%></td>
            <td><%= ruta.getRuta_sede_origen()%></td>
            <td><%= ruta.getRuta_destino()%></td>
            <td><%= ruta.getRuta_sede_destino()%></td>
            <td><%= ruta.getRuta_precio_base()%></td>
        </tr>
        <%
            }
        %> 
    </tbody>
</table>
    
<script type="text/javascript" src="resources/js/Funciones2.js"></script>    