

<%@page import="Interfaces.Interface_Ruta"%>

<%@page import="modeloDAO.RutaDAO"%>

<%@page import="modelo.Ruta"%>

<%@page import="com.google.gson.Gson"%>


<%  
    
    Interface_Ruta interface_ruta = new RutaDAO();

    String accion = request.getParameter("action");
    
    if (!accion.isEmpty()) {

        if (accion.equals("updateRuta")) {
            
            String ruta_codigo = request.getParameter("ruta_codigo");
            Ruta ruta = interface_ruta.obtenerUno(ruta_codigo);

            if(ruta != null){
                
                String json = new Gson().toJson(ruta);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                    
            }
        }

    }


%>
