
<%@page import="com.google.gson.Gson"%>
<%@page import="Interfaces.Interface_Generica"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Tienda"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.TiendaDAO"%>
<%@page import="modelo.Provincia"%>
<%@page import="modelo.Tienda"%>


<%  
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;
    
    TiendaDAO tiendaDAO = new TiendaDAO();
    Interface_Tienda interface_tienda = tiendaDAO;

    String accion = request.getParameter("action");
    
    if (!accion.isEmpty()) {

        if (accion.equals("updateTienda")) {
            
            String tiend_codigo = request.getParameter("codigo");
            Tienda tienda = interface_tienda.obtenerUno(tiend_codigo);

            if(tienda != null){
                
                String json = new Gson().toJson(tienda);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write(json);
                    
            }
        }

    }


%>

