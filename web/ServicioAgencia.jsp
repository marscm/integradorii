<%@page import="modeloDAO.DistritoDAO"%>
<%@page import="modelo.Provincia"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <!--========= FAVICON (icono q sala en navegador)(COPIAR PEGAR)=============--> 
        <link rel="shortcut icon" href="#" type="image/png">
        <!--========= REMIX ICONS (iconos) (COPIAR PEGAR)=============-->
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--========= NUESTRO CSS == (COPIAR PEGAR)===========-->
        <link rel="stylesheet" href="css/estilo.css"> 
        <link rel="stylesheet" href="css/estilo_service.css">  

        <title>Servicios Fast Courier</title>
        <script>
            function paso1(){
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:white");
                var circulo1Datos=document.getElementById('circulo1Datos'); 
                circulo1Datos.setAttribute("style","fill:none;stroke:black;stroke-width:2");
                var circulo2Datos=document.getElementById('circulo2Datos'); 
                circulo2Datos.setAttribute("style","fill:none;stroke:black;stroke-width:2");
                var pDatos=document.getElementById('pDatos'); 
                pDatos.setAttribute("style","fill:none;stroke:black;stroke-width:2");
    }
            
            function primerSgte(){
                var paso1=document.getElementById('paso1');
                var paso2=document.getElementById('paso2');
                var boton=document.getElementById('primerbtn'); 
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:white");
                var circulo1Datos=document.getElementById('circulo1Datos'); 
                circulo1Datos.setAttribute("style","fill:none;stroke:black;stroke-width:2");
                var circulo2Datos=document.getElementById('circulo2Datos'); 
                circulo2Datos.setAttribute("style","fill:none;stroke:black;stroke-width:2");
                var pDatos=document.getElementById('pDatos'); 
                pDatos.setAttribute("style","fill:none;stroke:black;stroke-width:2");
                    paso1.style.display="none";
                    paso2.style.display="block";
                    boton.style.display="none"; 
            }

            function tabla_verPaso2(){
                var pd=document.getElementById('paso2');
                var tabla=document.getElementById('paso3');
                var btnsT=document.getElementById('btnPaso3');
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:white");
                var pRegistro=document.getElementById('pRegistro'); 
                pRegistro.setAttribute("style","fill:black");
                var rRegistro=document.getElementById('rRegistro'); 
                rRegistro.setAttribute("style","fill:black");
                var polyRegistro=document.getElementById('polyRegistro'); 
                polyRegistro.setAttribute("style","fill:black");
                tabla.style.display="none";
                btnsT.style.display="none";
                pd.style.display="block";
            }
            
            function caja_verPaso3(){
                var xs=document.getElementById('paso2');
                var ys=document.getElementById('paso3');
                var btn3=document.getElementById('btnPaso3');
                var rs=document.getElementById('caja');
                var zs=document.getElementById('btnsCaja');  
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:black");
                var pRegistro=document.getElementById('pRegistro'); 
                pRegistro.setAttribute("style","fill:white");
                var rRegistro=document.getElementById('rRegistro'); 
                rRegistro.setAttribute("style","fill:white");
                var polyRegistro=document.getElementById('polyRegistro'); 
                polyRegistro.setAttribute("style","fill:white");
                    xs.style.display="none";
                    zs.style.display="none";
                    rs.style.display="none";
                    ys.style.display="block";
                    btn3.style.display="flex";
                    
            }
            
            function sobre_verPaso3(){
                var xs=document.getElementById('paso2');
                var ys=document.getElementById('paso3');
                var btn3=document.getElementById('btnPaso3');
                var rs=document.getElementById('sobre');
                var zs=document.getElementById('btnsSobre');  
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:black");
                var pRegistro=document.getElementById('pRegistro'); 
                pRegistro.setAttribute("style","fill:white");
                var rRegistro=document.getElementById('rRegistro'); 
                rRegistro.setAttribute("style","fill:white");
                var polyRegistro=document.getElementById('polyRegistro'); 
                polyRegistro.setAttribute("style","fill:white");
                    xs.style.display="none";
                    zs.style.display="none";
                    rs.style.display="none";
                    ys.style.display="block";
                    btn3.style.display="flex";
            }
            
            function caja_verPaso1(){
                var pi=document.getElementById('paso1');
                var ptipo=document.getElementById('paso2');
                var z=document.getElementById('primerbtn'); 
                var rs=document.getElementById('caja');
                var zs=document.getElementById('btnsCaja'); 
                var btnsS=document.getElementById('btnsSobre');
                var btnsC=document.getElementById('btnsCaja');
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:black");
                var circulo1Datos=document.getElementById('circulo1Datos'); 
                circulo1Datos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                var circulo2Datos=document.getElementById('circulo2Datos'); 
                circulo2Datos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                var pDatos=document.getElementById('pDatos'); 
                pDatos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                ptipo.style.display="none";
                rs.style.display="none";
                zs.style.display="none";
                btnsS.style.display="none";
                btnsC.style.display="none";
                pi.style.display="block";
                z.style.display="flex";
            }
            
            function sobre_verPaso1(){
                var pi=document.getElementById('paso1');
                var ptipo=document.getElementById('paso2');
                var z=document.getElementById('primerbtn'); 
                var rs=document.getElementById('sobre');
                var zs=document.getElementById('btnsCaja'); 
                var btnsS=document.getElementById('btnsSobre');
                var btnsC=document.getElementById('btnsCaja');
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:black");
                var circulo1Datos=document.getElementById('circulo1Datos'); 
                circulo1Datos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                var circulo2Datos=document.getElementById('circulo2Datos'); 
                circulo2Datos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                var pDatos=document.getElementById('pDatos'); 
                pDatos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                ptipo.style.display="none";
                rs.style.display="none";
                zs.style.display="none";
                btnsS.style.display="none";
                btnsC.style.display="none";
                pi.style.display="block";
                z.style.display="flex";
            }
            
            function pagar_verPaso3(){
                var tabla=document.getElementById('paso3');
                var pag=document.getElementById('paso4');
                var btnsT=document.getElementById('btnPaso3');
                var btnsP=document.getElementById('btnPaso4');
                var pRegistro=document.getElementById('pRegistro'); 
                pRegistro.setAttribute("style","fill:white");
                var rRegistro=document.getElementById('rRegistro'); 
                rRegistro.setAttribute("style","fill:white");
                var polyRegistro=document.getElementById('polyRegistro'); 
                polyRegistro.setAttribute("style","fill:white");
                var p1Enviado=document.getElementById('p1Enviado'); 
                p1Enviado.setAttribute("style","fill:black");
                var p2Enviado=document.getElementById('p2Enviado'); 
                p2Enviado.setAttribute("style","fill:black");
                tabla.style.display="block";
                pag.style.display="none";    
                btnsT.style.display="flex";
                btnsP.style.display="none";
            }
            function ingresarPedido(){
                var tabla=document.getElementById('paso3');
                var pi=document.getElementById('paso1');
                var btnsT=document.getElementById('btnPaso3');
                var z=document.getElementById('primerbtn'); 
                var pRegistro=document.getElementById('pRegistro'); 
                pRegistro.setAttribute("style","fill:black");
                var rRegistro=document.getElementById('rRegistro'); 
                rRegistro.setAttribute("style","fill:black");
                var polyRegistro=document.getElementById('polyRegistro'); 
                polyRegistro.setAttribute("style","fill:black");
                var circulo1Datos=document.getElementById('circulo1Datos'); 
                circulo1Datos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                var circulo2Datos=document.getElementById('circulo2Datos'); 
                circulo2Datos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                var pDatos=document.getElementById('pDatos'); 
                pDatos.setAttribute("style","fill:none;stroke:white;stroke-width:2");
                tabla.style.display="none";
                pi.style.display="block";    
                btnsT.style.display="none";
                z.style.display="flex";
            }
            
          
            function showSobre(){
                var datosCaja=document.getElementById('datosCaja');
                var sobreIcono=document.getElementById('sobreIcono'); 
                sobreIcono.setAttribute("style","fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
                var cajaIcono=document.getElementById('contornoCaja'); 
                cajaIcono.setAttribute("style","fill:black;stroke:black;stroke-width:0.5");    
                datosCaja.style.display="none";
            }
            function showCaja(){
                var datosCaja=document.getElementById('datosCaja');
                var sobreIcono=document.getElementById('sobreIcono'); 
                sobreIcono.setAttribute("style","fill:black;stroke:hsl(190, 24%, 97%);stroke-width:0.5");
                var cajaIcono=document.getElementById('contornoCaja'); 
                cajaIcono.setAttribute("style","fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
                datosCaja.style.display="block";
            }
            
            function colorCaja(){
                var cajaIcono=document.getElementById('contornoCaja'); 
                cajaIcono.setAttribute("style","fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
                var sobreIcono=document.getElementById('sobreIcono'); 
                sobreIcono.setAttribute("style","fill:black;stroke:hsl(190, 24%, 97%);stroke-width:0.5");
            }
            function colorSobre(){
                var sobreIcono=document.getElementById('sobreIcono'); 
                sobreIcono.setAttribute("style","fill:hsl(19, 34%, 35%);stroke:hsl(190, 24%, 97%);stroke-width:0.5");
                var cajaIcono=document.getElementById('contornoCaja'); 
                cajaIcono.setAttribute("style","fill:black;stroke:black;stroke-width:0.5");
            }
            function GoPagar(){
                var tabla=document.getElementById('paso3');
                var sgt=document.getElementById('paso4');
                var btnsT=document.getElementById('btnPaso3');
                var btnsP=document.getElementById('btnPaso4');
                var lapiz=document.getElementById('lapiz'); 
                lapiz.setAttribute("style","fill:black");
                var pRegistro=document.getElementById('pRegistro'); 
                pRegistro.setAttribute("style","fill:black");
                var rRegistro=document.getElementById('rRegistro'); 
                rRegistro.setAttribute("style","fill:black");
                var polyRegistro=document.getElementById('polyRegistro'); 
                polyRegistro.setAttribute("style","fill:black");
                var p1Enviado=document.getElementById('p1Enviado'); 
                p1Enviado.setAttribute("style","fill:white;stroke:white;stroke-width:0.2");
                var p2Enviado=document.getElementById('p2Enviado'); 
                p2Enviado.setAttribute("style","fill:white;stroke:white;stroke-width:0.2");
                tabla.style.display="none";
                sgt.style.display="block";    
                btnsT.style.display="none";
                btnsP.style.display="flex";
            }

        </script>

    </head>
    <body>
        <!--========= NAVER--(COPIAR PEGAR)-------------------------------------------- ================================================-->
        <header class="header" id="header">
            <nav class="nav container">
                <a href="#" class="nav_logo">FAST COURIER</a>

                <div class="nav_menu" id="nav-menu">
                    <ul class="nav_list">
                        <li class="nav_item">
                            <a href="index.jsp" class="nav_link ">Inicio</a>
                        </li>
                        <li class="nav_item">
                            <a href="index.jsp#nuestro_servicio" class="nav_link active-link">Servicio</a>
                        </li>
                        <li class="nav_item">
                            <a href="nosotros.jsp" class="nav_link">Nosotros</a>
                        </li>
                        <li class="nav_item">
                            <a href="contacto.jsp" class="nav_link">Contacto</a>
                        </li>
                    </ul>

                    <div class="nav_close" id="nav-close">
                        <i class="ri-close-line"></i>
                    </div>	
                </div>
                <!--======= ---Responsibe Hamburguesa====-->
                <div class="nav_toggle" id="nav-toggle">
                    <i class="ri-function-line"></i>
                </div>
            </nav>
        </header>
        <!--======= SOCIAL ==(COPIAR PEGAR)========================-->
        <div class="social">
            <ul class="sci">

                <li><a href="#" target="_blank" class="home_social-link"><i class="ri-facebook-circle-fill"></i></a></li>


                <li><a href="#" target="_blank" class="home_social-link"><i class="ri-instagram-fill"></i></a></li>

                <li><a href="#" target="_blank" class="home_social-link"><i class="ri-whatsapp-fill"></i></a></li>

            </ul>
        </div>
        <!--====================-->
        <main class="main">
            <!--======= Inicio (COPIAR PEGAR)===================================================================================================-->
            <section class="home " id="home">
                <img src="img/img_servicioAgencia.jpg" class="home_img">

                <div class="home_container container grid">
                    <div class="home_data">
                        <h1 class="home_data_titulo"><b>Surgio Algo? </b></h1>
                    </div>
                    <div class="home_info">
                        <p>Si tienes algún problema por favor contacta con nosotros. Puedes acceder a nuestras redes sociales, respondemos rapidamente, también puedes llamar a nuestras operadoras.
                        </p>
                    </div>
                    <div class="home_button">
                        <a href="contacto.html" class="button">Contactanos</a>
                    </div>
                </div>
            </section>
            <!--======= Servicios Agencia ===========================================================================================-->      
            <h1 class="tituloServicios">Servicio a Agencia</h1>
            <div class="barra">
                <div class="columnaBarra" id="datos">
                    <div>
                        <svg version="1.1" id="Icons" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
                        <style type="text/css">
                            .st1{fill:none;stroke:white;stroke-width:2;stroke-linejoin:round;stroke-miterlimit:10;}
                        </style>
                        <circle id="circulo1Datos" class="st1" cx="16" cy="16" r="14"/>
                        <circle id="circulo2Datos" class="st1" cx="16" cy="13" r="5"/>
                        <path id="pDatos" class="st1" d="M5.4,25.1c1.8-4.1,5.8-7,10.6-7c4.8,0,8.9,2.9,10.6,7"/>
                        </svg>
                    </div>
                    <p>Datos Personales</p>
                </div>
                <div class="columnaBarra">
                    <div>
                        <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 64 64">
                        <path id="lapiz" d="M60.586,15.899c0-1.068-0.416-2.073-1.172-2.828l-8.485-8.485c-1.511-1.512-4.146-1.512-5.657,0l-4.243,4.243c-0.779-0.78-2.049-0.78-2.828,0l-5.657,5.657c-0.378,0.378-0.586,0.88-0.586,1.414c0,0.535,0.208,1.037,0.586,1.414l0.05,0.05L5.518,44.399c-0.504,0.504-0.813,1.173-0.87,1.882l-0.675,8.397L3.27,59.599c-0.045,0.312,0.06,0.626,0.283,0.849c0.189,0.189,0.444,0.293,0.707,0.293c0.047,0,0.094-0.003,0.142-0.01l4.917-0.702l8.446-0.621c0.717-0.052,1.392-0.361,1.9-0.869l27.078-27.037c0.371,0.346,0.849,0.541,1.359,0.541c0.534,0,1.036-0.208,1.414-0.586l5.657-5.657c0.78-0.78,0.78-2.048,0-2.828l4.243-4.243C60.17,17.973,60.586,16.968,60.586,15.899z M9.593,58.002L6,54.41l0.611-7.605l5.284,1.057l0.576,2.882c0.079,0.396,0.389,0.705,0.784,0.784l2.882,0.576l1.068,5.338L9.593,58.002z M19.026,56.349l-0.953-4.765l16.592-16.592c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L16.659,50.169l-2.357-0.472l-0.471-2.357l13.764-13.764c0.391-0.391,0.391-1.023,0-1.414s-1.023-0.391-1.414,0L12.417,45.927l-4.664-0.933l26.256-26.216l11.314,11.314L19.026,56.349z M52.344,25.799C52.343,25.799,52.343,25.799,52.344,25.799c-0.391-0.391-1.024-0.391-1.414,0l-1.415,1.415c-0.391,0.391-0.391,1.023,0,1.414c0,0,0.001,0,0.001,0.001l-1.414,1.414l-0.001,0l0,0L33.958,15.899l1.414-1.414c0.195,0.195,0.451,0.293,0.707,0.293s0.512-0.098,0.707-0.293l1.415-1.415c0.391-0.391,0.391-1.023,0-1.414l1.414-1.414l0.707,0.707L53.05,23.678l0.707,0.707L52.344,25.799z M58,17.314l-4.243,4.243L42.443,10.243L46.686,6c0.756-0.756,2.073-0.756,2.829,0L58,14.485c0.378,0.378,0.586,0.88,0.586,1.414S58.378,16.936,58,17.314z"/><path d="M40.322 15.192l-1.414 1.414c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293s.512-.098.707-.293l1.414-1.414c.391-.391.391-1.023 0-1.414S40.713 14.802 40.322 15.192zM43.858 18.728l-1.414 1.414c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293s.512-.098.707-.293l1.414-1.414c.391-.391.391-1.023 0-1.414S44.249 18.337 43.858 18.728zM47.394 22.264l-1.414 1.414c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293s.512-.098.707-.293l1.414-1.414c.391-.391.391-1.023 0-1.414S47.784 21.873 47.394 22.264zM31.837 26.506l-2.828 2.828c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293s.512-.098.707-.293l2.828-2.828c.391-.391.391-1.023 0-1.414S32.228 26.116 31.837 26.506z"/>
                        </svg>
                    </div>
                    <p>Registro de Envío</p>
                </div>
                <div class="columnaBarra" id="confirmacion">
                    <div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
                        <g id="Layer_1">
                        <path id="pRegistro" d="M29.618,5l-2-4h-5.236l-2,4H17v2H8v42h34V7h-9V5H29.618z M21.618,7l2-4h2.764l2,4H31v4v2H19v-2V7H21.618z M33,15v-2h3v30
                              H14V13h3v2H33z M40,9v38H10V9h7v2h-5v34h26V11h-5V9H40z"/>
                        <rect id="rRegistro" x="24" y="6" width="2" height="2"/>
                        <polygon id="polyRegistro" points="33.707,22.707 32.293,21.293 22,31.586 18.707,28.293 17.293,29.707 22,34.414"/>
                        </g><g></g>
                        </svg>
                    </div>
                    <p>Confirma tus datos</p>
                </div>
                <div class="columnaBarra" id="enviado">
                    <div>
                        <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 64 64">
                        <path id="p1Enviado" d="M38.351,13.322c0.713,0.267,1.42,0.576,2.1,0.919c0.145,0.073,0.298,0.107,0.449,0.107c0.365,0,0.718-0.201,0.894-0.549c0.249-0.493,0.051-1.095-0.442-1.343c-0.744-0.376-1.519-0.715-2.301-1.007c-0.518-0.194-1.093,0.07-1.286,0.586C37.57,12.553,37.833,13.128,38.351,13.322z"/>
                        <path id="p2Enviado" d="M62.707,12.293l-4-4c-0.391-0.391-1.023-0.391-1.414,0l-5.066,5.066c-0.046-0.124-0.116-0.241-0.213-0.343C46.484,7.202,39.021,4,31,4C15.01,4,2,17.009,2,33s13.01,29,29,29s29-13.009,29-29c0-4.645-1.131-9.233-3.255-13.331l5.962-5.962C63.098,13.316,63.098,12.684,62.707,12.293z M58,33c0,14.888-12.112,27-27,27S4,47.888,4,33S16.112,6,31,6c7.469,0,14.417,2.981,19.565,8.394c0.11,0.116,0.244,0.19,0.385,0.242l-2.966,2.966c-0.045-0.119-0.111-0.232-0.203-0.33c-0.169-0.18-0.342-0.358-0.518-0.534c-0.658-0.659-1.363-1.283-2.094-1.856c-0.437-0.341-1.063-0.264-1.404,0.17c-0.341,0.435-0.265,1.063,0.17,1.404c0.668,0.523,1.312,1.094,1.914,1.696c0.16,0.16,0.317,0.322,0.471,0.486c0.109,0.117,0.242,0.194,0.382,0.246L31,34.586L20.707,24.293c-0.391-0.391-1.023-0.391-1.414,0l-4,4c-0.391,0.391-0.391,1.023,0,1.414l15,15C30.488,44.902,30.744,45,31,45s0.512-0.098,0.707-0.293l18.985-18.985c0.067,0.18,0.13,0.361,0.191,0.542c0.101,0.3,0.195,0.601,0.282,0.904c0.037,0.129,0.079,0.257,0.114,0.386c0.12,0.453,0.225,0.909,0.314,1.367c0.015,0.078,0.028,0.157,0.043,0.235c0.819,4.484,0.147,9.135-1.864,13.201c-0.103,0.207-0.206,0.414-0.315,0.616c-0.166,0.31-0.34,0.617-0.522,0.919c-0.167,0.275-0.343,0.541-0.521,0.806c-0.156,0.233-0.304,0.471-0.47,0.698c-0.347,0.474-0.711,0.93-1.093,1.368c-0.136,0.157-0.284,0.306-0.426,0.46c-5.954,6.445-15.582,8.719-24.019,4.936c-2.518-1.129-4.693-2.706-6.5-4.582c-0.25-0.259-0.485-0.528-0.719-0.796c-0.238-0.273-0.477-0.546-0.699-0.83c-0.171-0.218-0.328-0.444-0.49-0.667c-0.254-0.352-0.506-0.705-0.737-1.071c-0.104-0.165-0.198-0.336-0.297-0.503c-0.253-0.427-0.5-0.857-0.722-1.301c-0.068-0.136-0.126-0.276-0.192-0.413c-0.223-0.47-0.437-0.945-0.624-1.43c-0.062-0.162-0.113-0.327-0.171-0.491c-0.164-0.456-0.32-0.915-0.452-1.382c-0.071-0.253-0.125-0.509-0.186-0.764c-0.092-0.377-0.184-0.754-0.255-1.137c-0.069-0.377-0.116-0.758-0.164-1.138c-0.034-0.265-0.076-0.529-0.1-0.796c-0.042-0.478-0.059-0.958-0.068-1.438c-0.004-0.178-0.016-0.355-0.015-0.533c0.003-0.517,0.033-1.034,0.074-1.55c0.012-0.154,0.017-0.308,0.032-0.462c0.049-0.492,0.127-0.981,0.212-1.468c0.033-0.193,0.06-0.386,0.099-0.579c0.085-0.424,0.196-0.843,0.308-1.263c0.071-0.266,0.138-0.532,0.22-0.797c0.107-0.346,0.234-0.686,0.359-1.027c0.123-0.333,0.246-0.667,0.388-0.997c0.149-0.349,0.312-0.694,0.48-1.036c0.111-0.225,0.218-0.453,0.336-0.673c0.224-0.417,0.47-0.824,0.725-1.228c0.088-0.139,0.167-0.286,0.258-0.423c0.354-0.533,0.736-1.052,1.144-1.557c0.1-0.124,0.209-0.239,0.311-0.36c0.323-0.383,0.659-0.758,1.014-1.122c0.227-0.233,0.458-0.462,0.695-0.684c0.104-0.097,0.204-0.197,0.31-0.291c0.363-0.326,0.737-0.638,1.121-0.937c0.13-0.101,0.266-0.196,0.398-0.294c0.263-0.195,0.531-0.385,0.803-0.568c0.183-0.123,0.367-0.243,0.554-0.36c0.23-0.144,0.464-0.28,0.699-0.415c0.411-0.235,0.829-0.456,1.254-0.663c0.155-0.076,0.307-0.155,0.464-0.227c0.295-0.135,0.595-0.258,0.895-0.379c0.125-0.05,0.25-0.1,0.376-0.148c0.327-0.124,0.656-0.245,0.989-0.352c0.006-0.002,0.012-0.004,0.018-0.005c0.769-0.248,1.551-0.452,2.344-0.611c0.01-0.002,0.02-0.004,0.03-0.006c0.371-0.074,0.744-0.135,1.119-0.189c0.079-0.011,0.159-0.021,0.238-0.031c1.054-0.139,2.122-0.195,3.194-0.172c0.214,0.005,0.427,0.011,0.642,0.022c0.262,0.014,0.524,0.031,0.787,0.054c0.359,0.033,0.717,0.076,1.077,0.128c0.119,0.017,0.239,0.027,0.358,0.046c0.545,0.089,1.059-0.284,1.146-0.829c0-0.001,0-0.001,0-0.002v0c0.088-0.545-0.282-1.059-0.828-1.147c-0.125-0.02-0.249-0.026-0.374-0.044c-0.401-0.058-0.803-0.107-1.205-0.143c-0.277-0.025-0.553-0.042-0.829-0.057c-0.256-0.014-0.511-0.021-0.766-0.026c-1.128-0.024-2.246,0.037-3.349,0.177c-0.146,0.019-0.292,0.037-0.438,0.058c-0.366,0.054-0.729,0.113-1.091,0.184c-0.112,0.022-0.223,0.049-0.334,0.072c-0.675,0.143-1.34,0.315-1.996,0.517c-0.142,0.043-0.285,0.083-0.426,0.129c-0.303,0.1-0.602,0.209-0.901,0.321c-0.223,0.083-0.443,0.171-0.663,0.261c-0.259,0.107-0.519,0.212-0.774,0.328c-0.447,0.202-0.889,0.417-1.324,0.647c-0.146,0.078-0.286,0.164-0.43,0.245c-0.351,0.196-0.698,0.401-1.039,0.616c-0.15,0.095-0.298,0.191-0.446,0.29c-0.374,0.248-0.742,0.508-1.102,0.779c-0.083,0.062-0.168,0.121-0.25,0.185c-0.449,0.347-0.886,0.712-1.311,1.094c-0.069,0.063-0.135,0.129-0.204,0.192c-0.224,0.207-0.45,0.411-0.666,0.627c-0.079,0.079-0.144,0.166-0.222,0.245c-0.355,0.365-0.694,0.746-1.026,1.136c-0.135,0.159-0.28,0.313-0.41,0.474c-0.434,0.539-0.847,1.097-1.233,1.679c-0.114,0.172-0.211,0.351-0.32,0.525c-0.264,0.421-0.521,0.846-0.759,1.286c-0.134,0.249-0.253,0.502-0.377,0.755c-0.124,0.251-0.26,0.494-0.375,0.751c-0.091,0.202-0.17,0.406-0.255,0.609c-0.033,0.079-0.067,0.157-0.099,0.237c-0.263,0.655-0.492,1.316-0.691,1.98c-0.006,0.02-0.012,0.039-0.018,0.059c-0.856,2.887-1.116,5.843-0.835,8.721c0.001,0.009,0.002,0.018,0.003,0.026c0.068,0.689,0.166,1.374,0.295,2.051c0.017,0.093,0.037,0.186,0.056,0.279c0.121,0.596,0.264,1.186,0.431,1.77c0.048,0.17,0.101,0.339,0.153,0.509c0.16,0.513,0.336,1.021,0.531,1.522c0.083,0.214,0.171,0.426,0.261,0.638c0.198,0.467,0.411,0.928,0.64,1.381c0.107,0.213,0.218,0.424,0.333,0.634c0.253,0.465,0.523,0.922,0.808,1.37c0.108,0.17,0.217,0.338,0.33,0.505c0.335,0.497,0.692,0.98,1.066,1.452c0.082,0.103,0.162,0.206,0.247,0.308c0.438,0.533,0.903,1.046,1.391,1.54c0.027,0.027,0.048,0.057,0.075,0.083c0.005,0.005,0.012,0.006,0.017,0.011c1.921,1.922,4.212,3.536,6.835,4.712c3.047,1.366,6.235,2.013,9.377,2.012c6.074,0,11.965-2.421,16.288-6.729c0.003-0.003,0.007-0.004,0.01-0.007c0.079-0.079,0.149-0.165,0.226-0.245c0.32-0.328,0.635-0.662,0.936-1.012c0.069-0.08,0.131-0.164,0.198-0.244c0.31-0.37,0.612-0.747,0.9-1.139c0.033-0.045,0.062-0.091,0.095-0.136c0.908-1.253,1.709-2.609,2.367-4.076c1.928-4.299,2.462-9.057,1.616-13.617c-0.016-0.088-0.031-0.175-0.048-0.262c-0.099-0.501-0.21-1-0.343-1.496c-0.037-0.141-0.083-0.279-0.124-0.42c-0.096-0.335-0.199-0.668-0.311-0.999c-0.072-0.214-0.146-0.426-0.225-0.638c-0.1-0.268-0.208-0.534-0.317-0.799l3.026-3.026C57.051,24.823,58,28.888,58,33z M31,42.586L17.414,29L20,26.414l10.293,10.293c0.391,0.391,1.023,0.391,1.414,0L58,10.414L60.586,13L31,42.586z"/>
                        </svg>
                    </div>
                    <p>Registro Enviado</p>
                </div>
            </div>
        </header>    
        <main>
            <section>
                <h1 class="tituloRegistro">REGISTRO DE ENVIOS</h1>
                <form id="paso1" action="ControlCliente" method="post">
                    <fieldset >    
                        <div class="areaDatos">
                            <h2>¿Quien Envía?</h2><br>
                            <div class="filaFormulario">
                                <div class="seccion30">
                                    <input type="text"  name="dni" id="dniCliente" required>
                                    <label>DNI</label>
                                </div>
                                <div class="seccion30">
                                    <input type="text" name="telf" id="telfCliente" required>
                                    <label>Telefono</label>
                                </div>
                                <div class="seccion30">    
                                    <input onchange="paso1()" type="email" name="correo" id="correoCliente" required> 
                                    <label>Correo Electronico</label>
                                </div>
                            </div>
                        </div>
                        <div class="areaDatos">
                            <h2>¿Desde dónde parte el envío?</h2>
                            <% 
                                ProvinciaDAO pd=new ProvinciaDAO();
                            %>
                            <div class="filaFormulario">
                                <select name="Departamento" id="provinciaEmisor" class="seccion50">
                                    <%
                                        for(Provincia prov:pd.ListarProvincias()){
                                    %>
                                    <option id="provi"><%=prov.getNombre_prov()%></option>      
                                    <%
                                        }    
                                    %>
                                </select>
                                <select name="agencia" id="distritoEmisor" class="seccion50" id="">
                                    <option selected>---------</option>

                                </select> 
                            </div>
                        </div>
                    </fieldset>

                    <section id="paso2">
                        <h2 style="padding-left:80px">Tipos de envío</h2>
                        <section class="barraTipo">
                            <div id="opcionSobre" onclick="showSobre()" class="EnvioSobre">
                                <div>
                                    <svg viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" 
                                         xmlns:xlink="http://www.w3.org/1999/xlink" 
                                         xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                    <title>Envio de Sobre</title>
                                    <defs></defs>
                                    <style type="text/css">
                                        .stA{fill:black;stroke:hsl(190, 24%, 97%);stroke-width:0.5;}
                                    </style>
                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                    <g id="icon-2-mail-envelope-open" sketch:type="MSArtboardGroup" fill="#000000">
                                    <path class="stA" id="sobreIcono" d="M11.2931426,19 L8,16.2000122 L8,8.00207596 C8,7.44373571 8.44769743,7 8.9999602,7 
                                          L24.0000398,7 C24.5452911,7 25,7.44864469 25,8.00207596 L25,16.2000122 L21.6668971,19 
                                          L11.2931426,19 L11.2931426,19 Z M12.4692701,20 L14.2521973,21.5159302 L18.671936,
                                          21.5159302 L20.476498,20 L12.4692701,20 L12.4692701,20 Z M22.1851633,20.1270029 L20,22 
                                          L20,22 L25.9230769,27.0769231 L25.9230769,27.0769231 L25.1153846,27.8846154 L19.1195568,
                                          22.7453344 L13.8961462,22.7681253 L13.875,22.75 L7.88461538,27.8846154 L7.07692308,
                                          27.0769231 L7.07692308,27.0769231 L13,22 L10.8148367,20.1270029 L7,16.9000244 L7,11.36 
                                          L4,14 L4,15 L4,28.0059397 C4,29.1072288 4.88976324,30 6.00359486,30 L26.9964051,30 
                                          C28.10296,30 29,29.1054862 29,28.0059397 L29,14 L26,11.36 L26,16.9000244 L22.1851633,
                                          20.1270029 L22.1851633,20.1270029 L22.1851633,20.1270029 Z M19.9090909,6 L16.5,3 
                                          L13.0909091,6 L19.9090909,6 L19.9090909,6 L19.9090909,6 Z M10,10 L10,11 L23,11 L23,10 
                                          L10,10 L10,10 Z M10,13 L10,14 L23,14 L23,13 L10,13 L10,13 Z M10,16 L10,17 L23,17 L23,16 
                                          L10,16 L10,16 Z" id="mail-envelope-open" sketch:type="MSShapeGroup"> 
                                    </path>
                                    </g>
                                    </g>
                                    </svg>
                                </div>
                                <p>Sobre</p>
                                <p>Peso máximo: 100g</p>
                                <p>Solo para documentos</p>
                            </div>
                            <div class="EnvioSobre" id="opcionCaja" onclick="showCaja()">
                                <div >
                                    <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 128 128">
                                    <style type="text/css">
                                        .st2{fill:black;stroke:black;stroke-width:0.5;}
                                    </style>
                                    <polyline fill="#fff" points="12,34 32,14 96,14 116,34"/>
                                    <rect width="104" height="80" x="12" y="34" fill="#fff"/>
                                    <path id="contornoCaja" class="st2" fill="#444b54" d="M118.1,31.9l-20-20C97.6,11.3,96.8,11,96,11H32c-0.8,0-1.6,0.3-2.1,0.9l-20,20C9.3,32.4,9,33.2,9,34v80 c0,1.7,1.3,3,3,3h104c1.7,0,3-1.3,3-3V34C119,33.2,118.7,32.4,118.1,31.9z M108.8,31H67V17h27.8L108.8,31z M33.2,17H61v14H19.2 L33.2,17z M113,111H15V37h98V111z"/>
                                    </svg>
                                </div>
                                <p>Caja</p>
                                <p>Peso máximo: 254g</p>
                                <p>Medidas mínimas: 14 x 10 x 10 cm</p>
                            </div>
                        </section>
                    </section>
                    <fieldset id="datosCaja" style="display:none">
                        <div class="areaDatos">
                            <h2>Información del Paquete</h2><br>
                            <div class="filaFormulario">
                                <div class="seccion22">
                                    <input type="text"  name="Largo" required>
                                    <label>Largo</label>
                                </div>
                                <div class="seccion22">
                                    <input type="text"  name="Ancho" required>
                                    <label>Ancho</label>
                                </div> 
                                <div class="seccion22">
                                    <input type="text"  name="Altura" required>
                                    <label>Altura</label>
                                </div>
                                <div class="seccion22">
                                    <input type="text"  name="peso" required>
                                    <label>Peso</label>
                                </div>
                            </div>   
                            <div class="filaFormulario">
                                <select name="tipoPaquete" class="select30">
                                    <option>
                                        Tipo de paquete
                                    </option>
                                </select>
                                <div class="seccion30">
                                    <input type="text"  name="Especifique" required>
                                    <label>Especifique</label>
                                </div>
                                <div class="seccion30">
                                    <input type="text"  name="ValorRef" required>
                                    <label>Valor Referencial</label>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset id="DatosReceptor"> 
                        <br>
                        <div class="areaDatos">
                            <h2>¿Quién lo recoje?</h2><br>
                            <div class="filaFormulario">
                                <div class="seccion22">
                                    <input type="text"  name="dniDestinoCaja" required >
                                    <label>DNI</label>
                                </div>
                                <div class="seccion22">
                                    <input type="text"  name="telefonoDestinoCaja" required >
                                    <label>Telefono</label>
                                </div>
                                <div class="seccion46">
                                    <input type="mail"  name="mailDestinoCaja" required >
                                    <label>Correo electrónico</label>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                    <br>
                    <fieldset>
                        <div class="areaDatos">
                            <h2>¿Cuál de nuestras agencias recibirá el paquete?</h2><br>
                            <div class="filaFormulario">
                                <select class="select50" name="departamentoDestinoCaja">
                                    <option>Departamento</option>
                                </select>
                                <select class="select50" name="provinciaDestinoCaja">
                                    <option>Provincia1</option>
                                </select>
                            </div>
                            <div class="filaFormulario">
                                <select class="select90" name="tiendaDestinoCaja">
                                    <option>tienda1</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <br>   
                </form>
                <div class="boton" id="btnsSobre" style="display:none">
                    <button onclick="sobre_verPaso1()">Volver</button>
                    <button onclick="sobre_verPaso3()">Continuar</button>
                </div>
            </section>    
            <!--  <section class="resumenEnvios" id="paso3" style="display:none">
    <h2>Resumen de envíos</h2>
    <table class="tablaResumen">
        <tr>
            <th>Acción</th>
            <th>N°</th>
            <th>Recibe</th>
            <th>Origen - Destino</th>
            <th>Dirección y especificación</th>
            <th>Tipo de Envío</th>
            <th>Precio</th>
        </tr>
        <tr>
            <td class="quitar">
                <div>
                    <svg id="Layer_1" data-name="Layer 1" 
                    xmlns="http://www.w3.org/2000/svg" 
                    viewBox="0 0 24 24">
                    <title>eliminar</title>
                    <path d="M2.88,5,5.11,24H18.89L21.12,5ZM17.11,
                          22H6.89L5.12,7H18.88Z"/>
                    <polygon points="21 2 15 2 15 1 13 1 13 0 11 0 11 1 
                             9 1 9 2 3 2 3 4 21 4 21 2"/>
                    <polygon points="10.23 17.66 12 15.89 13.77 17.66 
                             15.18 16.24 13.41 14.47 15.18 12.71 13.77 
                             11.29 12 13.06 10.23 11.29 8.82 12.71 10.59
                             14.47 8.82 16.24 10.23 17.66"/>
                    </svg>
                </div>
                <div>
                    <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <title>modificar</title>
                    <svg fill="#000000" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 30 30"><path fill="none" stroke="#000000" stroke-miterlimit="10" stroke-width="2" d="M10.5 25.9L5 27l1.1-5.5L21.7 5.9c1.3-1.3 3.1-1.3 4.4 0l0 0c1.3 1.3 1.3 3.1 0 4.4L10.5 25.9zM20.4 7.4L24.7 11.7M10.8 25.6c-.5-2.2-2.2-3.9-4.4-4.4"/></svg>
                </div>
            </td>
            <td>1</td>
            <td>YYYYYYYYY</td>
            <td>Luna</td>
            <td>XXXXXXXYYYYXX</td>
            <td>XXXXXXXYYYYXX</td>
            <td>XXXXXXXYYYYXX</td> 
        </tr>
    </table>
    <p>Total a pagar s/</p>
</section>-->
            </form>        
            <div class="boton" id="btnPaso3" style="display:none">
                <button class="agregar" onclick="ingresarPedido()">+</button>
                <button>Volver</button>
                <button onclick="GoPagar()">Pagar</button>
            </div>

            <section id="paso4" style="display:none">
                <form>
                    <div class="areaDatos">
                        <h2>Pago</h2>
                        <br><!-- comment -->
                        <div class="filaFormulario">
                            <div class="seccion30">
                                <input type="text"  name="nombreCliente" required>
                                <label>Nombres</label>
                            </div>
                            <div class="seccion30">
                                <input type="text"  name="appCliente" required>
                                <label>Apellido Paterno</label>
                            </div>
                            <div class="seccion30">
                                <input type="text"  name="apmCliente" required>
                                <label>Apellido Materno</label>
                            </div>
                        </div>
                        <div class="filaFormulario">
                            <div class="seccion50">
                                <input type="text"  name="numTarjeta" required>
                                <label>Número de Tarjeta</label>
                            </div>
                            <div class="seccion50">
                                <input type="mail" name="correoCliente" required>
                                <label>Correo</label>
                            </div>
                        </div>
                        <div class="filaFormulario">
                            <div class="seccion30">
                                <input type="text"  name="MMM/DD" required>
                                <label>MMM/DD</label>
                            </div>
                            <div class="seccion30">
                                <input type="text"  name="CCV" required>
                                <label>CCV</label>
                            </div>
                            <div class="seccion30">
                                <label class="calculoPago">Total a pagar s/.</label>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
            <div class="boton" id="btnPaso4" style="display:none">
                <button onclick="pagar_verPaso3()">Volver</button>
                <button>Pagar</button>
            </div>
        </main>
        <footer class="footer section">
            <div class="footer_container container grid">
                <div class="footer_content grid">
                    <div class="footer_data">

                        <h3 class="footer_title">FAST COURIER</h3>

                        <div class="footer_data_flex">
                            <div class="footer_data">
                                <ul>
                                    <li class="footer_item">
                                        <a href="#" class="footer_link">PREGUNTAS FRECUENTES</a>
                                    </li>
                                    <li class="footer_item">
                                        <a href="#" class="footer_link">TERMINOS Y CONDICIONES</a>
                                    </li>
                                    <li class="footer_item">
                                        <a href="#" class="footer_link">LIBRO DE RECLAMACIONES</a>
                                    </li>						
                                </ul>
                            </div>

                            <div class="footer_data">
                                <div class="footer_contacto">					

                                    <ul>
                                        <li>
                                            <div class="footer_contacto">
                                                <div>
                                                    <i class="ri-phone-fill footer_icon"></i>
                                                </div>
                                                Teléfono: (01)408-6145
                                            </div>
                                        </li>
                                        <li>
                                            <div class="footer_contacto">
                                                <div>
                                                    <i class="ri-building-fill footer_icon"></i>
                                                </div>
                                                Av. Los Rosales 103 - La Victoria - Lima
                                            </div>
                                        </li>
                                        <li>
                                            <div class="footer_contacto">
                                                <div>
                                                    <i class="ri-mail-fill footer_icon"></i>
                                                </div>
                                                fastCurier@gmail.com
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer_rights">
                        <p class="footer_copy">&#169; 2021 Grupo 07. All rogth reserved. Lima-Perú</p>
                    </div>
                </div>
        </footer>	
        <!-------- SCROLL UP (Boton arriba) (COPIAR PEGAR)---------------->
        <a href="" class="scrollup" id="scroll-up">
            <i class="ri-arrow-up-s-line scrollup_icon"></i>
        </a>

        <!--========= SCROLL REVEAL (ANIMACION) (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="js/scrollreveal.min.js"></script>

        <!--========= NUESTRO JS (COPIAR PEGAR)=============-->
        <script type="text/javascript" src="js/main.js"></script>
</body>
</html>
