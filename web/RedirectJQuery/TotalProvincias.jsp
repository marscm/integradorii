
<%@page import="Interfaces.Interface_Generica"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modelo.Provincia"%>

<%  
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;
    
    String prov_nombre = request.getParameter("cmbProvincia");
    String codigo = interface_provincia.buscarCodigo(prov_nombre);
    Provincia provincia = interface_provincia.obtenerUno(codigo);
    
%>




    <div class="total_info">
        <%
            int TotalProvincias;
            if(prov_nombre.equals("Todas"))
                TotalProvincias = interface_provincia.listarCantidad();
            else
                TotalProvincias = 1;
        %>      
        <p class="total_num"><%= TotalProvincias %></p>
        <span>Total Sede de Rutas</span>
    </div>


