
<%@page import="Interfaces.Interface_Generica"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Tienda"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.TiendaDAO"%>
<%@page import="modelo.Provincia"%>
<%@page import="modelo.Tienda"%>

<%  
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;
    
    TiendaDAO tiendaDAO = new TiendaDAO();
    Interface_Tienda interface_tienda = tiendaDAO;
    
    String prov_nombre = request.getParameter("cmbProvincia");
    String codigo = interface_provincia.buscarCodigo(prov_nombre);
%>



<div class="total_info">
    <%
        int TotalTiendas;
        if (prov_nombre.equals("Todas")) {
            TotalTiendas = interface_tienda.listarCantidad();
        } else {
            TotalTiendas = interface_tienda.listarCantidad(codigo);
        }
    %>      
    <p class="total_num"><%= TotalTiendas%></p>
    <span>Total Sede de Rutas</span>
</div>

