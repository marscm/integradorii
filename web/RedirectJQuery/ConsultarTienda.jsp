<%@page import="Interfaces.Interface_Generica"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Tienda"%>
<%@page import="modeloDAO.ProvinciaDAO"%>
<%@page import="modeloDAO.TiendaDAO"%>
<%@page import="modelo.Provincia"%>
<%@page import="modelo.Tienda"%>

<%  
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;
    
    TiendaDAO tiendaDAO = new TiendaDAO();
    Interface_Tienda interface_tienda = tiendaDAO;
    
    String parametro_busqueda = request.getParameter("txtTienda");   
%>



<table class="tabla_tienda_cont tienda_prov" id="idtblTiendas">
    <thead>
        <tr> 
            <td>  </td>
            <td>  </td>
            <td>C�digo</td>
            <td>Tienda</td>
            <td>Ubicaci�n</td>
            <td>Provincia</td>
        </tr>
    </thead>
    <tbody> 
        <%
            for (Tienda tienda : interface_tienda.listar2(parametro_busqueda)) {
        %>         
        <tr>
            <td><a href="Control_Tienda?accion=eliminar&atiendcodigo=<%=tienda.getTiend_codigo()%>"><i class="icon_close ri-close-circle-fill"></i></a></td>
            <td><a class="tienda_codigo" id="tienda-codigo" codigo="<%= tienda.getTiend_codigo()%>" href="#"><i class="ri-edit-fill"></i></i></a></td>
            <td><%= tienda.getTiend_codigo()%></td>
            <td><%= tienda.getTiend_nombre()%></td>
            <td><%= tienda.getTiend_ubicacion()%></td>
            <td><%= tienda.getProvincia().getProv_nombre()%></td>
        </tr>
        <%
            }
        %>                                             
    </tbody>
</table>
    
     <script type="text/javascript" src="resources/js/Funciones2.js"></script>