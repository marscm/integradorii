<%@page import="Interfaces.Interface_Generica"%>
<%@page import="Interfaces.Interface_Provincia"%>
<%@page import="Interfaces.Interface_Tienda"%>
<%@page import="ModeloDAO.ProvinciaDAO"%>
<%@page import="ModeloDAO.TiendaDAO"%>
<%@page import="Modelo.Provincia"%>
<%@page import="Modelo.Tienda"%>

<%  
    ProvinciaDAO provinciaDAO = new ProvinciaDAO();
    Interface_Provincia interface_provincia = provinciaDAO;
    
    TiendaDAO tiendaDAO = new TiendaDAO();
    Interface_Tienda interface_tienda = tiendaDAO;
    
    String parametro_busqueda = request.getParameter("txtTienda");
%>


<table class="tabla_tienda_cont tienda_prov" id="idtblTiendas">
    <thead>
        <tr> 
            <td>  </td>
            <td>C�digo</td>
            <td>Tienda</td>
            <td>Ubicaci�n</td>
            <td>Provincia</td>
        </tr>
    </thead>
    <tbody> 
        <%

                for (Tienda tienda2: interface_tienda.listar2(parametro_busqueda)){
        %> 
                <tr>
                    <td><i class="icon_close ri-close-circle-fill"></i></td>
                    <td><%= tienda2.getTiend_codigo()%></td>
                    <td><%= tienda2.getTiend_nombre()%></td>
                    <td><%= tienda2.getTiend_ubicacion() %></td>
                    <td><%= tienda2.getProvincia().getProv_nombre()%></td>
                </tr>
        <%             
                }        
         %>                                             
    </tbody>
</table>  